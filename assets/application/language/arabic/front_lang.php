<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['txt_home_title']         	  = 'محام';
$lang['txt_menu_']         	  		= 'محام';
$lang['txt_menu_about_us']         	= 'معلومات عنا';
$lang['txt_menu_our_services']      = 'خدماتنا ';
$lang['txt_menu_ask_a_question']	= 'طرح سؤال';
$lang['txt_menu_contact_us']        = 'اتصل بنا';