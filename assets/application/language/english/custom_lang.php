<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['txt_home']				= 'Home';
$lang['txt_law_book']			= 'Law Book';
$lang['txt_user_questions']		= 'User Questions';
$lang['txt_user_list']			= 'Users';
$lang['txt_about_us']			= 'About Us';
$lang['txt_change_password']	= 'Change Passoword';
$lang['txt_sign_out']			= 'Sign Out';
$lang['txt_admin']				= 'Admin';

$lang['txt_user_listing']		= 'User Listing';

// Form Elements
$lang['txt_form_sno']			= 'Sr. No.';
$lang['txt_form_first_name']	= 'First name';
$lang['txt_form_last_name']		= 'Last name';
$lang['txt_form_email']			= 'Email';
$lang['txt_form_mobile_no']		= 'Mobile No';
$lang['txt_form_user_type']		= 'User Type';
$lang['txt_form_active']		= 'Status';
$lang['txt_form_action']		= 'Action';


$lang['txt_form_campaign_name']		= 'Campaign Name';
$lang['txt_form_campaign_code']		= 'Campaign Code';
$lang['txt_form_campaign_source_id']		= 'Source Id';
$lang['txt_form_campaign_name']		= 'Campaign Name';

$lang['txt_form_question']		= 'Question';

$lang['txt_form_add']			= 'Add';


