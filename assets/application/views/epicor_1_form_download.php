<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Five Mistakes Retailers Make When Selecting a New Software Solution</title>
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
	<link href="http://bizmartech.plugbytes.com/HTML/Epicor-1/css/styles.css" rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand">
				<img src="http://bizmartech.plugbytes.com/HTML/Epicor-1/images/epicor-logo-footer.png" alt="" />
			</a>
		</div>
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav"></ul>
		</div>
	</div>
</nav>
<div class="container"><br/>
	<div class="row">
		<div class="col-sm-12" style="min-height:400px">
            <h1>Five Mistakes Retailers Make When Selecting a New Software Solution </h1>
          	<p>Thank you for your interest in Five Mistakes Retailers Make When Selecting a New Software Solution. To access your download click the link below.</p>
        	<p class="filedwnld"><a href="http://www.mqltosql.com/HTML/Epicor-1/images/epicor-5-mistakes-retailers-make-wp-ens-0615.pdf">Click here to download your document</a></p>
	    </div>
	</div>
</div>
<footer class="footer">
	<div class="container">
		<p class="text-muted">© Copyright 2016, Epicor, All rights reserved.</p>
	</div>
</footer>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</body>
</html>
