<!-- Page Content -->
    <div class="container" style="margin-top: 30px">

        <!-- Marketing Icons Section -->
        <div class="row">
             <div class="col-md-6">
                    <div class="box">
                        <div class="box-content">
                             <h3 style="color:#027dc3; font-weight:bold"><strong>5 Creative Ideas to Propel Your Webinars</strong> </h3>
                             <div class="row">
      <div class="col-md-4">
        <img class="img-responsive" src="<?php echo HTTP_CAMPAIGN_IMAGES_PATH;?>5-creative246x316.jpg"></div>
      <div class="col-md-8">
     <p> Learn the key elements that go into producing, promoting and delivering successful webinars that deliver great results.</p>

<p> This white paper will share five out-there ideas for delivering truly memorable webinars, including:</p>
  <ul style="color:#515c66; margin-left:-27px">
                    <li>How gamification can make complex data digestible and fun</li>
                <li>Simple ways to add engaging video to your webinars</li>
                <li>informal and interactive webinar formats</li>
          </ul>
      </div>
      </div>
                        </div>
                    </div>
                    <br/><br/>
                    <div class="box">
                        <div class="box-content">
                             <h3 style="color:#027dc3; font-weight:bold"><strong>ABOUT ON24, INC.</strong> </h3>
                             

     <p style="line-height: 25px">ON24 is the leading webinar marketing platform for demand generation, lead
qualification and customer engagement. Its award-winning, patented, cloudbased
platform enables companies of all sizes to deliver engaging live and ondemand
webinars. Providing industry-leading analytics that can be integrated with
all leading marketing automation and CRM platforms, ON24 enables marketers
to optimize demand generation, enhance lead qualification and accelerate sales
pipeline opportunities.</p>

<p style="line-height: 25px"> Additional applications for the ON24 product portfolio include virtual training,
talent development and town hall meetings. More than 1,000 enterprises rely on
ON24, including IBM, CA Technologies, Merck, JPMorgan Chase, Deloitte, Credit
Suisse and SAP. The company is headquartered in San Francisco, with offices
throughout the world. For more information, visit ON24.com.</p>

    
                        </div>
                    </div>
                    
                </div>
            <div class="col-md-6">
            <div class="well well-sm formbg">
                <form class="form-horizontal">
  <fieldset>
    <legend>Login</legend>
    <div class="form-group">
      <label for="inputEmail" class="col-md-3 control-label">Email<span class="text-danger">*</span> </label>
      <div class="col-md-9">
        <input class="form-control" id="email" placeholder="Email" type="text">
      </div>
      <div id="msg" style="padding-left: 156px;"></div>
    </div>
    <div class="form-group">
      <div class="col-md-9 col-md-offset-3">
       <button type="button" onclick="validate()" class="btn btn-danger">View Content</button>
      </div>
    </div>
  </fieldset>
</form>

<br/>

<form class="form-horizontal" method="POST" action="submitData">
<input type="hidden" name="campId" value="<?php echo $campId?>">
      <input type="hidden" name="contentId" value="<?php echo $contentId?>">
      <input type="hidden" name="source" value="<?php echo $source?>">
      <input type="hidden" name="email" value="<?php echo $email?>">
  <fieldset>
    <legend>Sign Up</legend>
    <div class="form-group">
      <label for="inputEmail" class="col-md-3 control-label">Email<span class="text-danger">*</span> </label>
      <div class="col-md-9">
        <input class="form-control" id="inputEmail" name="email" placeholder="Email" type="text" value="<?php echo $email?>">
      </div>
    </div>
    <div class="form-group">
      <label for="inputEmail" class="col-md-3 control-label">First Name<span class="text-danger">*</span> </label>
      <div class="col-md-9">
        <input class="form-control" id="f_name" name="f_name" placeholder="First Name" type="text">
      </div>
    </div>
    <div class="form-group">
      <label for="inputEmail" class="col-md-3 control-label">Last Name<span class="text-danger">*</span> </label>
      <div class="col-md-9">
        <input class="form-control" id="l_name" name="l_name" placeholder="Last Name" type="text">
      </div>
    </div>
    <div class="form-group">
      <label for="inputEmail" class="col-md-3 control-label">Company<span class="text-danger">*</span> </label>
      <div class="col-md-9">
        <input class="form-control" id="Company" name="company" placeholder="Company" type="text">
      </div>
    </div>
    <div class="form-group">
      <label for="select" class="col-md-3 control-label">Job Title <span class="text-danger">*</span> </label>
      <div class="col-md-9">
        <select class="form-control" name="job_title">
    <option value="" selected="selected">Choose one</option>
    <option value="Senior Management (SVP/GM/Director)">Senior Management (SVP/GM/Director)</option>
    <option value="Executive Management (Chairman/CEO/CFO)">Executive Management (Chairman/CEO/CFO)</option>
    <option value="CIO/CTO/CSO">CIO/CTO/CSO</option>
    <option value="Agency Media Buyer/Director">Agency Media Buyer/Director</option>
    <option value="Marketing Manager/Professional">Marketing Manager/Professional</option>
    <option value="PR Specialist/Director">PR Specialist/Director</option>
    <option value="Search Marketing Professional/Manager">Search Marketing Professional/Manager</option>
    <option value="Online Marketer">Online Marketer</option>
    <option value="Chief Marketing Officer">Chief Marketing Officer</option>
    <option value="Marketing Vice President">Marketing Vice President</option>
    <option value="Other IS/IT Professional">Other IS/IT Professional</option>
    <option value="Other">Other</option>
    <option value="Retired/Unemployed">Retired/Unemployed</option>
    <option value="Student">Student</option>
</select>
        
      </div>
    </div>
    
    <div class="form-group">
      <label for="select" class="col-md-3 control-label">Job title level <span class="text-danger">*</span> </label>
      <div class="col-md-9">
        <select class="form-control" name="job_title_level">
        <option value="" selected="selected">Choose one</option><option value="C-Level">C-Level</option><option value="VP">VP</option><option value="Director">Director</option><option value="Manager">Manager</option><option value="Staff">Staff</option><option value="Owner">Owner</option></select>
        
      </div>
    </div>
    <div class="form-group">
      <label for="inputEmail" class="col-md-3 control-label">Phone<span class="text-danger">*</span> </label>
      <div class="col-md-9">
        <input class="form-control" id="phone" name="phone" placeholder="Phone" type="text">
      </div>
    </div>
    <div class="form-group">
      <label for="inputEmail" class="col-md-3 control-label">Address<span class="text-danger">*</span> </label>
      <div class="col-md-9">
        <input class="form-control" id="address" name="address" placeholder="Address" type="text">
      </div>
    </div>
    <div class="form-group">
      <label for="inputEmail" class="col-md-3 control-label">City<span class="text-danger">*</span> </label>
      <div class="col-md-9">
        <input class="form-control" id="city" name="city" placeholder="City" type="text">
      </div>
    </div>
    <div class="form-group">
      <label for="select" class="col-md-3 control-label">State<span class="text-danger">*</span> </label>
      <div class="col-md-9">
        <select class="form-control" name="state">
    <option value="" selected="selected">Choose one</option>
    <option value="AL">AL</option>
    <option value="AK">AK</option>
    <option value="AZ">AZ</option>
    <option value="AR">AR</option>
    <option value="CA">CA</option>
    <option value="CO">CO</option>
    <option value="CT">CT</option>
    <option value="DE">DE</option>
    <option value="FL">FL</option>
    <option value="GA">GA</option>
    <option value="HI">HI</option>
    <option value="ID">ID</option>
    <option value="IL">IL</option>
    <option value="IN">IN</option>
    <option value="IA">IA</option>
    <option value="KS">KS</option>
    <option value="KY">KY</option>
    <option value="LA">LA</option>
    <option value="ME">ME</option>
    <option value="MD">MD</option>
    <option value="MA">MA</option>
    <option value="MI">MI</option>
    <option value="MN">MN</option>
    <option value="MS">MS</option>
    <option value="MO">MO</option>
    <option value="MT">MT</option>
    <option value="NE">NE</option>
    <option value="NV">NV</option>
    <option value="NH">NH</option>
    <option value="NJ">NJ</option>
    <option value="NM">NM</option>
    <option value="NY">NY</option>
    <option value="NC">NC</option>
    <option value="ND">ND</option>
    <option value="OH">OH</option>
    <option value="OK">OK</option>
    <option value="OR">OR</option>
    <option value="PA">PA</option>
    <option value="RI">RI</option>
    <option value="SC">SC</option>
    <option value="SD">SD</option>
    <option value="TN">TN</option>
    <option value="TX">TX</option>
    <option value="UT">UT</option>
    <option value="VT">VT</option>
    <option value="VA">VA</option>
    <option value="WA">WA</option>
    <option value="WV">WV</option>
    <option value="WI">WI</option>
    <option value="WY">WY</option>
</select>
        
      </div>
    </div>
    <div class="form-group">
      <label for="inputEmail" class="col-md-3 control-label">Zip<span class="text-danger">*</span> </label>
      <div class="col-md-9">
        <input class="form-control" id="zip" name="zip" placeholder="Zip" type="text">
      </div>
    </div>
    
    <div class="form-group">
      <label for="select" class="col-md-3 control-label">Country<span class="text-danger">*</span> </label>
      <div class="col-md-9">
        <select class="form-control" name="country"><option value="" selected="selected">Choose one</option><option value="AL">AL</option><option value="AK">AK</option><option value="AZ">AZ</option><option value="AR">AR</option><option value="CA">CA</option><option value="CO">CO</option><option value="CT">CT</option><option value="DE">DE</option><option value="FL">FL</option><option value="GA">GA</option><option value="HI">HI</option><option value="ID">ID</option><option value="IL">IL</option><option value="IN">IN</option><option value="IA">IA</option><option value="KS">KS</option><option value="KY">KY</option><option value="LA">LA</option><option value="ME">ME</option><option value="MD">MD</option><option value="MA">MA</option><option value="MI">MI</option><option value="MN">MN</option><option value="MS">MS</option><option value="MO">MO</option><option value="MT">MT</option><option value="NE">NE</option><option value="NV">NV</option><option value="NH">NH</option><option value="NJ">NJ</option><option value="NM">NM</option><option value="NY">NY</option><option value="NC">NC</option><option value="ND">ND</option><option value="OH">OH</option><option value="OK">OK</option><option value="OR">OR</option><option value="PA">PA</option><option value="RI">RI</option><option value="SC">SC</option><option value="SD">SD</option><option value="TN">TN</option><option value="TX">TX</option><option value="UT">UT</option><option value="VT">VT</option><option value="VA">VA</option><option value="WA">WA</option><option value="WV">WV</option><option value="WI">WI</option><option value="WY">WY</option></select>
        
      </div>
    </div>
    <div class="form-group">
      <label for="select" class="col-md-3 control-label">Industry<span class="text-danger">*</span> </label>
      <div class="col-md-9">
        <select class="form-control" name="industry">
    <option value="" selected="selected">Choose one</option>
    <option value="Advertising/Media/Publishing">Advertising/Media/Publishing</option>
    <option value="Aerospace and Defense">Aerospace and Defense</option>
    <option value="Agriculture and Forestry">Agriculture and Forestry</option>
    <option value="Automotive">Automotive</option>
    <option value="Banking/Accounting/Financial">Banking/Accounting/Financial</option>
    <option value="Business Services">Business Services</option>
    <option value="Consulting">Consulting</option>
    <option value="Education and Training">Education and Training</option>
    <option value="Energy/Chemical/Utilities">Energy/Chemical/Utilities</option>
    <option value="Engineering/Construction/Facilities">Engineering/Construction/Facilities</option>
    <option value="Entertainment/Travel/Hospitality">Entertainment/Travel/Hospitality</option>
    <option value="Environmental">Environmental</option>
    <option value="Food and Beverage">Food and Beverage</option>
    <option value="Government-Federal">Government-Federal</option>
    <option value="Government-State and Local">Government-State and Local</option>
    <option value="Healthcare">Healthcare</option>
    <option value="High Tech-Hardware">High Tech-Hardware</option>
    <option value="High Tech-Software">High Tech-Software</option>
    <option value="Insurance">Insurance</option>
    <option value="Legal">Legal</option>
    <option value="Manufacturing">Manufacturing</option>
    <option value="Non Profit">Non Profit</option>
    <option value="Other">Other</option>
    <option value="Pharmaceuticals and Biotechnology">Pharmaceuticals and Biotechnology</option>
    <option value="Real Estate">Real Estate</option>
    <option value="Retail and Wholesale">Retail and Wholesale</option>
    <option value="Staffing Services">Staffing Services</option>
    <option value="Telecommunications">Telecommunications</option>
    <option value="Transportation and Shipping">Transportation and Shipping</option>
    <option value="VAR/VAD/System Integrator">VAR/VAD/System Integrator</option>
</select>
        
      </div>
    </div>
    <div class="form-group">
      <label for="select" class="col-md-3 control-label">Department<span class="text-danger">*</span> </label>
      <div class="col-md-9">
        <select class="form-control" name="department">
        <option value="" selected="selected">Choose one</option>
        <option value="Academic">Academic</option>
        <option value="Accounting/Finance/Legal">Accounting/Finance/Legal</option>
        <option value="Administrative/Operations">Administrative/Operations</option>
        <option value="Engineering/Research">Engineering/Research</option>
        <option value="Executive Management">Executive Management</option>
        <option value="Human Resources">Human Resources</option>
        <option value="IT-Database Admin">IT-Database Admin</option>
        <option value="IT-General">IT-General</option>
        <option value="IT-Help Desk">IT-Help Desk</option>
        <option value="IT-Mobile App">IT-Mobile App</option>
        <option value="IT-Network">IT-Network</option>
        <option value="IT-Project Management">IT-Project Management</option>
        <option value="IT-Security">IT-Security</option>
        <option value="IT-Software Development">IT-Software Development</option>
        <option value="IT-Support">IT-Support</option>
        <option value="IT-Sys Admin">IT-Sys Admin</option>
        <option value="IT-Telecom">IT-Telecom</option>
        <option value="IT-Web">IT-Web</option>
        <option value="Marketing/eCommerce">Marketing/eCommerce</option>
        <option value="Marketing/PR/Marcom">Marketing/PR/Marcom</option>
        <option value="Marketing/Product Marketing">Marketing/Product Marketing</option>
        <option value="Marketing/Web">Marketing/Web</option>
        <option value="Other">Other</option>
        <option value="Sales/Business Development">Sales/Business Development</option>
        <option value="Training">Training</option></select>
      </div>
    </div>
    <div class="form-group">
      <label for="select" class="col-md-3 control-label">Company size<span class="text-danger">*</span> </label>
      <div class="col-md-9">
        <select class="form-control" name="company_size">
        <option value="" selected="selected">Choose one</option>
        <option value="0-20">0-20</option>
        <option value="21-50">21-50</option>
        <option value="51-100">51-100</option>
        <option value="101-250">101-250</option>
        <option value="251-500">251-500</option>
        <option value="501-1000">501-1000</option>
        <option value="1001-2500">1001-2500</option>
        <option value="2501-5000">2501-5000</option>
        <option value="5001-7500">5001-7500</option>
        <option value="7501-10000">7501-10000</option>
        <option value="10001+">10001+</option></select>
      </div>
    </div>
    
    <div class="form-group">
      <div class="col-md-9 col-md-offset-3">
        <button type="submit" class="btn btn-danger">View Content</button>
      </div>
    </div>
  </fieldset>
</form>
</div>
            </div>
            
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
      <hr>
  
  <!-- Footer -->
  <footer>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="text-center">
        <img src="<?php echo HTTP_CAMPAIGN_IMAGES_PATH;?>on24-logo.png">
        <p> ON24, INC.
          201 THIRD STREET SAN FRANCISCO, CA 94103
          877.202.9599 ON24.COM </p>
          <p> © 2015 ON24, Inc. For more information on the benefits of the ON24 virtual environment, contact us at 877-202-9599 or visit www.on24.com. </p>
        </div>
      </div>
    </div>
    </div>
  </footer>