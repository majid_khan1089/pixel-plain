<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Admin Panel</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link href="<?php echo HTTP_CSS_PATH_ADMIN; ?>bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo HTTP_CSS_PATH_ADMIN; ?>AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?php echo HTTP_PLUG_PATH_ADMIN; ?>iCheck/square/blue.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition login-page">
    <div class="login-box">
      <div class="login-logo">
        <a href="<?php echo base_url(); ?>"><b>Admin</b> Login</a>
      </div><!-- /.login-logo -->
      <div class="login-box-body">
        <p class="login-box-msg"><?php echo lang('forgot_password_heading');?></p>

        <?php if(isset($message) && !empty($message)) { ?>
          <div class="alert alert-danger alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
            <?php echo $message;?>
          </div>
        <?php } ?>

        <?php echo form_open("auth/forgot_password");?>

        	<div class="form-group has-feedback">
        		<label for="identity" class="control-label">
					<?php echo (($type=='email') ? sprintf(lang('forgot_password_email_label'), $identity_label) : sprintf(lang('forgot_password_identity_label'), $identity_label));?>
				</label>
				<input class="form-control" placeholder="E-mail" name="identity" type="text" autofocus>
				<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
			</div>

			<div class="row">
          		<div class="col-xs-4">
              		<?php echo form_submit('submit', lang('forgot_password_submit_btn'), array("class" => "btn btn-primary btn-block btn-flat"));?>
            	</div>
          	</div>

		<?php echo form_close();?>
        
      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

    <!-- jQuery 2.1.4 -->
    <script src="<?php echo HTTP_PLUG_PATH_ADMIN; ?>jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?php echo HTTP_JS_PATH_ADMIN; ?>bootstrap.min.js"></script>
    <!-- iCheck -->
    <script src="<?php echo HTTP_PLUG_PATH_ADMIN; ?>iCheck/icheck.min.js"></script>
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>
  </body>
</html>