<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>The Competitive Advantage Of Mobility</title>
        <!-- Styles -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
        <link href="http://www.mqltosql.com/HTML/Epicor-1/css/styles.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>
        <style type="text/css">
            input[type=submit] {
          background:url(http://www.mqltosql.com/HTML/Epicor-1/images/form-button-submit.jpg);
          color: transparent;
          width: 135px;
          height: 38px;
        }
        .error{
            color: red;
            font-weight: normal;
        }
        </style>
    </head>
    <body>
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand">
                    <img src="http://www.mqltosql.com/HTML/Epicor-1/images/epicor-logo-footer.png"/>
                    </a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav"></ul>
                </div>
            </div>
        </nav>
        <div class="container">
            <br />
            <div class="row">
                <div class="col-sm-9">
                    <div class="form-page-content">
                        <input id="categoryid" name="categoryid" type="hidden" value="477"/>
                        <h1>Five Mistakes Retailers Make When Selecting a New Software Solution</h1>
                        <div class="asset-image"><img src="http://www.mqltosql.com/email-template/HTML/Epicor-1/images/p2.png" /></div>
                        <p style="font-size:16px;line-height:27px">	With the immense changes the retail industry has undergone and continues to face today, there are mistakes that you as a wholesaler simply can&rsquo;t afford to make. Certainly, one of these&mdash;perhaps the greatest of all in our technology-driven age&mdash;is choosing the wrong retail business management software for your business.</p>
<p style="font-size:16px;line-height:27px">	Our guide describes the five mistakes retailers make when selecting a software solution, including not involving the head of the organization in the decision-making process and skipping the planning phase because &ldquo;it takes too much time.&rdquo;</p>
<p style="font-size:16px;line-height:27px"> Retailers decide to replace their POS systems for a variety of reasons, but they usually boil down to this one: does the current system support and enable business strategies that will make the company successful? Or does the system hold it back? </p>
<ul style="font-size:16px;line-height:27px">
<li> Not involving the head of the organization in the decision-making process</li>
<li>Skipping the planning phase (because it "takes too much time")</li>
<li>Considering a technology provider that doesn't truly understand your industry</li>
<li>Assuming you will do things exactly as you do them today</li>
<li>Selecting a vendor that lacks a long-term product vision</li>
</ul>                                                
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="panel panel-primary reg-form">
                        <div class="dnfrmc full" id="dnfrmmask">
                            <form class="autoform" id="epicorform" method="Post" name="epicorform">
                                <input type="hidden" name="form-type" value="epicor_1_form">
                                <div class="reg-form-panel">
                                    <div id="leadform-error"></div>
                                    <p class="intro">Please fill in the information below to access this resource:</p>
                                    <div class="form-group" id="question1-group">
                                        <label class="sr-only" for="question1">First Name</label>
                                        <input type="text" class="form-control input-sm lfreq tblfreq" id="f_name" name="f_name" maxlength="50"  placeholder="First Name" required />
                                    </div>
                                    <div class="form-group" id="question2-group">
                                        <label class="sr-only" for="question2">Last Name</label>
                                        <input type="text" class="form-control input-sm lfreq tblfreq" id="l_name" name="l_name" maxlength="50"  placeholder="Last Name" required />
                                    </div>
                                    <div class="form-group" id="question8-group">
                                        <label class="sr-only" for="question8">Email Address</label>
                                        <input type="text" class="form-control input-sm lfreq tblfreq email" id="email" name="email" maxlength="100"  placeholder="Email Address" required/>
                                    </div>
                                    <div class="form-group" id="question9-group">
                                        <label class="sr-only" for="question9">Telephone</label>
                                        <input type="text" class="form-control input-sm lfreq tblfreq tel" id="phone" name="phone" maxlength="50"  placeholder="Telephone" required />
                                    </div>
                                    <div class="form-group" id="question3-group">
                                        <label class="sr-only" for="question3">Company Name</label>
                                        <input type="text" class="form-control input-sm lfreq tblfreq" id="company" name="company" maxlength="80"  placeholder="Company Name" required/>
                                    </div>
                                    <div class="form-group" id="question110-group">
                                        <label class="sr-only" for="question110">Company Size</label>
                                        <select class="form-control input-sm lfreq " required="required" id="company_size" name="company_size" placeholder="Company Size">
                                            <option value="">Select Company Size</option>
                                            <option value="1 to 3">1 to 3</option>
                                            <option value="4 to 10">4 to 10</option>
                                            <option value="11 to 25">11 to 25</option>
                                            <option value="26 to 50">26 to 50</option>
                                            <option value="51 to 100">51 to 100</option>
                                            <option value="101 to 200">101 to 200</option>
                                            <option value="201 to 500">201 to 500</option>
                                            <option value="501 to 1000">501 to 1000</option>
                                            <option value="1001 or more">1001 or more</option>
                                        </select>
                                    </div>
                                    <div class="form-group" id="question141-group">
                                        <label class="sr-only" for="question141">Job Function</label>
                                        <select class="form-control input-sm lfreq " required="required" id="job_title_level" name="job_title_level" placeholder="Job Function">
                                            <option value="">Select Job Function</option>
                                            <option value="IT Management(Manager IS/IT)">IT Management(Manager IS/IT)</option>
                                            <option value="Senior Non IT Management(CEO,CFO,VP,Director)">Senior Non IT Management(CEO,CFO,VP,Director)</option>
                                            <option value="Senior IT Management(CIO/CTO/VP/Director)">Senior IT Management(CIO/CTO/VP/Director)</option>
                                            <option value="Network Management/Administration">Network Management/Administration</option>
                                            <option value="IT Staff">IT Staff</option>
                                            <option value="CSO">CSO</option>
                                            <option value="Product Management">Product Management</option>
                                            <option value="Sales/Marketing">Sales/Marketing</option>
                                            <option value="Telecommunications Manager">Telecommunications Manager</option>
                                            <option value="Systems Management/Administration">Systems Management/Administration</option>
                                            <option value="Project Manager">Project Manager</option>
                                            <option value="Programmer/Developer">Programmer/Developer</option>
                                            <option value="Privacy Officer">Privacy Officer</option>
                                            <option value="Consultant/Systems Integrator">Consultant/Systems Integrator</option>
                                            <option value="Database Administrator">Database Administrator</option>
                                            <option value="Educator">Educator</option>
                                            <option value="Non IT Staff">Non IT Staff</option>
                                            <option value="Architect">Architect</option>
                                            <option value="Application Manager">Application Manager</option>
                                            <option value="Analyst">Analyst</option>
                                        </select>
                                    </div>
                                    <div class="form-group" id="question113-group">
                                        <label class="sr-only" for="question113">Industry</label>
                                        <select class="form-control input-sm lfreq " required="required" id="industry" name="industry" placeholder="Industry">
                                            <option value="">Select Industry</option>
                                            <option value="Advertising/Marketing">Advertising/Marketing</option>
                                            <option value="Automotive">Automotive</option>
                                            <option value="Agriculture/Forestry">Agriculture/Forestry</option>
                                            <option value="Business Services">Business Services</option>
                                            <option value="Consulting">Consulting</option>
                                            <option value="Distribution">Distribution</option>
                                            <option value="Entertainment">Entertainment</option>
                                            <option value="Education">Education</option>
                                            <option value="Engineering/Construction">Engineering/Construction</option>
                                            <option value="Financial">Financial</option>
                                            <option value="Food and Beverage">Food and Beverage</option>
                                            <option value="Government-Federal">Government-Federal</option>
                                            <option value="Government-State and Local">Government-State and Local</option>
                                            <option value="Healthcare/Medical">Healthcare/Medical</option>
                                            <option value="Hospitality">Hospitality</option>
                                            <option value="Insurance">Insurance</option>
                                            <option value="Legal">Legal</option>
                                            <option value="Manufacturing">Manufacturing</option>
                                            <option value="Non-profit">Non-profit</option>
                                            <option value="Real-Estate">Real-Estate</option>
                                            <option value="Retail">Retail</option>
                                            <option value="Student/Retired/Non-Business">Student/Retired/Non-Business</option>
                                            <option value="Transportation/Shipping">Transportation/Shipping</option>
                                            <option value="Technology">Technology</option>
                                            <option value="Telecommunications">Telecommunications</option>
                                            <option value="Utilities">Utilities</option>
                                            <option value="Warehousing">Warehousing</option>
                                            <option value="Other">Other</option>
                                        </select>
                                    </div>
                                    <div class="form-group" id="question7-group">
                                        <label class="sr-only" for="question7">Zip Code</label>
                                        <input type="text" class="form-control input-sm lfreq tblfreq zip" id="zip" name="zip" maxlength="5" placeholder="Zip Code" required="required"/>
                                    </div>
                                    <div class="form-group" id="question224-group">
                                        <label style="font-weight:normal">Please Indicate your current Point of Sale software</label>
                                        <select class="form-control input-sm lfreq " id="point_of_sale_software" name="point_of_sale_software" placeholder=": Indicate your current Point of Sale software" required="required">
                                            <option value="">Select</option>
                                            <option value="AXIS Retail Management System, by AcuSport">AXIS Retail Management System, by AcuSport</option>
                                            <option value="Catapult, by ECRS">Catapult, by ECRS</option>
                                            <option value="Celerant Command Retail, by Celerant Technology">Celerant Command Retail, by Celerant Technology</option>
                                            <option value="CounterPoint, by NCR">CounterPoint, by NCR</option>
                                            <option value="Microsoft Dynamics AX, by Microsoft">Microsoft Dynamics AX, by Microsoft</option>
                                            <option value="Microsoft Dynamics RMS, by Microsoft">Microsoft Dynamics RMS, by Microsoft</option>
                                            <option value="NetSuite, by Netsuite Inc.">NetSuite, by Netsuite Inc.</option>
                                            <option value="Paladin POS, by Paladin Data Corporation">Paladin POS, by Paladin Data Corporation</option>
                                            <option value="Retail Pro, by Retail Pro International">Retail Pro, by Retail Pro International</option>
                                            <option value="RetailSTAR Point of Sale software, by CAM">RetailSTAR Point of Sale software, by CAM</option>
                                            <option value="RMS Pharmacy POS, RM">RMS Pharmacy POS, RM</option>
                                            <option value="RockSolid, by ECi Software Solutions">RockSolid, by ECi Software Solutions</option>
                                            <option value="QuickBooks">QuickBooks</option>
                                            <option value="Other">Other</option>
                                        </select>
                                    </div>
                                    <p class="buttons text-center" >
                                    <input type="submit">
                                       <!--  <a href="download-page.html" onclick="document.getElementById('epicorform').submit();">
                                            <img src="http://bizmartech.plugbytes.com/HTML/Epicor/images/form-button-submit.jpg" width="135" height="36">
                                        </a>  -->
                                    </p>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <footer class="footer">
            <div class="container">
                <p class="text-muted">© Copyright 2016, Epicor, All rights reserved.</p>
            </div>
        </footer>

        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
        <script type="text/javascript">
            $("#epicorform").validate({
            /*rules: {
                f_name: "required",                
            },
            messages: {
                f_name: "Please enter your firstname"               
            }*/
        });
        </script>
    </body>
</html>