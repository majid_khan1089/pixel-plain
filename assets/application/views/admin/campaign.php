<?php $this->load->view('admin/common/header'); ?>
<div class="content-wrapper">
    <div class="container">
        <section class="content-header">
            <h1></h1>
            <br/>
            <ol class="breadcrumb">
                <li><a href="javascript:void(0);"><i class="fa fa-dashboard"></i>   <?php echo ($isNew) ? 'Add Campaign' : 'Edit Campaign'; ?></a></li>
            </ol>
        </section>
        <section class="content">
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Campaign Modification</h3>
                </div>
                <div class="box-body">
                    <div class="row">

                        <div class="col-md-10 col-md-offset-1">
                            <form id="campaign_add" action="<?php echo (isset($campaign->campaign_id) && !empty($campaign->campaign_id)) ? '' : 'add_campaign'; ?>" method="POST" role="form" data-toggle="validator" class="form-horizontal">
                                
                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="username">Campaign Name</label>
                                    <div class="col-md-10">
                                        <input type="text" required placeholder="Enter Campaign Name" name="camp_name" value="<?php echo isset($campaign->campaign_name) ? set_value("camp_name", ucfirst($campaign->campaign_name)) : set_value("camp_name"); ?>" id="camp_name" class="form-control form" <?php echo ($isNew) ? '' : 'readonly'; ?>>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="username">Campaign Type</label>
                                    <div class="col-md-10">
                                        <select name="campaign_type" class="form-control">
                                            <option value="email" <?php echo (!$isNew && $campaign->campaign_type == "email") ? 'selected="selected"' : '' ;?>>Email</option>
                                            <option value="registration" <?php echo (!$isNew && $campaign->campaign_type == "registration") ? 'selected="selected"' : '' ;?>>Registration</option>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="url_type">Link Type</label>
                                    <div class="col-md-10">
                                        <select name="url_type" class="form-control">
                                            <option value="pdf" <?php echo (!$isNew && $campaign->url_type == "pdf") ? 'selected="selected"' : '' ;?>>PDF</option>
                                            <option value="video" <?php echo (!$isNew && $campaign->url_type == "video") ? 'selected="selected"' : '' ;?>>VIDEO</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="username">URL PDF/Video</label>
                                    <div class="col-md-10">
                                        <input type="text" required placeholder="Enter Pdf link" name="pdf_url" value="<?php echo isset($campaign->pdf_url) ? set_value("pdf_url", $campaign->pdf_url) : set_value("pdf_url"); ?>" id="pdf_url" class="form-control form">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="username">Mail Subject</label>
                                    <div class="col-md-10">
                                        <input type="text" required placeholder="Enter Mail Subject" name="mail_subject" value="<?php echo isset($campaign->pdf_url) ? set_value("mail_subject", $campaign->mail_subject) : set_value("mail_subject"); ?>" id="mail_subject" class="form-control form">
                                    </div>
                                </div>
                                
                                <div class="form-group hide">
                                    <label class="col-md-2 control-label" for="username">Slug Landing Page</label>
                                    <div class="col-md-10">
                                        <input type="text" required placeholder="Enter Landing Page Slug" name="landing_page_slug" value="<?php echo isset($campaign->landing_page_slug) ? set_value("landing_page_slug", $campaign->landing_page_slug) : set_value("landing_page_slug"); ?>" id="landing_page_slug" class="form-control form" <?php echo ($isNew) ? '' : 'readonly'; ?>>
                                       
                                    </div>
                                </div>
                                <?php if(!$isNew) { ?>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label" for="username">Page Path</label>
                                        <div class="col-md-10">                                    
                                            <p style="margin-top:6px;">Mail Template Path : <a href="javascript:void(0);" style="color:purple">views/mailtemplate/<?php echo $campaign->campaign_slug; ?></a></p>
                                            <?php if($campaign->campaign_type == "registration") { ?>
                                            <p>Landing Page Path : <a href="javascript:void(0);" style="color:blue">views/admin/<?php echo $campaign->landing_page_slug; ?></a></p>
                                            <?php } ?>
                                        </div>
                                    </div>
                                <?php } ?>
                                
                                <div class="form-group">
                                    <div class="col-sm-12 text-right">
                                        <button class="btn btn-primary" type="submit"><?php echo (isset($campaign->campaign_id) && !empty($campaign->campaign_id)) ? 'Update' : 'Submit'; ?></button>
                                        <a class="btn btn-info" href="<?php echo base_url('campaign'); ?>">Cancel</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<?php $this->load->view('admin/common/footer'); ?>