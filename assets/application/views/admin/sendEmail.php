<?php $this->load->view('admin/common/header'); ?>
<div class="content-wrapper">
    <div class="container">
        <section class="content-header">
            <h1></h1>
            <br/>
            <ol class="breadcrumb">
                <li><a href="javascript:void(0);"><i class="fa fa-dashboard"></i> Send Mail</a></li>
            </ol>
        </section>
        <section class="content">
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Send Email</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <?php $this->load->view('admin/common/flash_message'); ?>
                        </div>
                        <br/>         
                    </div>
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <form id="send_campaign" method="POST" role="form" data-toggle="validator" class="form-horizontal" action="<?php echo base_url();?>campaign/sendCampaignMail">
                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="email">Campagin Type</label>
                                    <div class="col-md-10">
                                        <select name="email_type" class="form-control">
                                            <?php if(!empty($listings)) {
                                                foreach ($listings as $campaignKey => $campaignValue) { ?>
                                            <option value="<?php echo $campaignValue->campaign_slug; ?>"><?php echo $campaignValue->campaign_name; ?></option>
                                            <?php } } else {  ?>
                                            <option value="">No New Campaign Added Yet</option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="email">Prospect's Email</label>
                                    <div class="col-md-10">
                                        <input type="text" required placeholder="Enter Prospect's Email" name="p_name" class="form-control form">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12 text-right">
                                        <button class="btn btn-primary" type="submit"><?php echo (isset($content->client_id) && !empty($content->client_id)) ? 'Update' : 'Submit'; ?></button>
                                        <a class="btn btn-info" href="<?php echo base_url('admin/clients'); ?>">Cancel</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<?php $this->load->view('admin/common/footer'); ?>