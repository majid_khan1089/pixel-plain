<?php $this->load->view('admin/common/header'); ?>
<div class="content-wrapper">
    <div class="container">
        <section class="content-header">
            <h1></h1>
            <br/>
            <ol class="breadcrumb">
                <li><a href="javascript:void(0);"><i class="fa fa-dashboard"></i>   <?php echo ($isNew) ? 'Add Campaign' : 'Edit Campaign'; ?></a></li>
            </ol>
        </section>
        <section class="content">
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">User Modification</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <form id="campaign_add" method="POST" role="form" data-toggle="validator" class="form-horizontal">
                                
                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="first_name">First Name</label>
                                    <div class="col-md-10">
                                        <input type="text" required placeholder="Enter First Name" name="first_name" value="<?php echo isset($userDetails->first_name) ? set_value("first_name", ucfirst($userDetails->first_name)) : set_value("first_name"); ?>" id="first_name" class="form-control form">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="first_name">Last Name</label>
                                    <div class="col-md-10">
                                        <input type="text" required placeholder="Enter Last Name" name="last_name" value="<?php echo isset($userDetails->last_name) ? set_value("last_name", ucfirst($userDetails->last_name)) : set_value("last_name"); ?>" id="last_name" class="form-control form">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="first_name">Mobile</label>
                                    <div class="col-md-10">
                                        <input type="text" required placeholder="Enter Mobile Name" name="mobile" value="<?php echo isset($userDetails->mobile) ? set_value("mobile", ($userDetails->mobile)) : set_value("mobile"); ?>" id="mobile" class="form-control form">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="first_name">Email</label>
                                    <div class="col-md-10">
                                        <input type="email" required placeholder="Enter Email" name="email" value="<?php echo isset($userDetails->email) ? set_value("email", ucfirst($userDetails->email)) : set_value("email"); ?>" id="email" class="form-control form">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="address">Address</label>
                                    <div class="col-md-10">
                                        <input type="text" required placeholder="Enter First Name" name="address" value="<?php echo isset($userDetails->address) ? set_value("address", ucfirst($userDetails->address)) : set_value("address"); ?>" id="address" class="form-control form">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="city">city</label>
                                    <div class="col-md-10">
                                        <input type="text" required placeholder="Enter First city" name="city" value="<?php echo isset($userDetails->city) ? set_value("city", ucfirst($userDetails->city)) : set_value("city"); ?>" id="city" class="form-control form">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="country">country</label>
                                    <div class="col-md-10">
                                        <input type="text" required placeholder="Enter country" name="country" value="<?php echo isset($userDetails->country) ? set_value("country", ucfirst($userDetails->country)) : set_value("country"); ?>" id="country" class="form-control form">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-12 text-right">
                                        <button class="btn btn-primary" type="submit"><?php echo (isset($userDetails->user_id) && !empty($userDetails->user_id)) ? 'Update' : 'Submit'; ?></button>
                                        <a class="btn btn-info" href="<?php echo base_url('user/listing'); ?>">Cancel</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<?php $this->load->view('admin/common/footer'); ?>