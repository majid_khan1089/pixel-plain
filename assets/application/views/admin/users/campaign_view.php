<div class="row">
    <div class="col-md-12">
        <form id="campaign_add" method="POST" role="form" data-toggle="validator" class="form-horizontal">
            <div class="form-group">
                <label class="col-md-3 control-label" for="username">Campaign Type</label>
                <div class="col-md-9">
                    <select name="campaign_type" class="form-control">
                        <?php if(!empty($listings)) {
                            foreach ($listings as $campaignKey => $campaignValue) { ?>
                        <option value="<?php echo $campaignValue->campaign_slug; ?>"><?php echo $campaignValue->campaign_name; ?></option>
                        <?php } } else {  ?>
                        <option value="">No New Campaign Added Yet</option>
                        <?php } ?>
                    </select>
                </div>
            </div>
        </form>
    </div>
</div>