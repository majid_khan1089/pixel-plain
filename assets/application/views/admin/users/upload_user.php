<?php $this->load->view('admin/common/header'); ?>
<div class="content-wrapper">
    <div class="container">
        <section class="content-header">
            <h1></h1>
            <br/>
            <ol class="breadcrumb">
                <li><a href="javascript:void(0);"><i class="fa fa-dashboard"></i> Send Mail</a></li>
            </ol>
        </section>
        <section class="content">
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Upload Users</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <?php $this->load->view('admin/common/flash_message'); ?>
                        </div>
                        <br/>         
                    </div>
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <form id="upload_user" method="POST" role="form" class="form-horizontal" action="<?php echo base_url('upload-user');?>" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="email">Select Campagin</label>
                                    <div class="col-md-10">
                                        <select name="campaign_name" class="form-control">
                                            <?php if(!empty($campaigns)) {
                                                foreach ($campaigns as $campaignKey => $campaignValue) { ?>
                                                <option value="<?php echo $campaignValue->campaign_slug; ?>"><?php echo $campaignValue->campaign_name; ?></option>
                                            <?php } } else {  ?>
                                            <option value="">No New Campaign Added Yet</option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="email">User CSV</label>
                                    <div class="col-md-10">
                                        <input style="padding-top: 0px;" type="file" required name="user_records" class="form-control form">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-12 text-right">
                                        <button class="btn btn-primary" type="submit"><?php echo (isset($content->client_id) && !empty($content->client_id)) ? 'Update' : 'Submit'; ?></button>
                                        <a class="btn btn-info" href="<?php echo base_url('admin/clients'); ?>">Cancel</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<?php $this->load->view('admin/common/footer'); ?>