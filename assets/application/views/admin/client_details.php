<?php
	$userDetails = unserialize($userData->dump_data);
	
	$oldKey = array('f_name','l_name', 'company_size', 'job_title_level', 'subscribe_offer',  '_');
	$newKey = array('First Name','Last Name', 'Company Size', 'Job Title Level', 'Subscribe Offer',' ',);
?>
<table class="table table-striped">
    <tbody>
    	<?php if(!empty($userDetails)) { ?>
	    	<?php foreach ($userDetails as $key => $value) { ?>
	    		<?php if($key != 'form-type') { ?>
		    		<tr>
		    	    	<td><strong><?php echo ucfirst(str_replace($oldKey,$newKey,$key));  ?></strong></td>
		    	    	<td valign="center"><?php echo ($value) ? $value : '-';?></td>
			      	</tr>
	    		<?php } ?>
    		<?php } ?>
    	<?php } else { ?>
    	<tr>
	    	<td colspan="1" align="center"><strong>No Records Found</strong></td>
	  	</tr>
	  	<?php } ?>
    </tbody>
</table>