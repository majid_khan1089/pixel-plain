<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Admin | Email</title>
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<link href="<?php echo HTTP_CSS_PATH_ADMIN; ?>bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<link rel="stylesheet" href="<?php echo HTTP_CSS_PATH_ADMIN; ?>AdminLTE.min.css">
<link rel="stylesheet" href="<?php echo HTTP_PLUG_PATH_ADMIN; ?>datatables/dataTables.bootstrap.css">
<link rel="stylesheet" href="<?php echo HTTP_CSS_PATH_ADMIN; ?>skins/_all-skins.min.css">
<link rel="stylesheet" href="<?php echo HTTP_CSS_PATH_ADMIN; ?>fileinput.min.css">

<link rel="stylesheet" href="<?php echo HTTP_CSS_PATH_ADMIN; ?>custom.css">
<script src="<?php echo HTTP_PLUG_PATH_ADMIN; ?>jQuery/jQuery-2.1.4.min.js"></script>
<script type="text/javascript">var baseUrl = "<?php echo base_url(); ?>"</script>
<script type="text/javascript">var editSLug = "<?php echo $this->uri->segment(2) ? '/'.$this->uri->segment(2) : ''; ?>"</script>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<style type="text/css">
<?php if($this->session->userdata('lang')== 'ar') { ?> 
#body {
    direction: rtl;
}
<?php } ?>
</style>