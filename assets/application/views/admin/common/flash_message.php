<?php if ($this->session->flashdata('success')) { ?>
<div class="alert alert-success">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>Success!</strong> <?php echo $this->session->flashdata('success') ?>
</div>
<?php } ?>

<?php if ($this->session->flashdata('info')) { ?>
<div class="alert alert-info">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>Error!</strong> <?php echo $this->session->flashdata('info') ?>
</div>
<?php } ?>

<?php if ($this->session->flashdata('danger')) { ?>
<div class="alert alert-danger">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>Success!</strong> <?php echo $this->session->flashdata('danger') ?>
</div>
<?php } ?>
