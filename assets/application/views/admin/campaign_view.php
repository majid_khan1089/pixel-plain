<table class="table table-striped">
    <tbody>
    	<?php if(!empty($campaign)) { ?>
    		<tr>
    	    	<td><strong>Mail Template Path :</strong></td>
    	    	<td valign="center"><a href="javascript:void(0);" style="color:purple">views/mailtemplate/<?php echo $campaign->campaign_slug; ?></a></td>
	      	</tr>

	      	<tr>
    	    	<td><strong>Landing Page Path :</strong></td>
    	    	<td valign="center"><a href="javascript:void(0);" style="color:blue">views/admin/<?php echo $campaign->landing_page_slug; ?></a></td>
	      	</tr>
	    	
    	<?php } else { ?>
    	<tr>
	    	<td colspan="1" align="center"><strong>No Records Found</strong></td>
	  	</tr>
	  	<?php } ?>
    </tbody>
</table>