<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model
{
 	var $table = 'campaign_users';
    var $column_order = array(null, 'first_name', 'last_name','email','mobile','status', 'campaign_type'); //set column field database for datatable orderable
    var $column_search = array('first_name','last_name','mobile','email','status', 'campaign_type'); //set column field database for datatable searchable 
    var $order = array('user_id' => 'asc'); // default order 
 
	public function __construct()
	{
		parent::__construct();
	}

	public function getCampaignUsers(){
		$this->db->select('*');
		$this->db->from('campaign_users');
		//$this->db->where('status', "1");
		return  $this->db->get()->result();
	}

	public function getCampaignUserByID($id){
		return $this->db->get_where('campaign_users', array('user_id' => $id))->row();
	}

    public function getCampaignUserByIDArray($ids){
        $this->db->select('*');
        $this->db->from('campaign_users');
        $this->db->where_in('user_id', $ids);
        return  $this->db->get()->result();
    }

	public function deleteCampaignUser($id){

		if(is_array($id)){
			$this->db->where_in('user_id', $id);
		}else{
			$this->db->where('user_id', $id);
		}

		$this->db->delete('campaign_users');
	}

	public function addCampaignUser($userData = array()){

		if(!empty($userData)){
			foreach ($userData as $key => $value) {
				$value = (object) $value;

				$data = array(
					'first_name' => isset($value->first_name) ? $value->first_name : '',
					'last_name'  => isset($value->last_name) ? $value->last_name : '',
					'mobile'	 => isset($value->mobile) ? $value->mobile : '',
					'email' 	 => isset($value->email) ? $value->email : '',
					'address' 	 => isset($value->address) ? $value->address : '',
					'city' 		 => isset($value->city) ? $value->city : '',
					'country' 	 => isset($value->country) ? $value->country : '',
					'dob' 		 => isset($value->dob) ? $value->dob : '',
					'campaign_type' => $this->input->post('campaign_name'),
					'created_at' => date('Y-m-d H:i:s')
				);

				$this->db->insert('campaign_users',$data);
			}
		}

		return $insert_id = $this->db->insert_id();
	}

	public function updateCampaignUser($data, $userID){
		$this->db->where('user_id', $userID);
		$this->db->update('campaign_users', $data);
	}


	// Datatable
	private function _get_datatables_query()
    {
        $this->db->from($this->table);
 
        $i = 0;
     
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {                 
                if($i===0) // first loop
                {
                   // $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                //if(count($this->column_search) - 1 == $i) //last loop
                   // $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($this->input->post('length'), $this->input->post('start'));
        $query = $this->db->get();
        return $query->result();
    }
 
    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

	public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
	
}