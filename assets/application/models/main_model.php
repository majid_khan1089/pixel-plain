<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Main_model extends CI_Model {

  function __construct() {
    parent::__construct();
  }

  public function insertData($postData, $type = '', $campaignID){
    $data = array(
          'f_name' => isset($postData['f_name']) ? $postData['f_name'] : '',
          'l_name' => isset($postData['l_name']) ? $postData['l_name'] : '',
          'email' => isset($postData['email'])  ? $postData['email'] : '',
          'company' => isset($postData['company']) ? $postData['company'] : '',
          'job_title' => isset($postData['job_title']) ? $postData['job_title'] : '',
          'job_title_level' =>isset($postData['job_title_level']) ? $postData['job_title_level'] : '',
          'phone' =>isset($postData['phone']) ? $postData['phone'] : '',
          'address' =>isset($postData['address']) ? $postData['address'] : '',
          'city' => isset($postData['city']) ? $postData['city'] : '',
          'state' => isset($postData['state']) ? $postData['state'] : '',
          'zip' => isset($postData['zip']) ? $postData['zip'] : '',
          'country' => isset($postData['country']) ? $postData['country'] : '',
          'industry' => isset($postData['industry']) ? $postData['industry'] : '',
          'department' => isset($postData['department']) ? $postData['department'] : '',
          'company_size' => isset($postData['company_size']) ? $postData['company_size'] : '',
          'subscribe_offer' => isset($postData['subscribe_offer']) ? $postData['subscribe_offer'] : '',
          'campaign_type' => $type,
          'campaign_id' => $campaignID,
          'dump_data' => serialize($postData)
      );

      if($this->db->insert('clients', $data)){
          return true;
      }
      else{
          return false;
      }

  }

  public function validate($data){
    $query = $this->db->get_where('clients',array('email' =>$data['email']));
    $qdata = $query->num_rows();

    if($qdata == NULL || $qdata == 0){
      return false;
    }
    else{
      
      $updateData = array(
               'isDownload' => 1
            );

        $this->db->where('email', $data['email']);
        $this->db->update('user', $updateData); 
      return true;
    }
  }

  public function getReports(){
    $query = $this->db->get_where('user',array('isDownload' =>1));
    return $query->result();
  }

}