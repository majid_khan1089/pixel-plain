<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model("main_model");
        $this->load->helper('download');
    }


    public function index() {
    	//1861/4072/TMC001/farman4332@gmail.com
    	//echo base64_decode("1998/4286/TMC001/farman4332@gmail.com");die;
        //echo base64_decode($this->input->get('id'));die;
        if($this->input->get('id')){
            $data = base64_decode($this->input->get('id'));
            $results = explode('/', $data);
            if(sizeof($results) == 4){
                $dataVar = array(
                    'campId' => $results[0],
                    'contentId' => $results[1],
                    'source' => $results[2],
                    'email' => $results[3]
                    );
                $this->load->view('vwHeader');
                $this->load->view('vwHome', $dataVar);
                $this->load->view('vwFooter');
            }
            else{
                show_404();
            }
        }
        else{
            $this->load->view('main_home');
        }
    }



    public function register() {

        $dataVar = array(
            'campId' => $this->input->post('campId'),
            'contentId' => $this->input->post('contentId'),
            'source' => $this->input->post('source'),
            'email' => $this->input->post('email')
            );
        $this->load->view('vwHeader');
        $this->load->view('register', $dataVar);
        $this->load->view('vwFooter');
    }




    public function submitData() {
        $data = array();

        if($this->input->post()) {
            $message = "Send data";
            if($this->main_model->insertData($_POST)){
            $this->load->library('email');
            $this->email->from($this->input->post('email'), $message);
            $this->email->to('harbourspace@gmail.com');
            $this->email->subject($this->input->post('subject'));
            $this->email->message($message);

            if ($this->email->send()) {
                $data = array('error' => false, 'success' => true, 'message' => "Thanks For your Intrest We Will Get Back To You Soon.");
            } else {
                $data = array('error' => true,'success' => false, 'message' => "Sorry For Inconvinience Please Fill All The Fields Or Try After Some Time");
            }
        }
        else{

            echo "Error sending data to server!!!!";
        }

            /*$this->load->view('vwHeader');
            $this->load->view('vwHome');
            $this->load->view('vwFooter');*/
            redirect("http://cloudpapers.net/cp-player/".$this->input->post('campId')."?contentId=".$this->input->post('contentId')."&source=".$this->input->post('source')."&email=".$this->input->post('email'));
        } 
        
         
    }

    public function validate(){
         if($this->main_model->validate($_POST)){
                echo "1";
         }
         else{
                echo "0"
                ;
         }

    }

    public function reports(){

        $data['dat'] = $this->main_model->getReports();
        $this->load->view('repports', $data);

    }

    public function downloadPDF(){
        $this->load->helper('download');
        $pdf_path = "http://bizmartech.com/assets/pdf/10-Mistakes-to-Avoid-When-Buying-a-Phone-System.pdf";
        $pdf_name = "10-Mistakes-to-Avoid-When-Buying-a-Phone-System.pdf";
        $data = file_get_contents($pdf_path); // Read the file's contents

        force_download($pdf_name, $data);
    }
}

//http://cloudpapers.net/cp-player/1861?contentId=4072&source=TMC001&email=farman4332@gmail.com
/* End of file main.php */
/* Location: ./application/controllers/main.php */