<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Content extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->library('ion_auth');

		if (!$this->ion_auth->logged_in())
		{
			redirect('auth', 'refresh');
		}
		
		if($this->session->userdata('lang')== 'ar'){
			$langFile = 'custom'; $folderName = 'arabic';
		}else{
			$langFile = 'custom'; $folderName = 'english';
		}

		$this->lang->load($langFile, $folderName);
	}
	
	public function index(){

		$data['listings'] = $this->admin_model->get_content(); 
		$this->load->view('admin/content_listing', $data);
	}

	public function add(){

		$data = array();
		$data['listings'] = $this->admin_model->get_campaign();
		if($this->input->post()){
			$this->admin_model->add_content();
			$this->session->set_flashdata('success','Data Added Successfully');

			redirect('content');
		}
		$this->load->view('admin/content',$data);
		

	}

	public function edit($content_id = ''){
		if($this->input->post()){
			
			$this->admin_model->updatet_content();
			
			$this->session->set_flashdata('success','Data Updated Successfully');

			redirect(base_url('content'));
		}

		if($content_id){
			$data['content'] = $this->admin_model->getContentByID($content_id);
			$data['listings'] = $this->admin_model->get_campaign();
		}

		$this->load->view('admin/content',$data);
	}

	public function delete($content_id = ''){

		$this->admin_model->delete_content($content_id);
		$this->session->set_flashdata('success','Record Deleted Successfully');
		redirect(base_url('content'));
	}

}