<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Campaign extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->library('ion_auth');

		/*if (!$this->ion_auth->logged_in())
		{
			redirect('auth', 'refresh');
		}*/
		
		if($this->session->userdata('lang')== 'ar'){
			$langFile = 'custom'; $folderName = 'arabic';
		}else{
			$langFile = 'custom'; $folderName = 'english';
		}

		$config = array(
    		'table' => "campaign",
    		"id" => 'campaign_id',
    		'field' => 'campaign_slug',
    		'title' => 'campaign_name',
    		'replacement' => 'underscore' // Either dash or underscore
		);
		$this->load->library('slug', $config);

		$this->lang->load($langFile, $folderName);	
	}
	
	public function index(){
		$data['listings'] = $this->admin_model->get_campaign();
		$this->load->view('admin/campaign_listing', $data);
	}

	public function add(){
		$data = array();
		$data['isNew'] = true;
		if($this->input->post()){

			$data = array(
			    'campaign_name' => $this->input->post('camp_name')
			);
			$data['slug'] = $this->slug->create_uri($data);

			$this->admin_model->add_campaign($data);
			$this->session->set_flashdata('success','Data Added Successfully');

			redirect('campaign');
		}

		$this->load->view('admin/campaign',$data);
	}

	public function edit($campaign_id = ''){
		if($this->input->post()){
			
			$this->admin_model->updatet_campaign();
			
			$this->session->set_flashdata('success','Data Updated Successfully');

			redirect(base_url('campaign'));
		}

		if($campaign_id){
			$data['isNew'] = false;
			$data['campaign'] = $this->admin_model->getCampaignByID($campaign_id);
		}

		$this->load->view('admin/campaign',$data);
	}

	public function delete($campaign_id = ''){
		$this->admin_model->delete_campaign($campaign_id);
		$this->session->set_flashdata('success','Record Deleted Successfully');
		redirect(base_url('campaign'));
	}

	public function sendEmail(){
		$data['listings'] = $this->admin_model->get_campaign();
		$data['c_listings'] = $this->admin_model->get_content();
		$this->load->view('admin/sendEmail', $data);
	}

	public function sendCampaignMail(){
		$status = false;
		$data = array();
		$message = array();

	  	$emailType = $this->input->post("email_type");
		$emailID = base64_encode(trim($this->input->post("p_name")));
		$config = $this->getMailConfig();

		$this->email->initialize($config);
		$this->email->from(EMAIL_FROM, EMAIL_FROM_NAME);
		$this->email->to($this->input->post("p_name"));
		$this->email->reply_to(EMAIL_FROM, EMAIL_FROM_NAME);
		$this->email->bcc(array('majid.khan1089@gmail.com'));
		
		if($emailType){
			$campaignData = $this->admin_model->getCampaignData($emailType);

			$to = $this->input->post("p_name");
			$from = EMAIL_FROM;
			$frmName = EMAIL_FROM_NAME;
			$subject = $campaignData->mail_subject;

			if($campaignData){
				$this->email->subject($campaignData->mail_subject);
  				$data['link'] = base_url($campaignData->url . $emailID .'/'. base64_encode($campaignData->campaign_id));
			}
		}

		$message = $this->load->view('mailtemplate/'. $emailType ,$data,true);

		$this->email->message($message);

		// Add Mail Template To Log Table
        $this->admin_model->addEmailTemplate($to, $from, $frmName, $subject, 'customer', $message, $data['link']);

		if($this->email->send()){
			$status =  true;
			$this->session->set_flashdata('success','Email Sent Successfully');
		}

		redirect(base_url('campaign/sendEmail'));
	}

	private function getMailConfig(){
		$config = Array(
			// 'mailpath' => SMTP_MAILPATH,
			'protocol' 		=> 'smtp',
			'smtp_host' 	=> SMTP_HOST,
			'smtp_port' 	=> SMTP_PORT,
			'smtp_user' 	=> SMTP_USERNAME,
			'smtp_pass' 	=> SMTP_PASSWORD,
			'smtp_timeout'	=>	20,
			'mailtype' 		=> 'html',
			'charset' 		=> 'iso-8859-1',
			'wordwrap' 		=> TRUE,
			'crlf' 			=> "\r\n",
			'newline' 		=> "\r\n"
		);

		return $config;
	}

	public function landingPageRegistration(){
		$data =array();
		$campaignID =  base64_decode($this->uri->segment(3));

		if($campaignID){
			$campaignData  = $this->admin_model->getCampaignByID($campaignID);	

			if($this->input->post()){
				if($this->main_model->insertData($_POST, $campaignData->campaign_slug, $campaignID)){

					// If selected is Video
					if($campaignData->campaign_slug == 'symantech' || $campaignData->url_type == "video"){
						redirect($campaignData->pdf_url);
					}
					
					if($this->input->post('form-type')){
						redirect('download-content?type='. $this->input->post('form-type'));
					}else{
						$pdf_path = $campaignData->pdf_url;
			    		$pdf_name = str_replace(' ', '-', $campaignData->campaign_name). '.pdf';
				    	$data = file_get_contents($pdf_path); // Read the file's contents
		        		force_download($pdf_name, $data);
					}
				}

				$this->session->set_flashdata('success','Data Added Successfully');
			}
		}

		$this->load->view('admin/'. $campaignData->landing_page_slug ,$data);
	}

	public function downloadContent(){
		$downloadType = $_GET['type'];
		
		if($downloadType =='epicorform'){
			$this->load->view('pdf-download-test');
		}else{
			$this->load->view('epicor_1_form_download');
		}		
	}

	public function emailDownload(){
		$data =array();
		$emailID =  base64_decode($this->uri->segment(2)); 
	  	$campaignID =  base64_decode($this->uri->segment(3));

		if($emailID && $campaignID){
			$campaignData  = $this->admin_model->getCampaignByID($campaignID);

			// Redirect if Video is found
			if($campaignData->url_type == "video" && isset($campaignData->url_type)){
				redirect($campaignData->pdf_url);
			}

			if($this->admin_model->add_pdf_campaign($emailID) && $campaignID){
				$pdf_path = $campaignData->pdf_url;
	    		$pdf_name = str_replace(' ', '-', $campaignData->campaign_name). '.pdf';
		    	$data = file_get_contents($pdf_path); // Read the file's contents
        		force_download($pdf_name, $data);
			}
		}

		return true;
	}

	// Rest all for the demo purpose
	public function registerEsgCustomer(){
		$data =array();
		$campaignID =  base64_decode($this->uri->segment(2));

		if($campaignID){
			$campaignData  = $this->admin_model->getCampaignByID($campaignID);

			if($this->input->post()){
				if($this->main_model->insertData($_POST, 'ESG')){
					$pdf_path = $campaignData->pdf_url;
		    		$pdf_name = str_replace(' ', '-', $campaignData->campaign_name). '.pdf';
			    	$data = file_get_contents($pdf_path); // Read the file's contents
	        		force_download($pdf_name, $data);
				}

				$this->session->set_flashdata('success','Data Added Successfully');
			}
		}

		$this->load->view('admin/esg_landing_page',$data);
	}

	public function pdfDownloadRedirect(){

		$emailID = urldecode(base64_decode($this->uri->segment(2)));
          
		if($emailID){
			$this->admin_model->add_pdf_campaign();
		}
		/*
			$this->load->helper('download');
			$pdf_path = "http://bizmartech.com/assets/pdf/10-Mistakes-to-Avoid-When-Buying-a-Phone-System.pdf";
	    	$pdf_name = "10-Mistakes-to-Avoid-When-Buying-a-Phone-System.pdf";
    		$data = file_get_contents($pdf_path); // Read the file's contents
    		force_download($pdf_name, $data);
   		*/

        $this->downloadPDF();
	}

	public function registerCustomer(){
		$this->load->model("main_model");
		$this->load->helper('download');

		$data =array();

		if($this->input->post()){
			$this->main_model->insertData($_POST);
			$this->session->set_flashdata('success','Data Added Successfully');
			redirect('download-pdf');
		}
		
		$this->load->view('admin/campaign_landing_page',$data);
	}

	public function downloadPDF(){
		$this->load->helper('download');
		$pdf_path = "http://bizmartech.com/assets/pdf/10-Mistakes-to-Avoid-When-Buying-a-Phone-System.pdf";
    	$pdf_name = "10-Mistakes-to-Avoid-When-Buying-a-Phone-System.pdf";
    	$data = file_get_contents($pdf_path); // Read the file's contents

    	force_download($pdf_name, $data);
	}

	public function pdfLog(){
		$data = array();
		$data['listings'] = $this->admin_model->get_pdf_log();
		$this->load->view('admin/pdf_log',$data);
	}

	public function registerUser(){
		$data = array();
		$data['listings'] = $this->admin_model->get_registeruser_log();
		$this->load->view('admin/register_user',$data);
	}

	public function getClientData(){
		$data = array();
		$userID = $this->input->post('userID');

		$data = array(
	        		'text' => '',
    	    		'content' => '',
    			);

		if($userID){
			// Get User Data
			$data['userData'] = $this->admin_model->get_registeruser_log($userID);
			$clientView = $this->load->view('admin/client_details',$data, true);
			$data = array(
    	        		'text' => 'Good',
                		'content' => $clientView,
            		);
		}

		return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($data));
	}

	public function checkCampaignName(){
		$status = true;
		$campaignName = $this->input->post('camp_name');

		if($campaignName && $this->admin_model->campaignCheck($campaignName)){
			$status = false;
		}

		if($this->uri->segment(2)){
			$status = true;
		}

		echo  json_encode($status);
	}

	public function updateCampaignStatus(){
		$campaignID = $this->input->post('campaignID');
		$status = ($this->input->post('status')) ? 0 : 1;
	
		if($campaignID){
			$update_result = $this->admin_model->updateCampaignStatus($campaignID, $status);
		}

  		if($update_result){
  			$this->session->set_flashdata('success','User status updated successfully');
  		}else{
  			$this->session->set_flashdata('danger','Error in updating data, please try again later');
  		}
  		
  		echo json_encode($update_result);
  	}

  	public function showCampaign(){
  		$data = array();
		$campaignID = $this->input->post('campaignID');

		$data = array(
	        		'text' => '',
    	    		'content' => '',
    			);

		if($campaignID){
			// Get User Data
			$data['campaign'] = $this->admin_model->getCampaignByID($campaignID);
			$campaignView = $this->load->view('admin/campaign_view',$data, true);
			$data = array(
                		'content' => $campaignView,
            		);
		}

		return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($data));
  	}

	public function sendBulkCampaignMail(){
		$status = false;
		$data = array();
		$message = array();
		$data['users'] = array();

		$this->load->model('user_model');

	  	$campaignType = $this->input->post("campaign_type");
		$allSelectedUsers = $this->input->post("selected_users");
		
		if($allSelectedUsers){
			$allSelectedUsers = explode(',', $allSelectedUsers);
			
			// Get Multiple User data by Ids
			$data['users'] = $this->user_model->getCampaignUserByIDArray($allSelectedUsers);
		}


		if(!empty($data['users']) &&  $campaignType){			
			foreach ($data['users'] as $userKey => $userValue) {
				$data['first_name'] = ($userValue->first_name) ? ucfirst($userValue->first_name) : 'Subscriber';
				
				$emailID = base64_encode(trim($userValue->email));
				
				$config = $this->getMailConfig();

				$this->email->initialize($config); // add this line

				$this->email->from(EMAIL_FROM, EMAIL_FROM_NAME);
				$this->email->to($this->input->post($userValue->email));
				// $this->email->reply_to(EMAIL_FROM, EMAIL_FROM_NAME);
				if($campaignType){
					$campaignData = $this->admin_model->getCampaignData($campaignType);

					$to = $userValue->email;
					$from = EMAIL_FROM;
					$frmName = EMAIL_FROM_NAME;
					$subject = $campaignData->mail_subject;

					if($campaignData){
						$this->email->subject($campaignData->mail_subject);
		  				$data['link'] = base_url($campaignData->url . $emailID .'/'. base64_encode($campaignData->campaign_id));
					}
				}

				$message = $this->load->view('mailtemplate/'. $campaignType ,$data,true);
				$this->email->message($message);
				
				// Add Mail Template To Log Table
		        $this->admin_model->addEmailTemplate($to, $from, $frmName, $subject, 'customer', $message, $data['link']);
				$status =  true;
				$this->session->set_flashdata('success','Email Sent Successfully');

				$status = true;
			}
		}

		 return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($status));
	}

	public function getCampaignBySlug(){
		$data = array();
		$campaignSlug = $this->input->post('campaign_slug');
		
		if($campaignSlug){
			$data = $this->admin_model->getCampaignBySlug($campaignSlug);
		}

		return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($data));
	}

	public function mailLogCron(){
        $mailLogs = $this->admin_model->getEmailLogs();

        if(!empty($mailLogs)){
            foreach ($mailLogs as $logKey => $logValue) {
                $to = $logValue->customer_email;
                $from = $logValue->mail_from;
                $fromName = $logValue->mail_from_name;
                $subject = $logValue->mail_subject;
                $emailMessage = $logValue->email_template;
            
                $mailResponse = $this->sendLogEmails($to, $from, $fromName, $subject, $emailMessage);
                
                if($mailResponse){
                    // Update Log for mail response
                    $this->admin_model->updateEmailLog($logValue->id);
                }
            }
        }
    }

    // Genric Send Email Function
    private function sendLogEmails($to, $from, $frmName, $subject, $message){
        $responseString = array();
        $status =  false;
        
        // Send Mail To User
        $config = $this->getMailConfig();
        $this->email->initialize($config);
        $this->email->from($from, $frmName);
        $this->email->to($to);
        $this->email->reply_to(EMAIL_FROM, EMAIL_FROM_NAME);

        $this->email->subject($subject);
        $this->email->message($message);

        if($this->email->send()){
            $status =  true;
        }

        return $status;
    }
}