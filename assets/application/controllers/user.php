<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

    public function __construct() {
        parent::__construct();
        
        if (!$this->ion_auth->logged_in())
        {
            redirect('auth', 'refresh');
        }
        if($this->session->userdata('lang')== 'ar'){
            $langFile = 'custom'; $folderName = 'arabic';
        }else{
            $langFile = 'custom'; $folderName = 'english';
        }
        $this->lang->load($langFile, $folderName);  

        $this->load->model("user_model");
        $this->load->library('csvreader');
    }

    public function listing(){
        $data = array();

       // $data['listings'] = $this->user_model->getCampaignUsers();
        $data['listings'] = $this->admin_model->get_campaign();
        $this->load->view('admin/users/user_listing', $data);
    }

    public function getUserDatatable(){
        $data = array();
        $responseResult = $this->user_model->get_datatables();
        $no = $this->input->post('start');
        foreach ($responseResult as $response) {
            $no++;
            $row = array();
            $row[] = '<input type="checkbox" class="checkbox" name="user_id_'. $response->user_id.'" value="'. $response->user_id.'">';
            $row[] = ($response->campaign_type) ? strtoupper($response->campaign_type) : '-';
            //$clientName = ($response->first_name || $response->last_name) ? ucfirst($response->first_name) . ' ' . $response->last_name :  '-' ;
            $clientName = ($response->first_name) ? ucfirst($response->first_name) :  '-' ;
           // $row[] = '<a href="javascript:void(0);" data-id="'. $response->user_id .'" class="showUserDetails"><strong>'. $clientName .'</strong></a>';
            $row[] = ($response->first_name) ? strtoupper($response->first_name) : 'N.A';
            $row[] = ($response->email) ? ($response->email) : 'N.A';
            //  $row[] = $response->mobile;
            //  $row[] = ($response->status) ? '<span class="label label-success">Active</span>' : '<span class="label label-danger">Inactive</span>';
            
            /*$row[] = '<a href="'. base_url('/edit/'.$response->user_id. '/user') .'" class="btn btn-primary btn-xs">
                                                <i class="fa fa-pencil"></i>
                                            </a>
                                            <a onclick="confirm(\'Are you sure want to delete this users?\');" href="'. base_url('/delete/'. $response->user_id . '/user') .'" class="btn btn-danger btn-xs">
                                                <i class="fa fa-remove"></i>
                                            </a>';
 */
            $row[] = '<a onclick="confirm(\'Are you sure want to delete this users?\');" href="'. base_url('/delete/'. $response->user_id . '/user') .'" class="btn btn-danger btn-xs">
                        <i class="fa fa-remove"></i>
                    </a>';
            $data[] = $row;
        }

        $output = array(
                        "draw" => $this->input->post('draw'),
                        "recordsTotal" => $this->user_model->count_all(),
                        "recordsFiltered" => $this->user_model->count_filtered(),
                        "data" => $data,
                );

        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($output));
    }

    public function viewDetails(){
        $data = array();
        $userID = $this->input->post('user_id');

        $data = array(
                    'status' => false,
                    'content' => '',
                );

        if($userID){
            // Get User Data
            $data['userDetails'] = $this->user_model->getCampaignUserByID($userID);
            $userView = $this->load->view('admin/users/user_view',$data, true);
            $data = array(
                        'status' => true,
                        'content' => $userView,
                    );
        }

        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($data));
    }

    public function listCampaign(){
        $data = array();
        $userIds = $this->input->post('user_ids');

        $data = array(
                    'status' => false,
                    'content' => '',
                );

        if($userIds){
            // Get User Data
            $data['listings'] = $this->admin_model->get_campaign();
            $data['user_ids'] = $this->input->post('user_ids');

            // /$data['userDetails'] = $this->user_model->getCampaignUserByID($userIds);
            $userView = $this->load->view('admin/users/campaign_view',$data, true);
            
            $data = array(
                        'status' => true,
                        'content' => $userView,
                    );
        }

        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($data));
    }

    public function actionUser($userID = ''){
        $data =array();
        $data['isNew'] = true;
        
        if($userID){
            $data['isNew'] = false;
            $data['userDetails'] = $this->user_model->getCampaignUserByID($userID);
        }

        if($this->input->post()){
            $postData = $this->input->post();

            $this->user_model->updateCampaignUser($postData, $userID);
            $this->session->set_flashdata('success','Data Updates Successfully');
            redirect('user/listing');
        }

        $this->load->view('admin/users/action_user',$data);
    }

    public function bulkdeleteUser(){

        $userIds = $this->input->post('user_ids');
        
        $data = array(
                    'status' => false,
                );

        if(!empty($userIds)){
            $userIds = explode(',', $userIds);

            $this->user_model->deleteCampaignUser($userIds);
            $this->session->set_flashdata('success','Records Deleted Successfully');
            
            $data = array(
                        'status' => true
                    );
        }

         return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($data));
    }

    public function deleteUser($userID){
        if($userID){
            $this->user_model->deleteCampaignUser($userID);
            $this->session->set_flashdata('success','Record Deleted Successfully');
        }else{
            $this->session->set_flashdata('danger','Error, Please try again after sometime.');
        }

        redirect(base_url('user/listing'));
    }

    public function uploadUser(){
        $data = array();

        $data['campaigns'] = $this->admin_model->get_campaign();
        
        if($this->input->post()){
            
            if(!empty($_FILES)){
                $results =   $this->csvreader->parse_file($_FILES['user_records']['tmp_name']);//path to csv file
                //$data['csvData'] =  $result;
                ini_set( 'max_execution_time', 0 );
                $resposeID = $this->user_model->addCampaignUser($results);

                if($resposeID){
                    $this->session->set_flashdata('success','User Uploaded Successfully');
                }
            }

            redirect('user/listing');
        }

        $this->load->view('admin/users/upload_user',$data);
    }
}