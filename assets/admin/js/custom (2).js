$(function(){

	// Custom Changes for lang
	$(document).on('click','#changelang', function(){
	  $.ajax({
	    type: "post",
	    url: baseUrl +"changeLang",
	    data: {lang: $(this).data('lang')},
	    success: function(result)
	    {
	      location.reload();
	    }
	   });
	});

	$('#customTable').DataTable();
});