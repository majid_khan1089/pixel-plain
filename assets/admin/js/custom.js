$(function() {
//    console.log("In Ready");

    $('#customTable').DataTable();
    $('#myTable').DataTable();

    setTimeout(function() {
        $('.alert').hide();
    }, 3000);

    $(document).on('click', '.viewClientDetails', function() {
        $.ajax({
            url: baseUrl + 'client-details',
            type: "post",
            dataType: 'json',
            data: {
                'userID': $(this).data('id')
            },
            success: function(data) {
                $(".modal-body").html(data.content);
                $('#clientModal').modal('show');
            }
        });
    });

    $(".viewcontent").on("click", function(e) {
        var contentid = $(this).data('id');
        $.ajax({
            url: baseUrl + '/cms/' + contentid + '/modal',
            type: "post",
            dataType: 'json',
            success: function(data) {
                $("#showid").html(data.content);
                $("#titleid").text(data.title);
            }
        });
        e.preventDefault();
    });

    $("#campaign_add").validate({
        rules: {
            camp_name: {
                required: true,
                minlength: 4,
                remote: {
                    url: baseUrl +"check-campaign-name" + editSLug,
                    type: "post",
                    data: {
                      username: function() {
                        return $( "#camp_name" ).val();
                      }
                    }
                }
            },
            pdf_url: "required",
            mail_subject: "required",
            /*username: {
                required: true,
                minlength: 4,
               remote: {
                    url: baseUrl +"check-username",
                    type: "post",
                    data: {
                      username: function() {
                        return $( "#username" ).val();
                      }
                    }
                }
            },*/
            /*email: {
                required: true,
                email: true
            },*/
        },
        messages: {
            camp_name: {
                remote: "Campaign name already exists"
            },
            mobile: {
                number: "Please enter a valid mobile number"
            },
        },
    });

    $("#send_campaign").validate({
        rules: {
            p_name: {
                required: true,
                email: true
            }
        },
        messages: {
            p_name: {
                required: "Please insert email address",
                email: "Please enter a valid email address"
            }
        }
    });

    // Campaign Enable 
    $(document).on('click','.campaign-status', function(){
        $.ajax({
            url: baseUrl + 'campaign-status',
            type: "post",
            data: {'campaignID': $(this).data('id'), 'status' : $(this).data('status')},
            success: function (result)
            {
                location.reload();
            }
        });
    });

    $(document).on('click','.showCampaign', function(){
        $.ajax({
            url: baseUrl + 'show-campaign',
            type: "post",
            dataType: 'json',
            data: {
                'campaignID': $(this).data('id')
            },
            success: function(data) {
                $(".modal-body").html(data.content);
                $('#campaignModal').modal('show');
            }
        });
    });

    $(document).on('click','.showUserDetails', function(){
        $.ajax({
            url: baseUrl + 'user/viewDetails',
            type: "post",
            dataType: 'json',
            data: {
                'user_id': $(this).data('id')
            },
            success: function(data) {
                $(".modal-body").html(data.content);
                $('#userModal').modal('show');
            }
        });
    });

    //select all checkboxes
    $("#select_all").prop('checked', false); // default false
    $(".checkbox").prop('checked', false); // default false

    $("#select_all").change(function(){  //"select all" change
        $(".checkbox").prop('checked', $(this).prop("checked")); //change all ".checkbox" checked status
    });

    //".checkbox" change
    $('.checkbox').change(function(){
        //uncheck "select all", if one of the listed checkbox item is unchecked
        if(false == $(this).prop("checked")){ //if this item is unchecked
            $("#select_all").prop('checked', false); //change "select all" checked status to false
        }
        //check "select all" if all checkbox items are checked
        if ($('.checkbox:checked').length == $('.checkbox').length ){
            $("#select_all").prop('checked', true);
        }
    });

    // Bulk User Delete
    $(document).on('click','.bulkDelete',function(){
        if(!$('.checkbox').prop("checked")){
            alert("Please selecte the user");
        }else{
            var checkedVals = $('.checkbox:checkbox:checked').map(function() {
                return this.value;
            }).get();
            checkedVals = checkedVals.join(",");

            var result = confirm("Are you sure want to delete the selected users?")
            if(result){
                $.ajax({
                    url: baseUrl + 'user-bulk-delete',
                    type: "post",
                    dataType: 'json',
                    data: {
                          'user_ids': checkedVals
                    },
                    success: function(data) {
                        location.reload();
                    }
                });    
            }            
        }        
    });

    // Mass Mail
    $(document).on('click','.bulkMailToUser',function(){
        if(!$('.checkbox').prop("checked")){
            alert("Please selecte the user");
        }else{
            var checkedVals = $('.checkbox:checkbox:checked').map(function() {
                return this.value;
            }).get();
            checkedVals = checkedVals.join(",");
            $('.selected_user_id').attr("data-id", checkedVals);
            $('#sendMassMail').modal('show');
            /*$.ajax({
                url: baseUrl + 'user/listCampaign',
                type: "post",
                dataType: 'json',
                data: {
                    'user_ids': checkedVals
                },
                success: function(data) {
                    $(".modal-body").html(data.content);
                    $('#sendMassMail').modal('show');
                }
            });*/
        }
    });

    $(document).on('change','#selected_campaign', function(){
        var campaignSlug  = $(this).val();
        $('#selectedCampaingId').val(campaignSlug);

        if(campaignSlug){
            $.ajax({
                url: baseUrl + 'campaign/getCampaignBySlug',
                type: "post",
                dataType: 'json',
                data: {'campaign_slug' : campaignSlug},
                success: function(data) {
                    // console.log(data);
                    if(data){
                        $('p').show();
                        $('#campaignType').text(data.mail_subject.toUpperCase());
                        $('#emailSubject').text(data.campaign_type.toUpperCase());
                    }
                }
            });
        }
    });

    $(document).on('click','.sendMailToUser', function(){
        var selectedCampaignValue = $('#selected_campaign').val();
        var checkedVals = $('.checkbox:checkbox:checked').map(function() {
                return this.value;
            }).get();
        checkedVals = checkedVals.join(",");

        if(selectedCampaignValue){
            $.ajax({
                url: baseUrl + 'send-bulk-email',
                type: "post",
                dataType: 'json',
                data: {'campaign_type' : selectedCampaignValue, 'selected_users' : checkedVals},
                success: function(data) {
                    if(data){
                        location.reload();
                    }
                }
            });
        }else{
            alert("Please select the campaign for sending email template");
        }
    });

    $( ".sendMailToUser" ).on( "submit", function( event ) {
        event.preventDefault();
        console.log($(this).serialize());
    });
    
    $(document).on('click','.resetCheckbox', function(){
        $("#select_all").trigger('click');
    });    
});