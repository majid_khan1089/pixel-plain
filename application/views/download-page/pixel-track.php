<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Pixel Tracking Test</title>
        <!-- Styles -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
        <link href="http://www.mqltosql.com/email-template/HTML/28079-Thousand-Eyes/css/styles.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <!-- Fixed navbar -->
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/">
                    <img src="http://placehold.it/254x40" alt="" />
                    </a>
                </div>
                <!-- the actual nav items -->
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav"></ul>
                </div>
                <!--/.nav-collapse -->
            </div>
        </nav>
        <!-- main content -->
        <div class="container">
            <br />
            <div class="row">
                <!-- main column -->
                <div class="col-sm-12" style="min-height:400px">
                
                    <img class="pull-left" style="padding-right:20px;" src="http://placehold.it/300x300" width="180" height="232">
                    <h1>Thank You... </h1>
                    <p>For downloading </p>
                    <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed malesuada ullamcorper placerat. Suspendisse eu neque fringilla, fermentum velit facilisis, imperdiet mauris. </p>
                    <p class="filedwnld"><a href="http://www.adobe.com/content/dam/Adobe/en/devnet/acrobat/pdfs/pdf_open_parameters.pdf">Click here to download your document</a></p>
                </div>
                <img src="http://lead.truepixl.com/app/pixel/?c=bt6ZMeLp48g&p=ZMJK88q3vjM&e=<?php echo $_GET['email']; ?>">
                <!-- right column -->
            </div>
        </div>
        <footer class="footer">
            <div class="container">
                <p class="text-muted">© Copyright 2017, All rights reserved.</p>
            </div>
        </footer>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    </body>
</html>