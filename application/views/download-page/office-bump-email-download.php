<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"><head>
    
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>Bump your email up to business class</title>
    
    <!-- Styles -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />
<link href="http://martechb2b.com/email-template/HTML/office-bump-your-e-mail/css/styles.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<link href="https://fonts.googleapis.com/css?family=Gudea" rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
 
 <style>
 p {
    font-size: 18px;
    line-height: 27px;
    margin-bottom: 19px;
}
 .navbar-default {
    /*background-color: #f8f8f8;*/
    border-color:#D83B01;
}

.navbar {
    border-radius: 0px;
}
 </style>
  

</head>
<body>

    
<!-- Fixed navbar -->
<nav class="navbar navbar-default">
	<div class="container">
		<div class="navbar-header">
			
			<a class="navbar-brand" >
				<img src="http://martechb2b.com/email-template/HTML/office-bump-your-e-mail/images/Logo_Office_White_76x24.png" alt="" />
			</a>
		</div>
		<!-- the actual nav items -->
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav"></ul>
		</div>
		<!--/.nav-collapse -->
	</div>
</nav>
<!-- main content -->
<section>
<div style=" background-image:url(http://martechb2b.com/email-template/HTML/office-bump-your-e-mail/images/Original.png); background-position:top; background-repeat:no-repeat;">
<div class="container" style=" padding-top:20px;">
  <br />
	<div class="row">
		<!-- main column -->
		<div class="col-sm-12">
                <div class="form-page-content" >
                  <h1 style="color:#FFF;">Thank you for your interest in the "Bump your Email Up" eBook</h1>
                  
                               
             <div style=" padding-top:40px;"> <a href="http://martechb2b.com/uploads/Bump-Your-E-mail-to-Business-Class.pdf"><img src="http://martechb2b.com/email-template/HTML/office-bump-your-e-mail/images/get_free_ebook-download.png"></a> </div>
             <br/>
             <br/>
               <div style="height:150px;"> &nbsp; </div>
          </div>
            </div>

				<!-- right column -->
				
				</div>
			</div>
            </div>
            
            </section>
			<footer class="footer">
				<div class="container">
                <div class="row">
                <div class="col-md-6">
					<p class="text-muted" style="color:#FFF; font-size:14px;"><a href="http://go.microsoft.com/fwlink/p/?linkid=222682&amp;clcid=0x409">Trademarks</a> &nbsp;&nbsp;&nbsp;&nbsp; <a href="http://go.microsoft.com/fwlink/?LinkId=248681">Privacy &amp; Cookies</a></p>
                    </div>
                    <div class="col-md-6 text-right" >
                    <img src="http://martechb2b.com/email-template/HTML/office-bump-your-e-mail/images/microsoft_footer.png">
                    
                    </div>
                  </div>
				</div>
			</footer>

    <!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

</body>
</html>
