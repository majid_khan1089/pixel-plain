<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>Windows of Opportunity Global Survey Reveals 5 Key Trends for Windows 10 Adoption and Migration</title>
    
    <!-- Styles -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<link href="http://martechb2b.com/email-template/HTML/kitkat-ced78079-vmware-windows/css/styles.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
 
  

</head>
<body>

    
<!-- Fixed navbar -->
<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container">
		<div class="navbar-header">

			<a class="navbar-brand">
				<img src="http://martechb2b.com/email-template/HTML/kitkat-ced78079-vmware-windows/images/VMware-logo-small.png" alt="" />
			</a>
	  </div>
		<!-- the actual nav items -->
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav"></ul>
		</div>
		<!--/.nav-collapse -->
	</div>
</nav>
<!-- main content -->
<div class="container">
<br />
	<div class="row">
		<!-- main column -->
		<div class="col-sm-12" style="min-height:400px">
                <h1>Windows of Opportunity Global Survey Reveals 5 Key Trends for Windows 10 Adoption and Migration</h1>
         
                
          <p>Thank you for your interest in Windows of Opportunity Global Survey Reveals 5 Key Trends for Windows 10 Adoption and Migration. To access your download click the link below.</p>
                <p class="filedwnld"><a href="http://martechb2b.com/uploads/windows-of-opportunity-global-survey-reveals-5-key-trends-for-windows-10-adoption-and-migration.pdf">Click here to download your document</a></p>

      </div>

				<!-- right column -->
				
				</div>
			</div>
			

    <!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

</body>
</html>
