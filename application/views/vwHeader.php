<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>The Marketing Code</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo HTTP_CSS_PATH; ?>/bootstrap.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo HTTP_CSS_PATH; ?>/modern-business.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo base_url(); ?>assets/front-end/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<header class="headbg">
<div class="container">
<div class="row">
<div class="col-md-6">
  <img src="<?php echo HTTP_CAMPAIGN_IMAGES_PATH;?>on24-logo.png">
  <!--<h1>Bizmartech</h1>-->
  </div>
<?php if($this->uri->segment(1) != "register"){?><div class="col-md-6 text-right">
  <a href="#" target="_blank" class="btn-social btn-facebook"><i class="fa fa-facebook"></i></a>
  <a href="#" target="_blank" class="btn-social btn-twitter"><i class="fa fa-twitter"></i></a>
  <a href="#" target="_blank" class="btn-social btn-google-plus"><i class="fa fa-google-plus"></i></a>
  </div>
  <?php }?>  
</div>
</div>
</header>
   <!-- Page Content -->