<?php $this->load->view('admin/common/header'); ?>
<div class="content-wrapper">
  <div class="container">
    <section class="content-header">
          <h1><?php //echo lang('change_password_heading');?></h1><br/>
          <ol class="breadcrumb">
                <li><a href="javascript:void(0);"><i class="fa fa-dashboard"></i> Home</a></li>
                <!-- <li><a href="#">Layout</a></li>
                <li class="active">Top Navigation</li> -->
          </ol>
    </section>

    <section class="content">           
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title"><?php echo lang('create_user_heading');?></h3>
        </div>
                  
        <div class="box-body">
          <?php if(isset($message) && !empty($message)) { ?>
            <div class="alert alert-danger">
              <button data-dismiss="alert" class="close" type="button">×</button>
              <?php echo $message;?>
            </div>
            <?php } ?>

            <div class="row">
              <div class="col-md-10 col-md-offset-1">
                <?php echo form_open("auth/create_user", array('class' => ''));?>
              
               <div class="form-group">
                <label class="control-label">
                  <?php echo lang('create_user_fname_label', 'first_name');?> 
                </label>
                <?php echo form_input($first_name);?>
              </div>

              <div class="form-group">
                <?php echo lang('create_user_lname_label', 'last_name');?> <br />
                <?php echo form_input($last_name);?>
              </div>
                
              <?php
                if($identity_column!=='email') {
                    echo '<p>';
                    echo lang('create_user_identity_label', 'identity');
                    echo '<br />';
                    echo form_error('identity');
                    echo form_input($identity);
                    echo '</p>';
                }
              ?>

              <div class="form-group hide">
                <label class="control-label"><?php echo lang('create_user_company_label', 'company');?> </label>
                <?php echo form_input($company);?>
              </div>

              <div class="form-group">
                  <label class="control-label"><?php echo lang('create_user_email_label', 'email');?> </label>
                  <?php echo form_input($email);?>
              </div>

              <div class="form-group">
                <label class="control-label"><?php echo lang('create_user_phone_label', 'phone');?> </label>
                <?php echo form_input($phone);?>
              </div>

               <div class="form-group">
                <label class="control-label">User Type</label>
                <select name="user_type" id="user_type" class="form-control">
                <?php foreach ($grpData as $grp) { ?>
                  <option value="<?php echo $grp->id ?>"><?php echo ucfirst($grp->name); ?></option>
                <?php } ?>  
                </select>                
              </div>

              <div class="form-group">
                <label class="control-label"><?php echo lang('create_user_password_label', 'password');?> </label>
                <?php echo form_input($password);?>
              </div>

              <div class="form-group">
                <label class="control-label"><?php echo lang('create_user_password_confirm_label', 'password_confirm');?> </label>
                <?php echo form_input($password_confirm);?>
              </div>

              <p><?php echo form_submit('submit', lang('create_user_submit_btn') , array('class' => 'btn btn-primary pull-right'));?></p>
              <?php echo form_close();?>
            </div>
          </div>                              
        </div>
      </div>
    </section>
  </div>
</div>
<?php $this->load->view('admin/common/footer'); ?>
