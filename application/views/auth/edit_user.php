<?php $this->load->view('admin/common/header'); ?>
<div class="content-wrapper">
  <div class="container">
    <section class="content-header">
          <h1><?php //echo lang('change_password_heading');?></h1><br/>
          <ol class="breadcrumb">
                <li><a href="javascript:void(0);"><i class="fa fa-dashboard"></i> Home</a></li>
                <!-- <li><a href="#">Layout</a></li>
                <li class="active">Top Navigation</li> -->
          </ol>
    </section>

    <section class="content">           
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title"><?php echo lang('edit_user_heading');?></h3>
        </div>
                  
        <div class="box-body">
          <?php if(isset($message) && !empty($message)) { ?>
            <div class="alert alert-danger">
              <button data-dismiss="alert" class="close" type="button">×</button>
              <?php echo $message;?>
            </div>
            <?php } ?>

            <div class="row">
              <div class="col-md-10 col-md-offset-1">
                <?php echo form_open(uri_string());?>

                <div class="form-group">
                  <label class="control-label"><?php echo lang('edit_user_fname_label', 'first_name');?> </label>
                  <?php echo form_input($first_name);?>
                </div>

                <div class="form-group">
                  <label class="control-label"><?php echo lang('edit_user_lname_label', 'last_name');?> </label>
                  <?php echo form_input($last_name);?>
                </div>

                <div class="form-group hide">
                    <label class="control-label"><?php echo lang('edit_user_company_label', 'company');?> </label>
                    <?php echo form_input($company);?>
                </div>

                <div class="form-group">
                  <label class="control-label"><?php echo lang('create_user_email_label', 'email');?> </label>
                  <?php echo form_input($email);?>
                </div>

                <div class="form-group">
                  <label class="control-label"><?php echo lang('edit_user_phone_label', 'phone');?> </label>
                  <?php echo form_input($phone);?>
                  <?php echo form_input($phone_display);?>
                </div>

                <?php if ($this->ion_auth->is_admin()): ?>
                <div class="form-group">
                  <label class="control-label">User Type</label>
                  <select name="user_type" id="user_type" class="form-control">
                  <?php foreach ($grpData as $grp) {  $selected = ''; ?>
                    <?php $selected = ($currentGroups->id == $grp->id) ? 'selected' : ''; ?>
                    <option value="<?php echo $grp->id ?>" <?php echo $selected; ?>><?php echo ucfirst($grp->name); ?></option>
                  <?php } ?>  
                  </select>                
                </div>
                <?php endif; ?>

                <div class="form-group">
                  <label class="control-label"><?php echo lang('edit_user_password_label', 'password');?> </label>
                  <?php echo form_input($password);?>
                </div>

                <div class="form-group">
                  <label class="control-label"> <?php echo lang('edit_user_password_confirm_label', 'password_confirm');?></label>
                  <?php echo form_input($password_confirm);?>
                </div>

                 <div class="form-group hide">
                <?php if ($this->ion_auth->is_admin()): ?>

                    <h3><?php echo lang('edit_user_groups_heading');?></h3>
                    <?php
                     foreach ($groups as $group):?>
                        <label class="checkbox control-label">
                        <?php
                            $gID=$group['id'];
                            $checked = null;
                            $item = null;
                            foreach($currentGroups as $grp) {
                                if ($gID == $grp->id) {
                                    $checked= ' checked="checked"';
                                break;
                                }
                            }
                        ?>
                        <input type="checkbox" name="groups[]" value="<?php echo $group['id'];?>"<?php echo $checked;?>>
                        <?php echo htmlspecialchars($group['name'],ENT_QUOTES,'UTF-8');?>
                        </label>
                    <?php endforeach?>

                <?php endif ?>
                </div>
                <?php echo form_hidden('id', $user->id);?>
                <?php echo form_hidden($csrf); ?>

                <p><?php echo form_submit('submit', lang('edit_user_submit_btn') , array('class' => 'btn btn-primary pull-right'));?></p>

                <?php echo form_close();?>
            </div>
          </div>                              
        </div>
      </div>
    </section>
  </div>
</div>

<?php $this->load->view('admin/common/footer'); ?>
