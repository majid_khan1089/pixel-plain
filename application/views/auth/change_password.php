<?php $this->load->view('admin/common/header'); ?>
<div class="content-wrapper">
      <div class="container">
            <section class="content-header">
                  <h1></h1><br/>
                  <ol class="breadcrumb">
                        <li><a href="javascript:void(0);"><i class="fa fa-dashboard"></i> Change Password</a></li>
                  </ol>
            </section>

            <section class="content">           
                  <div class="box box-default">
                        <div class="box-header with-border">
                              <h3 class="box-title"><?php echo lang('change_password_heading');?></h3>
                        </div>
                              <div class="box-body">
                              <?php if(isset($message) && !empty($message)) { ?>
                              <div class="alert alert-danger alert-dismissable">
                                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                    <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                                    <?php echo $message;?>
                              </div>
                              <?php } ?>
                              <div class="row">
                                    <div class="col-md-6">
                                          <?php echo form_open("auth/change_password", array());?>
                                          <div class="form-group">
                                               <label for="old_password" class="control-label">
                                                <?php echo lang('change_password_old_password_label', 'old_password');?>
                                                </label>
                                                <?php echo form_input($old_password);?>
                                          </div>
                                          <div class="form-group">
                                                <label for="new_password" class="control-label"><?php echo sprintf(lang('change_password_new_password_label'), $min_password_length);?></label>
                                                <?php echo form_input($new_password);?>
                                          </div>
                                          <div class="form-group">
                                                <label for="old_password">
                                                      <?php echo lang('change_password_new_password_confirm_label', 'new_password_confirm');?>
                                                </label>
                                                <?php echo form_input($new_password_confirm);?>
                                          </div>
                                          <?php echo form_input($user_id);?>
                                          <input type="submit" name="" value="Submit" class="btn btn-primary pull-right">
                                          <?php //echo form_submit('submit', "Submit", array('class' => 'btn btn-primary pull-right'));?>
                                    <?php echo form_close();?>
                                    </div>
                              </div>
                        </div>
                  </div>
            </section>
      </div>
</div>
<?php $this->load->view('admin/common/footer'); ?>