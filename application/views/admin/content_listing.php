<?php $this->load->view('admin/common/header'); ?>
<div class="content-wrapper">
   <div class="container">
      <section class="content-header">
         <h1></h1><br/>
         <ol class="breadcrumb">
            <li><a href="javascript:void(0);"><i class="fa fa-dashboard"></i> Home</a></li>
         </ol>
      </section>

      <section class="content">           
         <div class="box box-default">
            <div class="box-header with-border">
               <h3 class="box-title">Content Listing</h3>
               <a href="add_content" class="btn btn-primary pull-right">Add Content</a>
         </div>

         <div class="box-body">
            <div class="row">
               <div class="col-md-12">
                  <?php $this->load->view('admin/common/flash_message'); ?>
               </div><br/>         
            </div>

            <div class="row">
               <div class="col-lg-12">
                  <table class="table table-striped table-bordered table-hover" id="customTable">
                     <thead>
                        <tr>
                           <th><?php echo $this->lang->line('txt_form_sno'); ?></th>
                           <th><?php echo $this->lang->line('txt_form_campaign_name'); ?></th>
                           <th><?php echo $this->lang->line('txt_form_campaign_code'); ?></th>
                           <th><?php echo $this->lang->line('txt_form_campaign_source_id'); ?></th>                           
                           <th><?php echo $this->lang->line('txt_form_action'); ?></th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php if(!empty($listings)) { ?>
                        <?php $i=0; foreach($listings as $listing){?>
                        <tr class="odd gradeX">
                           <td><?php echo ++$i;?></td>
                           <td><?php echo ($listing->content_name) ? $listing->content_name : '-';?></td>
                           <td><?php echo ($listing->content_name) ? $listing->content_name : '-';?></td>               
                           <td><?php echo ($listing->content_code) ? $listing->content_code : '-';?></td>
                           <td>
                              <a href="<?php echo base_url('/edit-content/'.$listing->content_id); ?>" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>
                              <a href="<?php echo base_url('/delete-content/'.$listing->content_id ) ; ?>" class="btn btn-danger btn-xs"><i class="fa fa-remove"></i></a>
                           </td>
                        </tr>
                        <?php }?>
                        <?php } ?>                      
                     </tbody>
                  </table>
               </div>                  
            </div>
         </div>
      </div>
      </section>
   </div>
</div>
<?php $this->load->view('admin/common/footer'); ?>