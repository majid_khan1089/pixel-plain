<?php $this->load->view('admin/common/header'); ?>
<div class="content-wrapper">
  <div class="container">
    <section class="content-header">
          <h1><?php //echo lang('change_password_heading');?></h1><br/>
          <ol class="breadcrumb">
                <li><a href="javascript:void(0);"><i class="fa fa-dashboard"></i> Home</a></li>
                <!-- <li><a href="#">Layout</a></li>
                <li class="active">Top Navigation</li> -->
          </ol>
    </section>

    <section class="content">           
      <div class="box box-default">
        <div class="box-header with-border">
        <h3 class="box-title">Content Modification</h3>
      </div>
      <div class="box-body">

        <div class="row">
          <div class="col-md-10 col-md-offset-1">
            <form id="content_add" action="<?php echo (isset($content[0]->content_id) && !empty($content[0]->content_id)) ? '' : 'add_content'; ?>" method="POST" role="form" data-toggle="validator" class="form-horizontal">
            
              <div class="form-group">
                <label class="col-md-2 control-label" for="username">Select Campaign Name</label>
                <div class="col-md-10">
                  <select class="form-control form" name="campaign" id="campaign">
                    <option>Select Campaign</option>
                    <?php foreach($listings as $listing){?>
                    <option value="<?php echo $listing->campaign_id?>"><?php echo $listing->campaign_name?></option>
                    <?php }?>
                  </select>
                </div>
              </div>


              <div class="form-group">
                <label class="col-md-2 control-label" for="username">Content Name</label>
                <div class="col-md-10">
                  <input type="text" required placeholder="Enter Content Name" name="content_name" value="<?php echo isset($content[0]->content_name) ? set_value("content_name", $content[0]->content_name) : set_value("content_name"); ?>" id="content_name" class="form-control form">
                </div>
              </div>


              <div class="form-group">
                <label class="col-md-2 control-label" for="username">Content Code</label>
                <div class="col-md-10">
                  <input type="text" required placeholder="Enter Campaign Code" name="content_code" value="<?php echo isset($content[0]->content_code) ? set_value("content_code", $content[0]->content_code) : set_value("content_code"); ?>" id="content_code" class="form-control form">
                </div>
              </div>


              <div class="form-group">
                <div class="col-sm-12 text-right">
                  <button class="btn btn-primary" type="submit"><?php echo (isset($content[0]->content_id) && !empty($content[0]->content_id)) ? 'Update' : 'Submit'; ?></button>
                  <a class="btn btn-info" href="<?php echo base_url('admin/clients'); ?>">Cancel</a>
                </div>
              </div>
            </form>

          </div>
        </div>                              
      </div>
    </div>
  </section>
  </div>
</div>
<?php $this->load->view('admin/common/footer'); ?>