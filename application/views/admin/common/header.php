<!DOCTYPE html>
<html>
<head>
    <?php $this->load->view('admin/common/head'); ?>
</head>
<body class="hold-transition skin-blue layout-top-nav">
    <div class="wrapper">
        <header class="main-header">
            <nav class="navbar navbar-static-top">
            <div class="container">
            <div class="navbar-header">
                <a href="<?php echo base_url('auth'); ?>" class="navbar-brand"><b><?php echo $this->lang->line('txt_admin'); ?></b></a>
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                    <i class="fa fa-bars"></i>
                </button>
            </div>

            <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
                <ul class="nav navbar-nav">
                    <li <?php if($this->uri->segment(1) == 'auth' && $this->uri->segment(2)  == '') { ?>class="active" <?php } ?>><a href="<?php echo base_url('auth'); ?>">DashBoard</a></li>
                    <li <?php if($this->uri->segment(1) == 'campaign' && $this->uri->segment(2)  == '') { ?>class="active" <?php } ?>><a href="<?php echo base_url('campaign'); ?>">Campaign</a></li>
                    <li <?php if($this->uri->segment(2) == 'sendEmail') { ?>class="active" <?php } ?>><a href="<?php echo base_url('campaign/sendEmail'); ?>">Send Email</a></li>
                    <li <?php if($this->uri->segment(2) == 'registerUser') { ?>class="active" <?php } ?>><a href="<?php echo base_url('campaign/registerUser'); ?>">Client Listing</a></li>
                    <li <?php if($this->uri->segment(2) == 'listing') { ?>class="active" <?php } ?>><a href="<?php echo base_url('user/listing'); ?>">Users</a></li>
                    <li <?php if($this->uri->segment(2) == 'mail-listing') { ?>class="active" <?php } ?>><a href="<?php echo base_url('unsubscriber/mail-listing'); ?>">Unsubscriber</a></li>
                </ul>
            </div>
            
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li>
                        <?php if($this->session->userdata('lang')== 'ar') { ?>
                            <a href="javascript:void(0);" data-lang="en" id="changelang" name="en">ENGISH</a>
                        <?php }  ?>
                    </li>
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="<?php echo HTTP_IMAGES_PATH_ADMIN; ?>admin.png" class="user-image" alt="User Image">
                            <span class="hidden-xs"><?php echo $this->lang->line('txt_admin'); ?></span>
                        </a>
            
                        <ul class="dropdown-menu">
                            <li class="user-header">
                                <img src="<?php echo HTTP_IMAGES_PATH_ADMIN; ?>admin.png" class="img-circle" alt="User Image">
                                <p>Welcome Admin</p>
                            </li>
                          
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="<?php echo base_url('auth/change-password'); ?>" class="btn btn-default btn-flat"><?php echo $this->lang->line('txt_change_password'); ?></a>
                                </div>
                                
                                <div class="pull-right">
                                    <a href="<?php echo base_url();?>auth/logout" class="btn btn-default btn-flat"><?php echo $this->lang->line('txt_sign_out'); ?></a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>