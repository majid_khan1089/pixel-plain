<footer class="main-footer">
    <div class="container">
        <strong>Copyright &copy;  <?php echo date("Y"); ?>. </strong> All rights reserved.
    </div>
</footer>
</div>


<script src="<?php echo HTTP_JS_PATH_ADMIN; ?>bootstrap.min.js"></script>
<script src="<?php echo HTTP_PLUG_PATH_ADMIN; ?>slimScroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo HTTP_PLUG_PATH_ADMIN; ?>fastclick/fastclick.min.js"></script>
<script src="<?php echo HTTP_JS_PATH_ADMIN; ?>app.min.js"></script>
<script src="<?php echo HTTP_JS_PATH_ADMIN; ?>demo.js"></script>
<script src="<?php echo HTTP_PLUG_PATH_ADMIN; ?>datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo HTTP_PLUG_PATH_ADMIN; ?>datatables/dataTables.bootstrap.min.js"></script>
<script src="<?php echo HTTP_JS_PATH_ADMIN;?>fileinput.min.js"></script>

<script src="<?php echo HTTP_JS_PATH_ADMIN;?>jquery.validate.js"></script>

<script src="<?php echo HTTP_JS_PATH_ADMIN;?>custom.js"></script>
</body>
</html>