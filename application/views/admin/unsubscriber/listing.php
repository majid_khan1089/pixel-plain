<?php $this->load->view('admin/common/header'); ?>
<div class="content-wrapper">
    <div class="container">
        <section class="content-header">
            <h1></h1>
            <br/>
            <ol class="breadcrumb">
                <li><a href="javascript:void(0);"><i class="fa fa-dashboard"></i> Home</a></li>
            </ol>
        </section>

        <section class="content">
            <div class="box box-default">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <?php $this->load->view('admin/common/flash_message'); ?>
                        </div>
                        <br/>         
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                             <table class="table table-striped table-bordered table-hover" id="registeredClients">
                                <thead>
                                    <tr>
                                        <th>Sr. No.</th>
                                        <th>Email</th>
                                        <th>Date</th>                                        
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<script type="text/javascript"> 
    var table; 
    jQuery(document).ready(function($) { 
        //datatables
        table = $('#registeredClients').DataTable({ 
            "iDisplayLength": 25,
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                "url": "<?php echo site_url('unsubscriber/getUnsubscriberDatatable')?>",
                "type": "POST"
            },
            "columnDefs": [
                {
                    "targets": [0],
                    "orderable": false,
                },
            ],
        });
    });
</script>
<?php $this->load->view('admin/common/footer'); ?>