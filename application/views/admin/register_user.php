<?php $this->load->view('admin/common/header'); ?>
<div class="content-wrapper">
   <div class="container">
      <section class="content-header">
         <h1></h1><br/>
         <ol class="breadcrumb">
            <li><a href="javascript:void(0);"><i class="fa fa-dashboard"></i> Home</a></li>
         </ol>
      </section>

      <section class="content">
         <div class="box box-default">
            <div class="box-body">
               <div class="row">
                  <div class="col-md-12">
                     <?php $this->load->view('admin/common/flash_message'); ?>
                  </div><br/>         
               </div>

               <div class="row">
                  <div class="col-lg-12">
                     <table class="table table-striped table-bordered table-hover" id="customTable">
                        <thead>
                           <tr>
                              <th><?php echo $this->lang->line('txt_form_sno'); ?></th>
                              <th>Campaign Name</th>
                              <th>Name</th>
                              <th>Email Address</th>
                              <th>Company</th>                           
                             <!-- <th>Date</th>-->
                              <th>Action</th>
                           </tr>
                        </thead>
                        <tbody>
                           <?php if(!empty($listings)) { ?>
                              <?php $i=0; foreach($listings as $listing){?>
                              <tr class="odd gradeX">
                                 <td><?php echo ++$i;?></td>
                                 <td><?php echo ($listing->campaign_type) ?  ucwords(str_replace('_', ' ', $listing->campaign_type)): '-';?></td>
                                 <td><?php echo ($listing->f_name) ? ucfirst($listing->f_name) .' ' . ucfirst($listing->l_name) : '-';?></td>
                                 <td><?php echo ($listing->email) ? $listing->email : '-';?></td>
                                 <td><?php echo ($listing->company) ? $listing->company : '-';?></td>
                                <!-- <td><?php // echo ($listing->date_added) ? $listing->date_added : '-';?></td>-->
                                 <td>
                                    <a href="javascript:void(0);" data-id="<?php echo $listing->user_id; ?>" class="btn btn-success btn-xs viewClientDetails">
                                       <i class="fa fa-eye" aria-hidden="true"></i>
                                    </a>
                                 </td>
                              </tr>
                              <?php }?>
                           <?php } ?>                      
                        </tbody>
                     </table>
                  </div>                  
               </div>
            </div>
         </div>
      </section>
   </div>
</div>

<!-- Modal -->
<div class="modal fade" id="clientModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Client Detail</h4>
      </div>
      <div class="modal-body"></div>
      <div class="modal-footer">
         <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<?php $this->load->view('admin/common/footer'); ?>