<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"><head>
    
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>How far could you go if everything got done automatically</title>
    
    <!-- Styles -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />
<link href="http://martechb2b.com/email-template/HTML/nintex/css/styles.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
 
  <style type="text/css">
            input[type=submit] {
          background:url(http://martechb2b.com/email-template/HTML/nintex/images/form-button-submit.jpg);
          color: transparent;
          width: 135px;
          height: 38px;
        }
        .error{
            color: red;
            font-weight: normal;
        }
        </style>
  

</head>
<body>

    
<!-- Fixed navbar -->
<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="/">
				<img src="http://martechb2b.com/email-template/HTML/nintex/images/Nintex_Logo-small.png" alt="" />
			</a>
		</div>
		<!-- the actual nav items -->
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav"></ul>
		</div>
		<!--/.nav-collapse -->
	</div>
</nav>
<!-- main content -->
<div class="container">
  <br />
	<div class="row">
		<!-- main column -->
		<div class="col-sm-9">
                <div class="form-page-content">
                  <h1>How far could you go if everything got done automatically?</h1>
               
             <div class="asset-image"><img src="http://martechb2b.com/email-template/HTML/nintex/images/Nitnex-BuyersGuide.jpg" /></div>
             <h3>Work together, and get more work done.</h3>
                <p>	Automation means collaboration on an entirely new level: your 
work flows from person to person, system to system, to the cloud 
and back—without bottlenecks or breakdowns. So your teams can 
work smarter, faster and be more connected than ever before.</p>
                <p>When the market shifts, business processes have to adapt 
quickly or the business fails. And if the change happens only on 
paper, it hasn't really happened. An automated workflow system 
can be quickly reconfigured—with a few quick clicks and 
without IT—so new processes don't mean new problems.</p>
             <h3> Work faster because you're not wasting time. </h3>   
          <p>There are two kinds of work: the things you do every day, and the things you 
do every now and then. They both require collaboration, but the former can 
be a time killer if you're waiting for documents to go through, emails to be 
answered, and phone calls to be returned. Workflow automation gets the right 
task to the right people at the right time, drastically improving efficiency.
 </p>      
<h3>Improve the work, not just the process. </h3>
<p>When processes are automated, nothing slips 
through the cracks. Projects don't get held up, data 
doesn't get lost, and the work—not the process—is 
the focus. That means your people can do what they 
were hired to do: the very best possible work </p>

<h3> Create documents automatically. </h3> 

<p> Cutting paper out of the process doesn't mean eliminating 
important paperwork—like proposals and contracts. 
But creating these documents can be cumbersome and 
error-prone. Automation fixes all that. You can easily merge 
data from SharePoint and Office 365 into any combination 
of Word, Excel, PowerPoint or PDF documents, so the 
process—and your people—can keep moving.</p>

<br />
                </div>
            </div>

				<!-- right column -->
				<div class="col-sm-3">
					<div class="panel panel-primary reg-form">
						<div class="dnfrmc full" id="dnfrmmask">
                        
                        <form class="autoform" id="nintex" method="Post" name="nintex">
                         <input type="hidden" name="form-type" value="nintex-download">
							<div class="reg-form-panel">
								<div id="leadform-error"></div>
								<p class="intro">Please fill in the information below to access this resource:</p>
								<div class="row">
									<div class="col-sm-12" style="padding-left: 10px; padding-right: 10px;">
										<div class="form-group" id="question1-group">
											<label class="sr-only" for="question1">First Name</label>
											<input type="text" class="form-control input-sm lfreq tblfreq" id="f_name" name="f_name" maxlength="50"  placeholder="First Name" required />
										</div>
										<div class="form-group" id="question2-group">
											<label class="sr-only" for="question2">Last Name</label>
											<input type="text" class="form-control input-sm lfreq tblfreq" id="l_name" name="l_name" maxlength="50"  placeholder="Last Name" required/>
										</div>
										<div class="form-group" id="question8-group">
											<label class="sr-only" for="question8">Email Address</label>
											<input type="text" class="form-control input-sm lfreq tblfreq email" id="email" name="email" maxlength="100"  placeholder="Email Address" required/>
										</div>
										<div class="form-group" id="question9-group">
											<label class="sr-only" for="question9">Telephone</label>
											<input type="text" class="form-control input-sm lfreq tblfreq tel" id="phone" name="phone" maxlength="50"  placeholder="Telephone" required/>
										</div>
										<div class="form-group" id="question3-group">
											<label class="sr-only" for="question3">Company Name</label>
											<input type="text" class="form-control input-sm lfreq tblfreq" id="company" name="company" maxlength="80"  placeholder="Company Name" required/>
										</div>
										<div class="form-group" id="question110-group">
											<label class="sr-only" for="question110">Company Size</label>
											<select class="form-control input-sm lfreq" required="required" id="company_size" name="company_size" placeholder="Company Size">
												<option value="">Select Company Size</option>
												<option value="1 to 3">1 to 3</option>
												<option value="4 to 10">4 to 10</option>
												<option value="11 to 25">11 to 25</option>
												<option value="26 to 50">26 to 50</option>
												<option value="51 to 100">51 to 100</option>
												<option value="101 to 200">101 to 200</option>
												<option value="201 to 500">201 to 500</option>
												<option value="501 to 1000">501 to 1000</option>
												<option value="1001 or more">1001 or more</option>
											</select>
										</div>
										<div class="form-group" id="question141-group">
											<label class="sr-only" for="question141">Job Function</label>
											<select class="form-control input-sm lfreq" required="required" id="job_title_level" name="job_title_level" placeholder="Job Title">
												<option value="">Select Job Title</option>
												<option value="Finance Director">Finance Director</option>
												<option value="VP Finance">VP Finance</option>
												<option value="Controller">Controller</option>
												<option value="Head of Finance">Head of Finance</option>
												<option value="CFO">CFO</option>
												<option value="HR Director">HR Director</option>
												<option value="VP HR">VP HR</option>
												<option value="Head of HR">Head of HR</option>
												<option value="CHRO">CHRO</option>
												<option value="Head Recruitment and Compensation">Head Recruitment and Compensation</option>
												<option value="Director Compensation and Payroll">Director Compensation and Payroll</option>
												<option value="IT Director">IT Director</option>
												<option value="Director Information Technology">Director Information Technology</option>
												<option value="VP Technology">VP Technology</option>
												<option value="Head IT">Head IT</option>
												<option value="Director Sales">Director Sales</option>
												<option value="Sales and Marketing Director">Sales and Marketing Director</option>
												<option value="VP Sales">VP Sales</option>
												<option value="Head of Sales">Head of Sales</option>
												<option value="Head of Marketing and Sales">Head of Marketing and Sales</option>
											</select>
										</div>
										<div class="form-group" id="question113-group">
											<label class="sr-only" for="question113">Industry</label>
											<select class="form-control input-sm lfreq" required="required" id="industry" name="industry" placeholder="Industry">
												<option value="">Select Industry</option>
												<option value="Advertising/Marketing">Advertising/Marketing</option>
												<option value="Automotive">Automotive</option>
												<option value="Agriculture/Forestry">Agriculture/Forestry</option>
												<option value="Business Services">Business Services</option>
												<option value="Consulting">Consulting</option>
												<option value="Distribution">Distribution</option>
												<option value="Entertainment">Entertainment</option>
												<option value="Education">Education</option>
												<option value="Engineering/Construction">Engineering/Construction</option>
												<option value="Financial">Financial</option>
												<option value="Food and Beverage">Food and Beverage</option>
												<option value="Government-Federal">Government-Federal</option>
												<option value="Government-State and Local">Government-State and Local</option>
												<option value="Healthcare/Medical">Healthcare/Medical</option>
												<option value="Hospitality">Hospitality</option>
												<option value="Insurance">Insurance</option>
												<option value="Legal">Legal</option>
												<option value="Manufacturing">Manufacturing</option>
												<option value="Non-profit">Non-profit</option>
												<option value="Real-Estate">Real-Estate</option>
												<option value="Retail">Retail</option>
												<option value="Student/Retired/Non-Business">Student/Retired/Non-Business</option>
												<option value="Transportation/Shipping">Transportation/Shipping</option>
												<option value="Technology">Technology</option>
												<option value="Telecommunications">Telecommunications</option>
												<option value="Utilities">Utilities</option>
												<option value="Warehousing">Warehousing</option>
												<option value="Other">Other</option>
											</select>
										</div>
										<div class="form-group" id="question7-group">
											<label class="sr-only" for="question7">Zip Code</label>
											<input type="text" class="form-control input-sm lfreq tblfreq zip" id="zip" name="zip" maxlength="5"  placeholder="Zip Code" value="" required/>
										</div>
                                       
                                       <div class="form-group" id="question110-group">
											<label for="question110" style="font-weight:normal"> Does your organization use Microsoft Office 365?</label>
											<select class="form-control input-sm lfreq" required="required" id="custom" name="custom" placeholder="state">
												
                                   <option value="">Select</option>
                                   <option value="Yes">Yes</option>
								   <option value="No">No</option>
								          </select>
                                                                        </div>
										<p class="buttons text-center">
                                        
                                        <input type="submit">
                                        
											<!--<a href="download-page.html">
												<img src="images/form-button-submit.jpg" width="135" height="36">
												</a>  -->      
							        </p>
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
                                </form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<footer class="footer">
			  <div class="container">
					<p class="text-muted">© Copyright 2017, Nintex, All rights reserved.</p>
				</div>
			</footer>

    <!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
        <script type="text/javascript">
            $("#nintex").validate();
        </script>

</body>
</html>
