<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>Factory Design Utilities Webinar</title>
    
    <!-- Styles -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<link href="http://martechb2b.com/email-template/HTML/kitkat_tco78054-autodesk-final/css/styles.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

  
<style type="text/css">
            input[type=submit] {
          background:url(http://martechb2b.com/email-template/HTML/kitkat_tco78054-autodesk-final/images/View-Webinar.png);
          color: transparent;
          width: 127px;
          height: 34px;
        }
        .error{
            color: red;
            font-weight: normal;
        }
        </style>
</head>
<body>

    
<!-- Fixed navbar -->
<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container">
		<div class="navbar-header">

			<a class="navbar-brand" href="/">
				<img src="http://martechb2b.com/email-template/HTML/kitkat_tco78054-autodesk-final/images/image1.png"/>
			</a>
		</div>
		<!-- the actual nav items -->
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav"></ul>
		</div>
		<!--/.nav-collapse -->
	</div>
</nav>
<!-- main content -->
<div class="container">
	<br />
	<div class="row">
		<!-- main column -->
		<div class="col-md-8">
        <div class="row">
        	<div class="col-sm-12">
			<div class="form-page-content">
				<img class="img-responsive" src="http://martechb2b.com/email-template/HTML/kitkat_tco78054-autodesk-final/images/digital-factory-landing.jpg">
              <h3>Factory Design Utilities Webinar</h3>
	    <p>Learn how Autodesk can help you to design and visualize more efficient factory and <br> facility layouts before equipment is installed. </p>
                    
					
					<br>
			  </div>
          </div>
                    
          </div>
				</div>
				<!-- right column -->
				<div class="col-md-4">
					<div class="panel panel-primary reg-form">
						<div class="dnfrmc full" id="dnfrmmask">
                        
                        <form class="autoform" id="autodesk_video" method="Post" name="autodesk_video">
                         <input type="hidden" name="form-type" value="autodesk-video-download">
                        
							<div class="reg-form-panel">
								<div id="leadform-error"></div>
								<p class="intro">Please fill in the information below to access this resource:</p>
								<div class="row">
									<div class="col-sm-12">
										<div class="form-group" id="question1-group" style="padding-bottom:5px;">
											<label class="sr-only" for="question1">First Name</label>
											<input type="text" class="form-control input-sm lfreq tblfreq" id="f_name" name="f_name" maxlength="50"  placeholder="First Name" required />
										</div>
										<div class="form-group" id="question2-group" style="padding-bottom:5px;">
											<label class="sr-only" for="question2">Last Name</label>
											<input type="text" class="form-control input-sm lfreq tblfreq" id="l_name" name="l_name" maxlength="50"  placeholder="Last Name" required/>
										</div>
										<div class="form-group" id="question8-group" style="padding-bottom:5px;">
											<label class="sr-only" for="question8">Email Address</label>
											<input type="text" class="form-control input-sm lfreq tblfreq email" id="email" name="email" maxlength="100"  placeholder="Email Address" required/>
										</div>
										<div class="form-group" id="question9-group" style="padding-bottom:5px;">
											<label class="sr-only" for="question9">Phone</label>
											<input type="text" class="form-control input-sm lfreq tblfreq tel" id="phone" name="phone" maxlength="50"  placeholder="Phone" required/>
										</div>
										<div class="form-group" id="question3-group" style="padding-bottom:5px;">
											<label class="sr-only" for="question3">Company</label>
											<input type="text" class="form-control input-sm lfreq tblfreq" id="company" name="company" maxlength="80"  placeholder="Company" required/>
										</div>
                                        
                                        <div class="form-group" id="question3-group" style="padding-bottom:5px;">
											<label class="sr-only" for="question3">Address</label>
											<input type="text" class="form-control input-sm lfreq tblfreq" id="address" name="address" maxlength="120"  placeholder="Address" required/>
										</div>
                                        
                                        <div class="form-group" id="question3-group" style="padding-bottom:5px;">
											<label class="sr-only" for="question3">City</label>
											<input type="text" class="form-control input-sm lfreq tblfreq" id="city" name="city" maxlength="80"  placeholder="City" required/>
										</div>
                                        
                                        <div class="form-group" id="question110-group" style="padding-bottom:5px;">
											<label class="sr-only" for="question110">State</label>
											<select class="form-control input-sm lfreq" required="required" id="state" name="state" placeholder="Company Size">
												<option value="">Select State</option>
												 <option value="MI">   MI </option>
                                                 <option value="IL">   IL </option>
                                                 <option value="WI">   WI </option>
                                                 <option value="MN">   MN  </option>
                                                 <option value="IN">   IN  </option>
                                                 <option value="OH">   OH  </option>
                                                 <option value="PA">   PA </option>
                                                 <option value="MO">   MO  </option>
                                                 <option value="WA">   WA  </option>
                                                 <option value="OR">   OR  </option>
                                                 <option value="TX">   TX  </option>
                                                 <option value="OK">   OK  </option>
                                                 <option value="LA">   LA  </option>
                                                 <option value="CO">  CO </option>
                                                 <option value="MA">   MA </option>
                                                 <option value="NJ">   NJ </option>
                                                 <option value="NY">   NY </option>
                                                 <option value="MD">   MD </option>
											</select>
                                        </div>
                                        
                                        <div class="form-group" id="question110-group" style="padding-bottom:5px;">
											<label class="sr-only" for="question110">Country</label>
											<select class="form-control input-sm lfreq" required="required" id="country" name="country" placeholder="Company Size">
												<option value="">Select Country</option>
												<option value="United States">United States</option>
												
											</select>
                                        </div>
                                        
										<div class="form-group" id="question110-group" style="padding-bottom:5px;">
											<label class="sr-only" for="question110">Company Size</label>
											<select class="form-control input-sm lfreq" required="required" id="company_size" name="company_size" placeholder="Company Size">
											   <option value="">Select Company Size</option>
											   <option value="50 - 99"> 50 - 99 </option>
                                               <option value="100 - 249"> 100 - 249 </option>
                                               <option value="250 - 499"> 250 - 499 </option>
                                               <option value="500 - 999"> 500 - 999 </option>
                                               <option value="1000 - 4999"> 1000 - 4999 </option>
                                               <option value="5000 - 9999"> 5000 - 9999 </option>
                                               <option value=">10000">	>10000 </option>
											</select>
										</div>
                                        
                                        <div class="form-group" id="question110-group" style="padding-bottom:5px;">
											<label class="sr-only" for="question110">Revenue</label>
											<select class="form-control input-sm lfreq" required="required" id="revenue" name="revenue" placeholder="Company Size">
											   <option value="">Select Revenue</option>
											                                               
	                                          <option value="&#60;$25M">  &#60;$25M </option>
                                              <option value="$25M-$49M">  $25M-$49M </option>
                                              <option value="$50M-$99M">  $50M-$99M </option>
                                              <option value="$100M-$250M">  $100M-$250M </option>
                                              <option value="$250M-$1B">  $250M-$1B </option>
                                              <option value="$1B-$5B">  $1B-$5B </option>
                                              <option value="$5B-$10B">  $5B-$10B </option>
                                               <option value=">$10B">  >$10B </option>

											</select>
										</div>
                                        
                                        <div class="form-group" id="question3-group" style="padding-bottom:5px;">
											<label class="sr-only" for="question3">Job Title</label>
											<input type="text" class="form-control input-sm lfreq tblfreq" id="job_title" name="job_title" maxlength="80"  placeholder="Job Title" required/>
										</div>
                                        <div class="form-group" id="question110-group" style="padding-bottom:5px;">
											<label class="sr-only" for="question110">Industry</label>
											<select class="form-control input-sm lfreq" required="required" id="industry" name="industry" placeholder="Industry">
												<option value="">Select Industry </option>
												<option value="Manufacturing">Manufacturing</option>
												
											</select>
                                        </div>
										<div class="form-group" id="question141-group" style="padding-bottom:5px;">
											<label class="sr-only" for="question141">Job Function</label>
											<select class="form-control input-sm lfreq" required="required" id="job_function" name="job_function" placeholder="Job Function">
												<option value="">Select Job Function</option>
												<option value="IT Management(Manager IS/IT)">IT Management(Manager IS/IT)</option>
												<option value="Senior Non IT Management(CEO,CFO,VP,Director)">Senior Non IT Management(CEO,CFO,VP,Director)</option>
												<option value="Senior IT Management(CIO/CTO/VP/Director)">Senior IT Management(CIO/CTO/VP/Director)</option>
												<option value="Network Management/Administration">Network Management/Administration</option>
												<option value="IT Staff">IT Staff</option>
												<option value="CSO">CSO</option>
												<option value="Product Management">Product Management</option>
												<option value="Sales/Marketing">Sales/Marketing</option>
												<option value="Telecommunications Manager">Telecommunications Manager</option>
												<option value="Systems Management/Administration">Systems Management/Administration</option>
												<option value="Project Manager">Project Manager</option>
												<option value="Programmer/Developer">Programmer/Developer</option>
												<option value="Privacy Officer">Privacy Officer</option>
												<option value="Consultant/Systems Integrator">Consultant/Systems Integrator</option>
												<option value="Database Administrator">Database Administrator</option>
												<option value="Educator">Educator</option>
												<option value="Non IT Staff">Non IT Staff</option>
												<option value="Architect">Architect</option>
												<option value="Application Manager">Application Manager</option>
												<option value="Analyst">Analyst</option>
											</select>
										</div>
										
                                  </div>
									
							  </div>
								<p class="buttons text-center">
									<input type="submit">
									</p>
									
						    <div class="clearfix"></div>
								</div>
                                </form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<footer class="footer">
				<div class="container">
					<p class="text-muted text-center">Copyright &copy; 2017 Business Tech Alert</p>
				</div>
			</footer>

    <!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
        <script type="text/javascript">
            $("#autodesk_video").validate();
        </script>

</body>
</html>
