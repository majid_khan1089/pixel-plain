<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"><head>
    
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>Magic Quadrant for Content Collaboration Platforms</title>
    
    <!-- Styles -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />
<link href="http://martechb2b.com/email-template/HTML/kitkat-pes78013-garter/css/styles.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
 
  
   <style type="text/css">
            input[type=submit] {
          background:url(http://martechb2b.com/email-template/HTML/kitkat-pes78013-garter/images/form-button-submit.jpg);
          color: transparent;
          width: 135px;
          height: 38px;
        }
        .error{
            color: red;
            font-weight: normal;
        }
        </style>
       
  

</head>
<body>

    
<!-- Fixed navbar -->
<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container">
		<div class="navbar-header">
			
			<a class="navbar-brand">
				<img src="http://martechb2b.com/email-template/HTML/kitkat-pes78013-garter/images/Gartner_logo_landing.png" alt="" />
			</a>
		</div>
		<!-- the actual nav items -->
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav"></ul>
		</div>
		<!--/.nav-collapse -->
	</div>
</nav>
<!-- main content -->
<div class="container">
	<br />
	<div class="row">
		<!-- main column -->
		<div class="col-sm-8">
                <div class="form-page-content">
                  <h1>Magic Quadrant for Content Collaboration Platforms</h1>
             <div> </div>   
             <div class="asset-image"><img src="http://martechb2b.com/email-template/HTML/kitkat-pes78013-garter/images/314654_0001.png" /></div>
                <p>Content collaboration platforms are the evolution of the EFSS destination products and address digital workplace enablement strategies. Application leaders can choose among a range of options to transform individual productivity, team collaboration and business workflow automation.  </p>
                
                <p> Gartner defines the CCP market as a range of content-centric platforms enabling secure file productivity and content collaboration for individuals and teams as well as supporting the creation of a modern digital workplace. Core functionalities include mobile access to content repositories; file synchronization across devices and cloud repositories; file sharing with people and applications inside or outside the organization; and file search and retrieval across multiple file repositories. In addition, these platforms support, at different levels, collaborative document creation and teamwork, lightweight content management, and workflows, natively or through integration with third-party tools. Deployments can be in public cloud, hybrid cloud, private cloud or on-premises architectures. CCP offerings present different levels of support. </p>
                
                
                                		
<br />                


                </div>
            </div>

				<!-- right column -->
				<div class="col-sm-4">
					<div class="panel panel-primary reg-form">
						<div class="dnfrmc full" id="dnfrmmask">
                        <form class="autoform" id="garter_magic" method="Post" name="garter_magic">
							<div class="reg-form-panel">
								<div id="leadform-error"></div>
								<p class="intro">Please fill in the information below to access this resource:</p>
								<div class="row">
									<div class="col-sm-12" style="padding-left: 10px; padding-right: 10px;">
										<div class="form-group" id="question1-group">
											<label class="sr-only" for="question1">First Name</label>
											<input type="text" class="form-control input-sm lfreq tblfreq" id="f_name" name="f_name" maxlength="50"  placeholder="First Name" required />
										</div>
										<div class="form-group" id="question2-group">
											<label class="sr-only" for="question2">Last Name</label>
											<input type="text" class="form-control input-sm lfreq tblfreq" id="l_name" name="l_name" maxlength="50"  placeholder="Last Name" required/>
										</div>
										<div class="form-group" id="question8-group">
											<label class="sr-only" for="question8">Email Address</label>
											<input type="text" class="form-control input-sm lfreq tblfreq email" id="email" name="email" maxlength="100"  placeholder="Email Address" required/>
										</div>
										<div class="form-group" id="question9-group">
											<label class="sr-only" for="question9">Telephone</label>
											<input type="text" class="form-control input-sm lfreq tblfreq tel" id="phone" name="phone" maxlength="50"  placeholder="Telephone" required/>
										</div>
										<div class="form-group" id="question3-group">
											<label class="sr-only" for="question3">Company Name</label>
											<input type="text" class="form-control input-sm lfreq tblfreq" id="company" name="company" maxlength="80"  placeholder="Company Name" required/>
										</div>
										<div class="form-group" id="question110-group">
											<label class="sr-only" for="question110">Company Size</label>
											<select class="form-control input-sm lfreq" required="required" id="company_size" name="company_size" placeholder="Company Size">
												<option value="">Select Company Size</option>
												<option value="1 to 3">1 to 3</option>
												<option value="4 to 10">4 to 10</option>
												<option value="11 to 25">11 to 25</option>
												<option value="26 to 50">26 to 50</option>
												<option value="51 to 100">51 to 100</option>
												<option value="101 to 200">101 to 200</option>
												<option value="201 to 500">201 to 500</option>
												<option value="501 to 1000">501 to 1000</option>
												<option value="1001 or more">1001 or more</option>
											</select>
										</div>
										<div class="form-group" id="question141-group">
											<label class="sr-only" for="question141">Job Function</label>
											<select class="form-control input-sm lfreq" required="required" id="job_title_level" name="job_title_level" placeholder="Job Function">
												<option value="">Select Job Function</option>
												<option value="IT Management(Manager IS/IT)">IT Management(Manager IS/IT)</option>
												<option value="Senior Non IT Management(CEO,CFO,VP,Director)">Senior Non IT Management(CEO,CFO,VP,Director)</option>
												<option value="Senior IT Management(CIO/CTO/VP/Director)">Senior IT Management(CIO/CTO/VP/Director)</option>
												<option value="Network Management/Administration">Network Management/Administration</option>
												<option value="IT Staff">IT Staff</option>
												<option value="CSO">CSO</option>
												<option value="Product Management">Product Management</option>
												<option value="Sales/Marketing">Sales/Marketing</option>
												<option value="Telecommunications Manager">Telecommunications Manager</option>
												<option value="Systems Management/Administration">Systems Management/Administration</option>
												<option value="Project Manager">Project Manager</option>
												<option value="Programmer/Developer">Programmer/Developer</option>
												<option value="Privacy Officer">Privacy Officer</option>
												<option value="Consultant/Systems Integrator">Consultant/Systems Integrator</option>
												<option value="Database Administrator">Database Administrator</option>
												<option value="Educator">Educator</option>
												<option value="Non IT Staff">Non IT Staff</option>
												<option value="Architect">Architect</option>
												<option value="Application Manager">Application Manager</option>
												<option value="Analyst">Analyst</option>
											</select>
										</div>
										<div class="form-group" id="question113-group">
											<label class="sr-only" for="question113">Industry</label>
											<select class="form-control input-sm lfreq" required="required" id="industry" name="industry" placeholder="Industry">
												<option value="">Select Industry</option>
												<option value="Advertising/Marketing">Advertising/Marketing</option>
												<option value="Automotive">Automotive</option>
												<option value="Agriculture/Forestry">Agriculture/Forestry</option>
												<option value="Business Services">Business Services</option>
												<option value="Consulting">Consulting</option>
												<option value="Distribution">Distribution</option>
												<option value="Entertainment">Entertainment</option>
												<option value="Education">Education</option>
												<option value="Engineering/Construction">Engineering/Construction</option>
												<option value="Financial">Financial</option>
												<option value="Food and Beverage">Food and Beverage</option>
												<option value="Government-Federal">Government-Federal</option>
												<option value="Government-State and Local">Government-State and Local</option>
												<option value="Healthcare/Medical">Healthcare/Medical</option>
												<option value="Hospitality">Hospitality</option>
												<option value="Insurance">Insurance</option>
												<option value="Legal">Legal</option>
												<option value="Manufacturing">Manufacturing</option>
												<option value="Non-profit">Non-profit</option>
												<option value="Real-Estate">Real-Estate</option>
												<option value="Retail">Retail</option>
												<option value="Student/Retired/Non-Business">Student/Retired/Non-Business</option>
												<option value="Transportation/Shipping">Transportation/Shipping</option>
												<option value="Technology">Technology</option>
												<option value="Telecommunications">Telecommunications</option>
												<option value="Utilities">Utilities</option>
												<option value="Warehousing">Warehousing</option>
												<option value="Other">Other</option>
											</select>
										</div>
										<div class="form-group" id="question7-group">
											<label class="sr-only" for="question7">Zip Code</label>
											<input type="text" class="form-control input-sm lfreq tblfreq zip" id="zip" name="zip" maxlength="5"  placeholder="Zip Code" value="" required/>
										</div>
                                        
                                        <p class="buttons text-center">
											
                                            <input type="submit">
                                         
							        </p>
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
                                </form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<footer class="footer">
				<div class="container">
					<p class="text-muted">© Copyright 2017, Gartner, All rights reserved.</p>
				</div>
			</footer>

    <!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
        <script type="text/javascript">
            $("#garter_magic").validate();
        </script>
</body>
</html>
