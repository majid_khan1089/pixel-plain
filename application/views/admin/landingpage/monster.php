<html>
<head>
	<title>Monster | A Guide to the Changing Workforce</title>
    <link rel="icon" type="image/png" href="https://securemedia.newjobs.com/favicon.ico" sizes="32x32">

	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script src="https://code.jquery.com/jquery-3.2.1.min.js" ></script>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" >

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

	<link rel="stylesheet" href="http://martechb2b.com/email-template/HTML/monster/css/jquery-ui.css">
	<link rel="stylesheet" href="http://martechb2b.com/email-template/HTML/monster/css/style.css">
    
        <style type="text/css">
            input[type=submit] {
          background:url(http://martechb2b.com/email-template/HTML/monster/images/submit.jpg);
          color: transparent;
          width: 318px;
          height: 41px;
        }
        .error{
            color: red;
            font-weight: normal;
        }
        </style>
</head>
<body>
	<div class="container-fluid logo">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-xs-12 left">
					<img src="http://martechb2b.com/email-template/HTML/monster/images/Monster_Jobs_logo_small.png">
			  </div>
				<div class="col-lg-6 col-xs-12 right">

				</div>
			</div>
			
		</div>
	</div>

	<div class="container-fluid title">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<h1 style="text-transform: uppercase;">A Guide to the Changing Workforce</h1>
				</div>
			</div>
		</div>
	</div>

	<div class="container-fluid content">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-xs-12">
					
					<p><img src="http://martechb2b.com/email-template/HTML/monster/images/cover-1.png" style="float: right; margin-left: 10px; margin-bottom: 10px; width: 150px; border: 1px solid black;" />Today's workforce is experiencing significant change. Are you prepared for how these trends could impact your business?
			    <p>The Changing Workforce Hiring Guide will help you create a winning recruitment strategy now - and for the months and years ahead.</p>
					<div style="clear: right;"></div>
				</div>
				<div class="col-lg-4 col-xs-12">
					<!--  -->
					<div class="form">
						 <form class="autoform" id="monster" method="Post" name="monster">
                         <input type="hidden" name="form-type" value="sap-seven-key-download">
							<div class="reg-form-panel">
								<div id="leadform-error"></div>
								<p class="intro text-center" style="font-size:18px;"><strong>Register to download the free Guide</strong></p>
								<div class="row">
									<div class="col-sm-12" style="padding-left: 10px; padding-right: 10px;">
										<div class="form-group" id="question1-group">
											<label class="sr-only" for="question1">First Name</label>
											<input type="text" class="form-control input-sm lfreq tblfreq" id="f_name" name="f_name" maxlength="50"  placeholder="First Name" required />
										</div>
										<div class="form-group" id="question2-group">
											<label class="sr-only" for="question2">Last Name</label>
											<input type="text" class="form-control input-sm lfreq tblfreq" id="l_name" name="l_name" maxlength="50"  placeholder="Last Name" required/>
										</div>
                                          <div class="form-group" id="question7-group">
											<label class="sr-only" for="question7">Job Title</label>
											<input type="text" class="form-control input-sm lfreq tblfreq zip" id="job_title" name="job_title" maxlength="80"  placeholder="Job Title" value="" required/>
										</div>
                                        
                                        <div class="form-group" id="question3-group">
											<label class="sr-only" for="question3">Company Name</label>
											<input type="text" class="form-control input-sm lfreq tblfreq" id="company" name="company" maxlength="80"  placeholder="Company Name" required/>
										</div>
                                        
										<div class="form-group" id="question8-group">
											<label class="sr-only" for="question8">Email Address</label>
											<input type="text" class="form-control input-sm lfreq tblfreq email" id="email" name="email" maxlength="100"  placeholder="Email Address" required/>
										</div>
										<div class="form-group" id="question9-group">
											<label class="sr-only" for="question9">Phone</label>
											<input type="text" class="form-control input-sm lfreq tblfreq tel" id="phone" name="phone" maxlength="50"  placeholder="Telephone" required/>
										</div>
                                                                               
                                        <div class="form-group" id="question7-group">
											<label class="sr-only" for="question7">Address</label>
											<input type="text" class="form-control input-sm lfreq tblfreq zip" id="address" name="address" maxlength="100"  placeholder="Address" value="" required/>
										</div>
                                        
                                      
										<p class="buttons text-center">
                                        
                                        <input type="submit">
                                        
											<!--<a href="download-page.html">
												<img src="images/form-button-submit.jpg" width="135" height="36">
												</a>  -->      
							        </p>
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
                                </form>
					</div>
				</div>
			</div>
		</div>
	</div>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
        <script type="text/javascript">
            $("#monster").validate();
        </script>
</body>
</html>

