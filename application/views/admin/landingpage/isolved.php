<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"><head>
    
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>Moving Beyond Payroll Applications: A Look at The Future of HCM Solutions</title>
    
    <!-- Styles -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />
<link href="http://martechb2b.com/email-template/HTML/isolved/css/styles.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
 
  <style type="text/css">
            input[type=submit] {
          background:url(http://martechb2b.com/email-template/HTML/isolved/images/form-button-submit.jpg);
          color: transparent;
          width: 135px;
          height: 38px;
        }
        .error{
            color: red;
            font-weight: normal;
        }
        </style>

</head>
<body>

    
<!-- Fixed navbar -->
<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="/">
				<img src="http://martechb2b.com/email-template/HTML/isolved/images/PayChoiceLogo2-small.png" alt="" />
			</a>
		</div>
		<!-- the actual nav items -->
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav"></ul>
		</div>
		<!--/.nav-collapse -->
	</div>
</nav>
<!-- main content -->
<div class="container">
	<br />
	<div class="row">
		<!-- main column -->
		<div class="col-sm-9">
                <div class="form-page-content">
                  <h1>Moving Beyond Payroll Applications: A Look at The Future of HCM Solutions</h1>
               
             <div class="asset-image"><img src="http://martechb2b.com/email-template/HTML/isolved/images/iSolved-HCM-Moving-Beyond-Payroll-Applications-A-Look-at-The-Future.jpg" /></div>
                <p>	Streamlining the tasks of the day is one of the great obstacles HR professionals face on a daily basis. While they were once out of reach for small and midsized companies, human capital management (HCM) solutions are becoming more affordable and effective than ever. They are able to unite multiple business functions under a single umbrella – from benefits to payroll – while utilizing cloud computing to its full potential for unrivaled processing speeds. Accessibility across devices, advanced analytics, and an all-in-one streamlined platform are just a few of the things you can expect from HCM applications. Here's an in-depth look at how this technology is taking the industry by storm. </p>
                
                              
<br />
                </div>
            </div>

				<!-- right column -->
				<div class="col-sm-3">
					<div class="panel panel-primary reg-form">
						<div class="dnfrmc full" id="dnfrmmask">
                        <form class="autoform" id="isolved" method="Post" name="isolved">
                         <input type="hidden" name="form-type" value="isolved-download">
							<div class="reg-form-panel">
								<div id="leadform-error"></div>
								<p class="intro">Please fill in the information below to access this resource:</p>
								<div class="row">
									<div class="col-sm-12" style="padding-left: 10px; padding-right: 10px;">
										<div class="form-group" id="question1-group">
											<label class="sr-only" for="question1">First Name</label>
											<input type="text" class="form-control input-sm lfreq tblfreq" id="f_name" name="f_name" maxlength="50"  placeholder="First Name" required />
										</div>
										<div class="form-group" id="question2-group">
											<label class="sr-only" for="question2">Last Name</label>
											<input type="text" class="form-control input-sm lfreq tblfreq" id="l_name" name="l_name" maxlength="50"  placeholder="Last Name" required/>
										</div>
										<div class="form-group" id="question8-group">
											<label class="sr-only" for="question8">Email Address</label>
											<input type="text" class="form-control input-sm lfreq tblfreq email" id="email" name="email" maxlength="100"  placeholder="Email Address" required/>
										</div>
										<div class="form-group" id="question9-group">
											<label class="sr-only" for="question9">Telephone</label>
											<input type="text" class="form-control input-sm lfreq tblfreq tel" id="phone" name="phone" maxlength="50"  placeholder="Telephone" required/>
										</div>
										<div class="form-group" id="question3-group">
											<label class="sr-only" for="question3">Company Name</label>
											<input type="text" class="form-control input-sm lfreq tblfreq" id="company" name="company" maxlength="80"  placeholder="Company Name" required/>
										</div>
										<div class="form-group" id="question110-group">
											<label class="sr-only" for="question110">Company Size</label>
											<select class="form-control input-sm lfreq" required="required" id="company_size" name="company_size" placeholder="Company Size">
												<option value="">Select Company Size</option>
												<option value="51 to 100">51 to 100</option>
												<option value="101 to 200">101 to 200</option>
												<option value="201 to 500">201 to 500</option>
												<option value="501 to 1000">501 to 1000</option>
												<option value="1001 or more">1,001 or 5,000</option>
											</select>
										</div>
										<div class="form-group" id="question141-group">
											<label class="sr-only" for="question141">Job Function</label>
											<select class="form-control input-sm lfreq" required="required" id="job_title_level" name="job_title_level" placeholder="Job Function">
												<option value="">Select Job Title</option>
												<option value="IT Management(Manager IS/IT)">IT Management(Manager IS/IT)</option>
												<option value="Senior Non IT Management(CEO,CFO,VP,Director)">Senior Non IT Management(CEO,CFO,VP,Director)</option>
												<option value="Senior IT Management(CIO/CTO/VP/Director)">Senior IT Management(CIO/CTO/VP/Director)</option>
												<option value="CFO">CFO</option>
                                                <option value="CEO">CEO</option>
												<option value="Sales/Marketing">Sales/Marketing</option>
												<option value="HR Director">HR Director</option>
												<option value="VP Director">VP Director</option>
												<option value="Head HR">Head HR</option>
												<option value="CHRO">CHRO</option>
												<option value="Director Employment Engagement">Director Employment Engagement</option>
												<option value="Director Talent Accqusition/VP">Director Talent Accqusition/VP</option>
												<option value="Director Recruitment">Director Recruitment</option>
												<option value="Staffing">Staffing</option>
                                                <option value="HR Business Partner">HR Business Director</option>
                                                <option value="Employee Relations Advisor">Employee Relations Advisor</option>
                                                <option value="HR Manager">HR Manager</option>
                                                <option value="Employment Manager">Employment Manager</option>
                                                <option value="Contract Recruiter">Contract Recruiter</option>
											</select>
										</div>
										<div class="form-group" id="question113-group">
											<label class="sr-only" for="question113">Industry</label>
											<select class="form-control input-sm lfreq" required="required" id="industry" name="industry" placeholder="Industry">
												<option value="">Select Industry</option>
												<option value="Advertising/Marketing">Advertising/Marketing</option>
												<option value="Automotive">Automotive</option>
												<option value="Agriculture/Forestry">Agriculture/Forestry</option>
												<option value="Business Services">Business Services</option>
												<option value="Consulting">Consulting</option>
												<option value="Distribution">Distribution</option>
												<option value="Entertainment">Entertainment</option>
												<option value="Education">Education</option>
												<option value="Engineering/Construction">Engineering/Construction</option>
												<option value="Financial">Financial</option>
												<option value="Food and Beverage">Food and Beverage</option>
												<option value="Government-Federal">Government-Federal</option>
												<option value="Government-State and Local">Government-State and Local</option>
												<option value="Healthcare/Medical">Healthcare/Medical</option>
												<option value="Hospitality">Hospitality</option>
												<option value="Insurance">Insurance</option>
												<option value="Legal">Legal</option>
												<option value="Manufacturing">Manufacturing</option>
												<option value="Non-profit">Non-profit</option>
												<option value="Real-Estate">Real-Estate</option>
												<option value="Retail">Retail</option>
												<option value="Student/Retired/Non-Business">Student/Retired/Non-Business</option>
												<option value="Transportation/Shipping">Transportation/Shipping</option>
												<option value="Technology">Technology</option>
												<option value="Telecommunications">Telecommunications</option>
												<option value="Utilities">Utilities</option>
												<option value="Warehousing">Warehousing</option>
												<option value="Other">Other</option>
											</select>
										</div>
										<div class="form-group" id="question7-group">
											<label class="sr-only" for="question7">Zip Code</label>
											<input type="text" class="form-control input-sm lfreq tblfreq zip" id="zip" name="zip" maxlength="5"  placeholder="Zip Code" value="" required/>
										</div>
                                       
                                       <div class="form-group" id="question110-group">
											<label for="question110" style="font-weight:normal">Please select one of the HR payroll system that you currently use?</label>
											<select class="form-control input-sm lfreq" required="required" id="custom" name="custom" placeholder="state">
												
                                   <option value="">Select</option>
                                   <option value="ADP">ADP</option>
								   <option value="Paychex">Paychex</option>
								   <option value="Zenefits">Zenefits</option>
                                   <option value="Ultimate Software">Ultimate Software</option>
                                   <option value="Paycom">Paycom</option>
                                   <option value="Ceridian">Ceridian</option>
                                   <option value="Paylocity">Paylocity</option>
                                   
                                                                        </select>
                                                                        </div>
                                                                        
                                                                        <div class="form-group" id="question110-group">
											<label for="question110" style="font-weight:normal">Would you be open to receive a call back from a representative of iSolved in the next 2-5 working days?</label>
											<select class="form-control input-sm lfreq" required="required" id="custom1" name="custom1" placeholder="state">
												
                                   <option value="">Select</option>
                                   <option value="Yes">Yes</option>
								   <option value="No">No</option>
								     </select>
                                                                        </div>
                                       
										<p class="buttons text-center">
                                        
                                        <input type="submit">
                                        
											<!--<a href="download-page.html">
												<img src="images/form-button-submit.jpg" width="135" height="36">
												</a>  -->      
							        </p>
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
                                </form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<footer class="footer">
				<div class="container">
					<p class="text-muted">© Copyright 2017, iSolved HCM, All rights reserved.</p>
				</div>
			</footer>

    <!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
        <script type="text/javascript">
            $("#isolved").validate();
        </script>
</body>
</html>
