<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    
   <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>Incubators Innovation  Is Not Enough You Must Execute</title>
    
    <!-- Styles -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<link href="http://martechb2b.com/email-template/HTML/31563-Microsoft-Office/css/styles.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
 
  <style type="text/css">
            input[type=submit] {
          background:url(http://martechb2b.com/email-template/HTML/31563-Microsoft-Office/images/watch-now-top.png);
          color: transparent;
          width: 275px;
          height: 52px;
        }
        .error{
            color: red;
            font-weight: normal;
        }
        </style> 

</head>
<body>

    
<!-- Fixed navbar -->
<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand">
				<img src="http://martechb2b.com/email-template/HTML/31563-Microsoft-Office/images/Microsoft_logo_landing-page.png" alt="" />
			</a>
		</div>
		<!-- the actual nav items -->
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav"></ul>
		</div>
		<!--/.nav-collapse -->
	</div>
</nav>
<!-- main content -->
<div class="container">
	<br />
	<div class="row">
		<!-- main column -->
		<div class="col-md-8">
			<div class="form-page-content">
				
					<h1>Incubators Innovation  Is Not Enough You Must Execute</h1>
                   
                    <p><img class="img-responsive" src="http://martechb2b.com/email-template/HTML/31563-Microsoft-Office/images/1500x300.jpg"></p>
                    <p> You may have a billion-dollar idea, but without the proper execution it won't be worth a cent. With incubators walking the fine line between risk and reward with their investments, creating an environment that encourages innovation with less financial risk may be key to your next big success. </p>
					<ul>
                    <li>Executive Director of Lowe's Innovation Labs, Kyle Nel, shares how innovation has propelled their business into creating new successes, and provides strategies for transforming ideas into action.</li>
                    <li>Innovation expert and Stanford University Professor, Dr. Hayagreeva "Huggy" Rao, explains how to apply the wisdom of incubators to help organizations of any size innovate with greater success.</li>
                    
                    </ul>
                   
					</div>
				</div>
				<!-- right column -->
				<div class="col-md-4">
					<div class="panel panel-primary reg-form">
						<div class="dnfrmc full" id="dnfrmmask">
                        <form class="autoform" id="micro_video" method="Post" name="micro_video">
                         <input type="hidden" name="form-type" value="micro-download">
							<div class="reg-form-panel">
								<div id="leadform-error"></div>
								<p class="intro">Please fill in the information below to access this resource:</p>
								<div class="row">
									<div class="col-sm-12">
										<div class="form-group" id="question1-group" style="padding-bottom:5px;">
											<label class="sr-only" for="question1">First Name</label>
											<input type="text" class="form-control input-sm lfreq tblfreq" id="f_name" name="f_name" maxlength="50"  placeholder="First Name" required />
										</div>
										<div class="form-group" id="question2-group" style="padding-bottom:5px;">
											<label class="sr-only" for="question2">Last Name</label>
											<input type="text" class="form-control input-sm lfreq tblfreq" id="l_name" name="l_name" maxlength="50"  placeholder="Last Name" required/>
										</div>
										<div class="form-group" id="question8-group" style="padding-bottom:5px;">
											<label class="sr-only" for="question8">Email Address</label>
											<input type="text" class="form-control input-sm lfreq tblfreq email" id="email" name="email" maxlength="100"  placeholder="Email Address" required/>
										</div>
										<div class="form-group" id="question9-group" style="padding-bottom:5px;">
											<label class="sr-only" for="question9">Telephone</label>
											<input type="text" class="form-control input-sm lfreq tblfreq tel" id="phone" name="phone" maxlength="50"  placeholder="Telephone" required/>
										</div>
										<div class="form-group" id="question3-group" style="padding-bottom:5px;">
											<label class="sr-only" for="question3">Company Name</label>
											<input type="text" class="form-control input-sm lfreq tblfreq" id="company" name="company" maxlength="80"  placeholder="Company Name" required/>
										</div>
										<div class="form-group" id="question110-group" style="padding-bottom:5px;">
											<label class="sr-only" for="question110">Company Size</label>
											<select class="form-control input-sm lfreq" required="required" id="company_size" name="company_size" placeholder="Company Size">
												<option value="">Select Company Size</option>
												<option value="1 to 3">1 to 3</option>
												<option value="4 to 10">4 to 10</option>
												<option value="11 to 25">11 to 25</option>
												<option value="26 to 50">26 to 50</option>
												<option value="51 to 100">51 to 100</option>
												<option value="101 to 200">101 to 200</option>
												<option value="201 to 500">201 to 500</option>
												<option value="501 to 1000">501 to 1000</option>
												<option value="1001 or more">1001 or more</option>
											</select>
										</div>
										<div class="form-group" id="question141-group" style="padding-bottom:5px;">
											<label class="sr-only" for="question141">Job Function</label>
											<select class="form-control input-sm lfreq" required="required" id="job_title_level" name="job_title_level" placeholder="Job Function">
												<option value="">Select Job Function</option>
												<option value="IT Management(Manager IS/IT)">IT Management(Manager IS/IT)</option>
												<option value="Senior Non IT Management(CEO,CFO,VP,Director)">Senior Non IT Management(CEO,CFO,VP,Director)</option>
												<option value="Senior IT Management(CIO/CTO/VP/Director)">Senior IT Management(CIO/CTO/VP/Director)</option>
												<option value="Network Management/Administration">Network Management/Administration</option>
												<option value="IT Staff">IT Staff</option>
												<option value="CSO">CSO</option>
												<option value="Product Management">Product Management</option>
												<option value="Sales/Marketing">Sales/Marketing</option>
												<option value="Telecommunications Manager">Telecommunications Manager</option>
												<option value="Systems Management/Administration">Systems Management/Administration</option>
												<option value="Project Manager">Project Manager</option>
												<option value="Programmer/Developer">Programmer/Developer</option>
												<option value="Privacy Officer">Privacy Officer</option>
												<option value="Consultant/Systems Integrator">Consultant/Systems Integrator</option>
												<option value="Database Administrator">Database Administrator</option>
												<option value="Educator">Educator</option>
												<option value="Non IT Staff">Non IT Staff</option>
												<option value="Architect">Architect</option>
												<option value="Application Manager">Application Manager</option>
												<option value="Analyst">Analyst</option>
											</select>
										</div>
										<div class="form-group" id="question113-group" style="padding-bottom:5px;">
											<label class="sr-only" for="question113">Industry</label>
											<select class="form-control input-sm lfreq" required="required" id="industry" name="industry" placeholder="Industry">
												<option value="">Select Industry</option>
												<option value="Advertising/Marketing">Advertising/Marketing</option>
												<option value="Automotive">Automotive</option>
												<option value="Agriculture/Forestry">Agriculture/Forestry</option>
												<option value="Business Services">Business Services</option>
												<option value="Consulting">Consulting</option>
												<option value="Distribution">Distribution</option>
												<option value="Entertainment">Entertainment</option>
												<option value="Education">Education</option>
												<option value="Engineering/Construction">Engineering/Construction</option>
												<option value="Financial">Financial</option>
												<option value="Food and Beverage">Food and Beverage</option>
												<option value="Government-Federal">Government-Federal</option>
												<option value="Government-State and Local">Government-State and Local</option>
												<option value="Healthcare/Medical">Healthcare/Medical</option>
												<option value="Hospitality">Hospitality</option>
												<option value="Insurance">Insurance</option>
												<option value="Legal">Legal</option>
												<option value="Manufacturing">Manufacturing</option>
												<option value="Non-profit">Non-profit</option>
												<option value="Real-Estate">Real-Estate</option>
												<option value="Retail">Retail</option>
												<option value="Student/Retired/Non-Business">Student/Retired/Non-Business</option>
												<option value="Transportation/Shipping">Transportation/Shipping</option>
												<option value="Technology">Technology</option>
												<option value="Telecommunications">Telecommunications</option>
												<option value="Utilities">Utilities</option>
												<option value="Warehousing">Warehousing</option>
												<option value="Other">Other</option>
											</select>
										</div>
										<div class="form-group" id="question7-group" >
											<label class="sr-only" for="question7">Zip Code</label>
											<input type="text" class="form-control input-sm lfreq tblfreq zip" id="zip" name="zip" maxlength="5"  placeholder="Zip Code" value="" required/>
										</div>
                                        <div class="form-group" id="question110-group">
                  <label for="question110" style="font-weight:normal">Does your company plan to purchase any cloud-based productivity suites in the next six months?</label>
                  <select class="form-control input-sm lfreq" required="required" id="custom1" name="custom1" placeholder="Company Size">
                    <option value="">Select </option>
                    <option value="Yes"> Yes </option>
                    <option value="No"> No </option>
                  </select>
                </div>
                
                <div class="form-group" id="question110-group">
                  <label for="question110" style="font-weight:normal">Which of the following topics are you interested in learning more about from a Microsoft expert?</label>
                  <select class="form-control input-sm lfreq" required="required" id="custom2" name="custom2" placeholder="Company Size">
                    <option value="">Select </option>
                    <option value="Cost savings vs. on-premise solutions"> Cost savings vs. on-premise solutions </option>
                    <option value="Increased productivity from Office 365"> Increased productivity from Office 365  </option>
                    <option value="Benefits of automatic backup and recovery"> Benefits of automatic backup and recovery </option>
                    <option value="Modern collaboration tools"> Modern collaboration tools </option>
                    <option value="Other"> Other </option>
                  </select>
                </div>
                                        
                                        <div class="form-group" id="question110-group">
                  <label for="question110" style="font-weight:normal">I would like to hear from Microsoft and its family of companies via email about products, services, and events, including the latest solutions, tips, and exclusive offers.</label>
                  <select class="form-control input-sm lfreq" required="required" id="custom3" name="custom3" placeholder="Company Size">
                    <option value="">Select </option>
                    <option value="Yes"> Yes </option>
                    <option value="No"> No </option>
                  </select>
                </div>
                                  </div>
									
							  </div>
								<p class="buttons text-center">
                                
                                <input type="submit">
									<!--<a href="download-page.html">
										<img src="images/watch-now-top.png" width="275" height="52">
										</a>-->
									</p>
									<p>To withdraw consent or manage your contact preferences, visit the Promotional Communications Manager. <a href="https://privacy.microsoft.com/en-us/privacystatement">Privacy and Cookies</a></p>
						    <div class="clearfix"></div>
								</div>
                                </form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<footer class="footer">
				<div class="container">
					<p class="text-muted">© Copyright 2016, Microsoft, All rights reserved.</p>
				</div>
			</footer>

    <!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
        <script type="text/javascript">
            $("#micro_video").validate();
        </script>

</body>
</html>
