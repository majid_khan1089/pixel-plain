<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"><head>
    
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>5 Ways GoToMeeting Can Help you Connect Better</title>
    
    <!-- Styles -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />
<link href="http://martechb2b.com/email-template/HTML/gotomeeting-five-way/css/styles.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
 
    <style type="text/css">
            input[type=submit] {
          background:url(http://martechb2b.com/email-template/HTML/gotomeeting-five-way/images/form-button-submit.jpg);
          color: transparent;
          width: 135px;
          height: 38px;
        }
        .error{
            color: red;
            font-weight: normal;
        }
        </style>

</head>
<body>

    
<!-- Fixed navbar -->
<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container">
		<div class="navbar-header">
			
			<a class="navbar-brand" >
				<img src="http://martechb2b.com/email-template/HTML/gotomeeting-five-way/images/go-to-meeting-logo-landing.png" alt="" />
			</a>
		</div>
		<!-- the actual nav items -->
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav"></ul>
		</div>
		<!--/.nav-collapse -->
	</div>
</nav>
<!-- main content -->
<div class="container">
  <br />
	<div class="row">
		<!-- main column -->
		<div class="col-sm-9">
                <div class="form-page-content">
                  <h1>5 Ways GoToMeeting Can Help you Connect Better</h1>
                  
                               
             <div class="asset-image"><img src="http://martechb2b.com/email-template/HTML/gotomeeting-five-way/images/5ways-front.png" /></div>
               <p> The following Infographic will show you 5 Ways GoToMeeting helps you connect better.</p>
               
               <p>Professional, robust, reliable. GoToMeeting makes business collaboration a better experience - no matter the distance. We support millions of meetings a year, helping businesses of all sizes look and sound their best.</p>

<br/>
                </div>
            </div>

				<!-- right column -->
				<div class="col-sm-3">
					<div class="panel panel-primary reg-form">
						<div class="dnfrmc full" id="dnfrmmask">
                        <form class="autoform" id="gotomeeting_five" method="Post" name="gotomeeting_five">
                         <input type="hidden" name="form-type" value="sap-seven-key-download">
							<div class="reg-form-panel">
								<div id="leadform-error"></div>
								<p class="intro text-center"><strong>Download the Free Article Now</strong></p>
								<div class="row">
									<div class="col-sm-12" style="padding-left: 10px; padding-right: 10px;">
										<div class="form-group" id="question1-group">
											<label class="sr-only" for="question1">First Name</label>
											<input type="text" class="form-control input-sm lfreq tblfreq" id="f_name" name="f_name" maxlength="50"  placeholder="First Name" required />
										</div>
										<div class="form-group" id="question2-group">
											<label class="sr-only" for="question2">Last Name</label>
											<input type="text" class="form-control input-sm lfreq tblfreq" id="l_name" name="l_name" maxlength="50"  placeholder="Last Name" required/>
										</div>
                                          <div class="form-group" id="question7-group">
											<label class="sr-only" for="question7">Job Title</label>
											<input type="text" class="form-control input-sm lfreq tblfreq zip" id="job_title" name="job_title" maxlength="50"  placeholder="Job Title" value="" required/>
										</div>
                                        
                                        <div class="form-group" id="question3-group">
											<label class="sr-only" for="question3">Company Name</label>
											<input type="text" class="form-control input-sm lfreq tblfreq" id="company" name="company" maxlength="80"  placeholder="Company Name" required/>
										</div>
                                        
										<div class="form-group" id="question8-group">
											<label class="sr-only" for="question8">Email Address</label>
											<input type="text" class="form-control input-sm lfreq tblfreq email" id="email" name="email" maxlength="100"  placeholder="Email Address" required/>
										</div>
										<div class="form-group" id="question9-group">
											<label class="sr-only" for="question9">Phone</label>
											<input type="text" class="form-control input-sm lfreq tblfreq tel" id="phone" name="phone" maxlength="50"  placeholder="Telephone" required/>
										</div>
                                        
                                        <div class="form-group" id="question110-group">
											<label class="sr-only" for="question110">Company Size</label>
											<select class="form-control input-sm lfreq" required="required" id="company_size" name="company_size" placeholder="Company Size">
												
												<option value="">Select Company Size</option>
                                               <option value="1-49">1-49</option>
											<option value="50-200">50-200</option>
											<option value="201-500">201-500</option>
											<option value="251-500">251-500</option>
											<option value="501-1000">501-1000</option>
											<option value="1001-5000">1001-5000</option>
											<option value="5000+">5000+</option>
											</select>
										</div>
										
                                        <div class="form-group" id="question7-group">
											<label class="sr-only" for="question7">State</label>
											<input type="text" class="form-control input-sm lfreq tblfreq zip" id="state" name="state" maxlength="25"  placeholder="State" value="" required/>
										</div>
                                        
                                        <div class="form-group" id="question7-group">
											<label class="sr-only" for="question7">Zip Code</label>
											<input type="text" class="form-control input-sm lfreq tblfreq zip" id="zip" name="zip" maxlength="5"  placeholder="Zip Code" value="" required/>
										</div>
                                        
                                        <div class="form-group" id="question110-group">
											<label for="question110" style="font-weight:normal">What is your role in selecting an online webinar platform software?</label>
											<select class="form-control input-sm lfreq" required="required" id="custom" name="custom" placeholder="Company Size">
												
												<option value="">Select </option>
                                               <option value="Decision Maker">Decision Maker</option>
											<option value="Influencer">Influencer</option>
											<option value="No Role">No Role</option>
											</select>
										</div>
                                      
										<p class="buttons text-center">
                                        
                                        <input type="submit">
                                        
											<!--<a href="download-page.html">
												<img src="images/form-button-submit.jpg" width="135" height="36">
												</a>  -->      
							        </p>
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
                                </form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<footer class="footer">
				<div class="container">
					<p class="text-muted">© Copyright 2017, GoToMeeting, All rights reserved.</p>
				</div>
			</footer>

    <!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
        <script type="text/javascript">
            $("#gotomeeting_five").validate();
        </script>
</body>
</html>
