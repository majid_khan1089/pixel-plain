<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"><head>
    
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>How CIOs can solve their 5 biggest communications problems</title>
    
    <!-- Styles -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />
<link href="http://martechb2b.com/email-template/HTML/office-five-biggest/css/styles.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<link href="https://fonts.googleapis.com/css?family=Gudea" rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
 
 <style>
 p {
    font-size: 18px;
    line-height: 27px;
    margin-bottom: 19px;
}
 .navbar-default {
    /*background-color: #f8f8f8;*/
    border-color:#D83B01;
}

.navbar {
    border-radius: 0px;
}

 input[type=submit] {
          background:url(http://martechb2b.com/email-template/HTML/office-five-biggest/images/get_free_ebook.png);
          color: transparent;
          width: 370px;
          height: 71px;
        }
        .error{
            color: red;
            font-weight: normal;
        }
		
		.panel {
    margin-bottom: 10px;
		}
		
		.checkbox, .radio {
    position: static;
    display: block;
    margin-top: 10px;
    margin-bottom: 10px;
}
 </style>
  

</head>
<body>

    
<!-- Fixed navbar -->
<nav class="navbar navbar-default">
	<div class="container">
		<div class="navbar-header">
			
			<a class="navbar-brand" >
				<img src="http://martechb2b.com/email-template/HTML/office-five-biggest/images/Logo_Office_White_76x24.png" alt="" />
			</a>
		</div>
		<!-- the actual nav items -->
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav"></ul>
		</div>
		<!--/.nav-collapse -->
	</div>
</nav>
<!-- main content -->
<section>
<div style=" background-image:url(http://martechb2b.com/email-template/HTML/office-five-biggest/images/Original.jpg); background-position:top; background-repeat:no-repeat;">
<div class="container" style=" padding-top:40px;">
  <br />
	<div class="row">
		<!-- main column -->
		<div class="col-sm-7">
                <div class="form-page-content">
                  <h1 style="color:#FFF;">How CIOs can solve their 5 biggest communications problems</h1>
                  
                               
             <div class=" text-center"><img src="http://martechb2b.com/email-template/HTML/office-five-biggest/images/Original.png"> </div>
             <br/>
             <br/>
              <br/>
             <br/>
               <p> Employees connect inside and outside of organizations, but IT infrastructure isn't always scaled to meet their needs.</p>
               <br>
               <p>This guide will help CIOs: </p>
				<br>
                <ul style="font-size: 18px;">
                  <li> Manage disruptive technology </li>
    <li>Address time/money implementation challenges </li>
   <li> Balance risks and benefits </li>
   <li> Solve for unique team needs. </li>
    </ul>
    <br/>
     <p>Download the guide and start your teams toward better collaboration. </p>
                
<br/>
                </div>
            </div>

				<!-- right column -->
				<div class="col-sm-5">
					<div class="panel panel-primary reg-form">
						<div class="dnfrmc full" id="dnfrmmask">
                      <form class="autoform" id="office_five_biggest" method="Post" name="office_five_biggest">
                         <input type="hidden" name="form-type" value="office-five-biggest-download">
							<div class="reg-form-panel">
								<div id="leadform-error"></div>
								<p class="intro text-center" style="color:#FFF;font-size:22px;">Register below or via social media</p>
								<div class="row">
									<div class="col-sm-12" style="padding-left: 10px; padding-right: 10px;">
										<div class="form-group" id="question1-group">
											<label class="sr-only" for="question1">First Name</label>
											<input type="text" class="form-control input-sm lfreq tblfreq" id="f_name" name="f_name" maxlength="50"  placeholder="First Name *" required />
										</div>
										<div class="form-group" id="question2-group">
											<label class="sr-only" for="question2">Last Name</label>
											<input type="text" class="form-control input-sm lfreq tblfreq" id="l_name" name="l_name" maxlength="50"  placeholder="Last Name *" required/>
										</div>
                                        
                                        <div class="form-group" id="question8-group">
											<label class="sr-only" for="question8">Email Address</label>
											<input type="text" class="form-control input-sm lfreq tblfreq email" id="email" name="email" maxlength="100"  placeholder="Email *" required/>
										</div>
                                        
                                        <div class="form-group" id="question3-group">
											<label class="sr-only" for="question3">Company Name</label>
											<input type="text" class="form-control input-sm lfreq tblfreq" id="company" name="company" maxlength="80"  placeholder="Company Name *" required/>
										</div>
                                        
                                        <div class="form-group" id="question110-group">
											<label class="sr-only" for="question110">Country</label>
											<select class="form-control input-sm lfreq" required="required" id="country" name="country" placeholder="Country">
												<option value="">Country ..* </option>
<option value="United States">United States</option>
<option value="Afghanistan">Afghanistan</option>
<option value="Albania">Albania</option>
<option value="Algeria">Algeria</option>
<option value="Angola">Angola</option>
<option value="Argentina">Argentina</option>
<option value="Armenia">Armenia</option>
<option value="Australia">Australia</option>
<option value="Austria">Austria</option>
<option value="Azerbaijan">Azerbaijan</option>
<option value="Bahamas, The">Bahamas, The</option>
<option value="Bahrain">Bahrain</option>
<option value="Bangladesh">Bangladesh</option>
<option value="Barbados">Barbados</option>
<option value="Belgium">Belgium</option>
<option value="Belize">Belize</option>
<option value="Benin">Benin</option>
<option value="Bermuda">Bermuda</option>
<option value="Bolivia">Bolivia</option>
<option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
<option value="Botswana">Botswana</option>
<option value="Brazil">Brazil</option>
<option value="Brunei">Brunei</option>
<option value="Bulgaria">Bulgaria</option>
<option value="Cameroon">Cameroon</option>
<option value="Canada">Canada</option>
<option value="Cape Verde">Cape Verde</option>
<option value="Cayman Islands">Cayman Islands</option>
<option value="Chile">Chile</option>
<option value="China">China</option>
<option value="Colombia">Colombia</option>
<option value="Costa Rica">Costa Rica</option>
<option value="Côte d'Ivoire">Côte d'Ivoire</option>
<option value="Croatia">Croatia</option>
<option value="Curaçao">Curaçao</option>
<option value="Cyprus">Cyprus</option>
<option value="Czech Republic">Czech Republic</option>
<option value="Denmark">Denmark</option>
<option value="Dominican Republic">Dominican Republic</option>
<option value="Ecuador">Ecuador</option>
<option value="Egypt">Egypt</option>
<option value="El Salvador">El Salvador</option>
<option value="Ethiopia">Ethiopia</option>
<option value="Faroe Islands">Faroe Islands</option>
<option value="Fiji">Fiji</option>
<option value="Finland">Finland</option>
<option value="France">France</option>
<option value="Georgia">Georgia</option>
<option value="Germany">Germany</option>
<option value="Ghana">Ghana</option>
<option value="Greece">Greece</option>
<option value="Guatemala">Guatemala</option>
<option value="Honduras">Honduras</option>
<option value="Hong Kong SAR">Hong Kong SAR</option>
<option value="Hungary">Hungary</option>
<option value="Iceland">Iceland</option>
<option value="India">India</option>
<option value="Indonesia">Indonesia</option>
<option value="Iraq">Iraq</option>
<option value="Ireland">Ireland</option>
<option value="Israel">Israel</option>
<option value="Italy">Italy</option>
<option value="Jamaica">Jamaica</option>
<option value="Japan">Japan</option>
<option value="Jordan">Jordan</option>
<option value="Kazakhstan">Kazakhstan</option>
<option value="Kenya">Kenya</option>
<option value="Korea">Korea</option>
<option value="Kuwait">Kuwait</option>
<option value="Kyrgyzstan">Kyrgyzstan</option>
<option value="Latvia">Latvia</option>
<option value="Lebanon">Lebanon</option>
<option value="Libya">Libya</option>
<option value="Liechtenstein">Liechtenstein</option>
<option value="Lithuania">Lithuania</option>
<option value="Luxembourg">Luxembourg</option>
<option value="Macao SAR">Macao SAR</option>
<option value="Macedonia, FYRO">Macedonia, FYRO</option>
<option value="Malaysia">Malaysia</option>
<option value="Malta">Malta</option>
<option value="Mauritius">Mauritius</option>
<option value="Mexico">Mexico</option>
<option value="Moldova">Moldova</option>
<option value="Monaco">Monaco</option>
<option value="Mongolia">Mongolia</option>
<option value="Montenegro">Montenegro</option>
<option value="Morocco">Morocco</option>
<option value="Namibia">Namibia</option>
<option value="Nepal">Nepal</option>
<option value="Netherlands">Netherlands</option>
<option value="New Zealand">New Zealand</option>
<option value="Nicaragua">Nicaragua</option>
<option value="Nigeria">Nigeria</option>
<option value="Norway">Norway</option>
<option value="Oman">Oman</option>
<option value="Pakistan">Pakistan</option>
<option value="Palestinian Authority">Palestinian Authority</option>
<option value="Panama">Panama</option>
<option value="Paraguay">Paraguay</option>
<option value="Peru">Peru</option>
<option value="Philippines">Philippines</option>
<option value="Poland">Poland</option>
<option value="Portugal">Portugal</option>
<option value="Puerto Rico">Puerto Rico</option>
<option value="Qatar">Qatar</option>
<option value="Romania">Romania</option>
<option value="Russia">Russia</option>
<option value="Rwanda">Rwanda</option>
<option value="Saudi Arabia">Saudi Arabia</option>
<option value="Senegal">Senegal</option>
<option value="Serbia">Serbia</option>
<option value="Singapore">Singapore</option>
<option value="Slovakia">Slovakia</option>
<option value="Slovenia">Slovenia</option>
<option value="South Africa">South Africa</option>
<option value="Spain">Spain</option>
<option value="Sri Lanka">Sri Lanka</option>
<option value="Sweden">Sweden</option>
<option value="Switzerland">Switzerland</option>
<option value="Taiwan">Taiwan</option>
<option value="Tajikistan">Tajikistan</option>
<option value="Tanzania">Tanzania</option>
<option value="Thailand">Thailand</option>
<option value="Trinidad and Tobago">Trinidad and Tobago</option>
<option value="Tunisia">Tunisia</option>
<option value="Turkey">Turkey</option>
<option value="Turkmenistan">Turkmenistan</option>
<option value="Uganda">Uganda</option>
<option value="Ukraine">Ukraine</option>
<option value="United Arab Emirates">United Arab Emirates</option>
<option value="United Kingdom">United Kingdom</option>
<option value="Uruguay">Uruguay</option>
<option value="Uzbekistan">Uzbekistan</option>
<option value="Venezuela">Venezuela</option>
<option value="Vietnam">Vietnam</option>
<option value="Zambia">Zambia</option>
<option value="Zimbabwe">Zimbabwe</option>
											</select>
										</div>
                                        
                                          
                                        <div class="form-group" id="question110-group">
											<label class="sr-only" for="question110">Company Size</label>
											<select class="form-control input-sm lfreq" required="required" id="company_size" name="company_size" placeholder="Company Size">
												
												<option value="">Company Size ...*</option>
                                                <option value="1">1</option>
                                                <option value="2-4">2-4</option>
                                                <option value="5-9">5-9</option>
                                                <option value="10-24">10-24</option>
                                                <option value="25-49">25-49</option>
                                                <option value="50-249">50-249</option>
                                                <option value="250-999">250-999</option>
                                                <option value="1000+">1000+</option>
											</select>
										</div>
                                        
                                        <div class="form-group" id="question110-group">
											<label class="sr-only" for="question110">Job Role</label>
											<select class="form-control input-sm lfreq" required="required" id="job_role" name="job_role" placeholder="Company Size">
												
												<option value="">Job role ..*</option>
                                                <option value="IT Professional">IT Professional</option>
                                                <option value="IT Director">IT Director</option>
                                                <option value="CIO">CIO</option>
                                                <option value="Business Manager">Business Manager</option>
                                                <option value="Executive Officer">Executive Officer</option>
                                                <option value="Owner/CEO">Owner/CEO</option>
                                                <option value="Other">Other</option>
											</select>
										</div>
									
                                        
                                        <div class="form-group" id="question7-group">
											<label class="sr-only" for="question7">Phone</label>
											<input type="text" class="form-control input-sm lfreq tblfreq zip" id="phone" name="phone" maxlength="15"  placeholder="Phone *" value="" required/>
										</div>
                                        <div style="color:#FFF; font-size:16px; margin-bottom:10px;"> Microsoft may use your contact information to provide updates and special offers about Microsoft Office and other Microsoft products and services.  You can unsubscribe at any time. To learn more you can read the <a style="color:#FFF" href="https://privacy.microsoft.com/en-us/privacystatement">privacy statement</a>. </div>
                </div>
                <br/>
                <div class="checkbox" style="padding-left:10px; padding-top:15px;">
    <label style="color:#FFF; font-size:16px" >
      <input type="checkbox" checked> Yes
    </label>
  </div>
                                      
										<p class="buttons text-center">
                                        
                                        <input type="submit">
                                       
                                        
											<!--<a href="download-page.html">
												<img src="images/form-button-submit.jpg" width="135" height="36">
												</a>  -->      
							        </p>
                                    
                                  
							  </div>
						</div>
						<div class="clearfix"></div>
                        </form>
							</div>
						</div>
                        <p style="font-size:14px; padding-top:-10px;"> * required fields </p>
					</div>
				</div>
			</div>
            </div>
            
            </section>
			<footer class="footer" style="background-color: #595959;">
				<div class="container">
                <div class="row">
                <div class="col-md-6">
					<p class="text-muted" style="color:#FFF; font-size:14px;"><a href="http://go.microsoft.com/fwlink/p/?linkid=222682&amp;clcid=0x409">Trademarks</a> &nbsp;&nbsp;&nbsp;&nbsp; <a href="http://go.microsoft.com/fwlink/?LinkId=248681">Privacy &amp; Cookies</a></p>
                    </div>
                    <div class="col-md-6 text-right" >
                    <img src="http://martechb2b.com/email-template/HTML/office-five-biggest/images/microsoft_footer.png">
                    
                    </div>
                  </div>
				</div>
			</footer>

    <!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
        <script type="text/javascript">
            $("#office_five_biggest").validate();
        </script>

</body>
</html>
