<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>Performance And Flexibility Serve The Enterprise</title>
    
    <!-- Styles -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<link href="http://www.mqltosql.com/email-template/HTML/27156-IBM-Sys-Hardware-Power-Linux/css/styles.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>

<style type="text/css">
            input[type=submit] {
          background:url(http://www.mqltosql.com/email-template/HTML/27156-IBM-Sys-Hardware-Power-Linux/images/form-button-submit.jpg);
          color: transparent;
          width: 135px;
          height: 38px;
        }
        .error{
            color: red;
            font-weight: normal;
        }
        </style>
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
 
  

</head>
<body>

    
<!-- Fixed navbar -->
<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand">
				<img src="http://www.mqltosql.com/email-template/HTML/27156-IBM-Sys-Hardware-Power-Linux/images/image1.png" alt="Forrester" />
			</a>
		</div>
		<!-- the actual nav items -->
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav"></ul>
		</div>
		<!--/.nav-collapse -->
	</div>
</nav>
<!-- main content -->
<div class="container">
	<br />
	<div class="row">
		<!-- main column -->
		<div class="col-md-5">
        <h1> Performance And Flexibility Serve The Enterprise </h1>
					<h3>Research Reveals Path To Maximize Linux Performance</h3>
        <div class="row">
        	<div class="col-sm-8">
			<div class="form-page-content">
				<input id="categoryid" name="categoryid" value="491" type="hidden">
					
					<p>Forrester Consulting conducted a survey to reveal what types of challenges enterprise hardware and operating systems are facing with the increasingly demanding workloads from applications and data. </p>
                    <p>In conducting a quantitative survey with 150 North American application developers, IT,  architects and strategists,  Forrester found that while these companies are very cautious about making a major ecosystem change , they recognize that increasing demands for performance, stability, and reliability mean they must plan and articulate a clear path to a more robust, performance - oriented architecture. </p>
					
					
					<br>
					</div>
          </div>
                    <div class="col-sm-4">
                    <img class="img-responsive" src="http://www.mqltosql.com/email-template/HTML/27156-IBM-Sys-Hardware-Power-Linux/images/pic1.png">
                    </div>
          </div>
				</div>
				<!-- right column -->
				<div class="col-md-7">
					<div class="panel panel-primary reg-form">
						<div class="dnfrmc full" id="dnfrmmask">
                        <form class="autoform" id="symantech" method="Post" name="symantech">
                               
							<div class="reg-form-panel">
								<div id="leadform-error"></div>
								<p class="intro">Please fill in the information below to access this resource:</p>
								<div class="row">
									<div class="col-sm-4">
										<div class="form-group" id="question1-group">
											<label class="sr-only" for="question1">First Name</label>
											<input type="text" class="form-control input-sm lfreq tblfreq" id="f_name" name="f_name" maxlength="50"  placeholder="First Name" required />
										</div>
										<div class="form-group" id="question2-group">
											<label class="sr-only" for="question2">Last Name</label>
											<input type="text" class="form-control input-sm lfreq tblfreq" id="l_name" name="l_name" maxlength="50"  placeholder="Last Name" required/>
										</div>
										<div class="form-group" id="question8-group">
											<label class="sr-only" for="question8">Email Address</label>
											<input type="text" class="form-control input-sm lfreq tblfreq email" id="email" name="email" maxlength="100"  placeholder="Email Address" required/>
										</div>
										<div class="form-group" id="question9-group">
											<label class="sr-only" for="question9">Telephone</label>
											<input type="text" class="form-control input-sm lfreq tblfreq tel" id="phone" name="phone" maxlength="50"  placeholder="Telephone" required/>
										</div>
										<div class="form-group" id="question3-group">
											<label class="sr-only" for="question3">Company Name</label>
											<input type="text" class="form-control input-sm lfreq tblfreq" id="company" name="company" maxlength="80"  placeholder="Company Name" required/>
										</div>
										<div class="form-group" id="question110-group">
											<label class="sr-only" for="question110">Company Size</label>
											<select class="form-control input-sm lfreq " id="company_size" name="company_size" placeholder="Company Size">
												<option value="">Select Company Size</option>
												<option value="1 to 3">1 to 3</option>
												<option value="4 to 10">4 to 10</option>
												<option value="11 to 25">11 to 25</option>
												<option value="26 to 50">26 to 50</option>
												<option value="51 to 100">51 to 100</option>
												<option value="101 to 200">101 to 200</option>
												<option value="201 to 500">201 to 500</option>
												<option value="501 to 1000">501 to 1000</option>
												<option value="1001 or more">1001 or more</option>
											</select>
										</div>
										<div class="form-group" id="question141-group">
											<label class="sr-only" for="question141">Job Function</label>
											<select class="form-control input-sm lfreq " id="job_title_level" name="job_title_level" placeholder="Job Function">
												<option value="">Select Job Function</option>
												<option value="IT Management(Manager IS/IT)">IT Management(Manager IS/IT)</option>
												<option value="Senior Non IT Management(CEO,CFO,VP,Director)">Senior Non IT Management(CEO,CFO,VP,Director)</option>
												<option value="Senior IT Management(CIO/CTO/VP/Director)">Senior IT Management(CIO/CTO/VP/Director)</option>
												<option value="Network Management/Administration">Network Management/Administration</option>
												<option value="IT Staff">IT Staff</option>
												<option value="CSO">CSO</option>
												<option value="Product Management">Product Management</option>
												<option value="Sales/Marketing">Sales/Marketing</option>
												<option value="Telecommunications Manager">Telecommunications Manager</option>
												<option value="Systems Management/Administration">Systems Management/Administration</option>
												<option value="Project Manager">Project Manager</option>
												<option value="Programmer/Developer">Programmer/Developer</option>
												<option value="Privacy Officer">Privacy Officer</option>
												<option value="Consultant/Systems Integrator">Consultant/Systems Integrator</option>
												<option value="Database Administrator">Database Administrator</option>
												<option value="Educator">Educator</option>
												<option value="Non IT Staff">Non IT Staff</option>
												<option value="Architect">Architect</option>
												<option value="Application Manager">Application Manager</option>
												<option value="Analyst">Analyst</option>
											</select>
										</div>
										<div class="form-group" id="question113-group">
											<label class="sr-only" for="question113">Industry</label>
											<select class="form-control input-sm lfreq " id="industry" name="industry" placeholder="Industry">
												<option value="">Select Industry</option>
												<option value="Advertising/Marketing">Advertising/Marketing</option>
												<option value="Automotive">Automotive</option>
												<option value="Agriculture/Forestry">Agriculture/Forestry</option>
												<option value="Business Services">Business Services</option>
												<option value="Consulting">Consulting</option>
												<option value="Distribution">Distribution</option>
												<option value="Entertainment">Entertainment</option>
												<option value="Education">Education</option>
												<option value="Engineering/Construction">Engineering/Construction</option>
												<option value="Financial">Financial</option>
												<option value="Food and Beverage">Food and Beverage</option>
												<option value="Government-Federal">Government-Federal</option>
												<option value="Government-State and Local">Government-State and Local</option>
												<option value="Healthcare/Medical">Healthcare/Medical</option>
												<option value="Hospitality">Hospitality</option>
												<option value="Insurance">Insurance</option>
												<option value="Legal">Legal</option>
												<option value="Manufacturing">Manufacturing</option>
												<option value="Non-profit">Non-profit</option>
												<option value="Real-Estate">Real-Estate</option>
												<option value="Retail">Retail</option>
												<option value="Student/Retired/Non-Business">Student/Retired/Non-Business</option>
												<option value="Transportation/Shipping">Transportation/Shipping</option>
												<option value="Technology">Technology</option>
												<option value="Telecommunications">Telecommunications</option>
												<option value="Utilities">Utilities</option>
												<option value="Warehousing">Warehousing</option>
												<option value="Other">Other</option>
											</select>
										</div>
										<div class="form-group" id="question7-group">
											<label class="sr-only" for="question7">Zip Code</label>
											<input type="text" class="form-control input-sm lfreq tblfreq zip" id="zip" name="zip" maxlength="5"  placeholder="Zip Code" value=""/>
										</div>
									</div>
									<div class="col-sm-8">
										<div class="form-group" id="question110-group" style="padding-bottom:5px;">
											<label for="question110" style="font-weight:normal"> A compelling open infrastructure requires a solid solutions ecosystem. Open Source is a key foundation on which the new Linux on Power Systems solutions are being built. Do you feel that your enterprise has a solid solution ecosystem is place? </label>
											<select class="form-control input-sm lfreq " id="custom_question1" name="custom_question1" placeholder="Company Size">
												<option value="">Select </option>
												<option value="No, we could use some help">No, we could use some help </option>
												<option value="How would I know if we do or not">How would I know if we do or not ? </option>
											</select>
										</div>
										<div class="form-group" id="question110-group" style="padding-bottom:5px;">
											<label for="question110" style="font-weight:normal"> Linux on Power Systems is the only Linux infrastructure that offers both scale-out and scale-up choices that align to your needs and meet the pressing demands of the wait less world. Is your current infrastructure equipped to meet these needs and demands? </label>
											<select class="form-control input-sm lfreq " id="custom_question2" name="custom_question2" placeholder="Company Size">
												<option value="">Select </option>
												<option value="No, our infrastructure is not equipped to meet these needs and demands"> No, our infrastructure is not equipped to meet these needs and demands </option>
												<option value=" I don't think we are, where can we get help?">  I don't think we are, where can we get help?  </option>
											</select>
										</div>
										<div class="form-group" id="question110-group" style="padding-bottom:5px;">
											<label for="question110" style="font-weight:normal"> Would it be advantageous for your enterprise to consolidate workloads from hundreds of servers to a few servers?</label>
											<select class="form-control input-sm lfreq " id="custom_question3" name="custom_question3" placeholder="Company Size">
												<option value="">Select </option>
												<option value="Yes, where do I sign up">  Yes, where do I sign up? </option>
												<option value="I think it could, can you give me more information"> I think it could, can you give me more information?</option>
											</select>
										</div>
                                        <div class="checkbox">
  <label>
    <input type="checkbox" value="">
    IBM companies worldwide would keep you informed of their products, services and offerings via Email. Okay.
  </label>
</div>
									</div>
								</div>
								<p class="buttons text-center">
                                <input type="submit">
									<!--<a href="images/performance-and-flexibility-serve-the-enterprise.pdf">
										<img src="images/form-button-submit.jpg" width="135" height="36">
										</a>-->
									</p>
									
						    <div class="clearfix"></div>
								</div>
                                </form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<footer class="footer">
				<div class="container">
					<p class="text-muted">© Copyright 2017, Business Tech Alert, All rights reserved.</p>
				</div>
			</footer>

    <!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
        <script type="text/javascript">
            $("#symantech").validate({
            /*rules: {
                f_name: "required",                
            },
            messages: {
                f_name: "Please enter your firstname"               
            }*/
        });
        </script>

</body>
</html>
