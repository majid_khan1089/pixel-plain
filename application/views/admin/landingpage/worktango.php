<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"><head>
    
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>A 2017 Guide to Measuring and Acting on Employee Engagement</title>
    
    <!-- Styles -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />
<link href="http://martechb2b.com/email-template/HTML/worktango/css/styles.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
 
 <style type="text/css">
            input[type=submit] {
          background:url(http://martechb2b.com/email-template/HTML/worktango/images/form-button-submit.jpg);
          color: transparent;
          width: 135px;
          height: 38px;
        }
        .error{
            color: red;
            font-weight: normal;
        }
        </style>
  

</head>
<body>

    
<!-- Fixed navbar -->
<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="/">
				<img src="http://martechb2b.com/email-template/HTML/worktango/images/League-eBook-landing.png" alt="" />
			</a>
		</div>
		<!-- the actual nav items -->
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav"></ul>
		</div>
		<!--/.nav-collapse -->
	</div>
</nav>
<!-- main content -->
<div class="container">
	<br />
	<div class="row">
		<!-- main column -->
		<div class="col-sm-9">
                <div class="form-page-content">
                  <h1>A 2017 Guide to Measuring and Acting on Employee Engagement </h1>
               
             <div class="asset-image"><img src="http://martechb2b.com/email-template/HTML/worktango/images/w_leak01c8.jpg" /></div>
                <p>	Innovation starts with your people, and their expectations are changing. In today's workplace, the employee engagement experience has taken on a new meaning.</p>
                <p>You will learn : </p>
                
                <ul>
                <li>Why employee engagement matters and how to sell it in your company.</li>
                <li>What factors drive employee engagement.</li>
                <li>How to give employees a voice, and approaches to collecting feedback.</li>
                <li>What should you measure, and how often. </li>
                <li>How to act – because your job doesn't end with measuring engagement.</li>
                
                </ul>
                
                
<p>	This comprehensive guide will tell you everything you need to know about measuring and acting on employee engagement in today's modern workplace. </p>

<br />
                </div>
            </div>

				<!-- right column -->
				<div class="col-sm-3">
					<div class="panel panel-primary reg-form">
						<div class="dnfrmc full" id="dnfrmmask">
                        <form class="autoform" id="worktango" method="Post" name="worktango">
                         <input type="hidden" name="form-type" value="worktango-download">
                        
							<div class="reg-form-panel">
								<div id="leadform-error"></div>
								<p class="intro">Please fill in the information below to access this resource:</p>
								<div class="row">
									<div class="col-sm-12" style="padding-left: 10px; padding-right: 10px;">
										<div class="form-group" id="question1-group">
											<label class="sr-only" for="question1">First Name</label>
											<input type="text" class="form-control input-sm lfreq tblfreq" id="f_name" name="f_name" maxlength="50"  placeholder="First Name" required />
										</div>
										<div class="form-group" id="question2-group">
											<label class="sr-only" for="question2">Last Name</label>
											<input type="text" class="form-control input-sm lfreq tblfreq" id="l_name" name="l_name" maxlength="50"  placeholder="Last Name" required/>
										</div>
										<div class="form-group" id="question8-group">
											<label class="sr-only" for="question8">Email Address</label>
											<input type="text" class="form-control input-sm lfreq tblfreq email" id="email" name="email" maxlength="100"  placeholder="Email Address" required/>
										</div>
										<div class="form-group" id="question9-group">
											<label class="sr-only" for="question9">Telephone</label>
											<input type="text" class="form-control input-sm lfreq tblfreq tel" id="phone" name="phone" maxlength="50"  placeholder="Telephone" required/>
										</div>
										<div class="form-group" id="question3-group">
											<label class="sr-only" for="question3">Company Name</label>
											<input type="text" class="form-control input-sm lfreq tblfreq" id="company" name="company" maxlength="80"  placeholder="Company Name" required/>
										</div>
										<div class="form-group" id="question110-group">
											<label class="sr-only" for="question110">Company Size</label>
											<select class="form-control input-sm lfreq" required="required" id="company_size" name="company_size" placeholder="Company Size">
												<option value="">Select Company Size</option>
												<option value="11 to 25">11 to 50</option>
												<option value="51 to 100">51 to 100</option>
												<option value="101 to 200">101 to 250</option>
												<option value="201 to 500">250+</option>
											</select>
										</div>
										<div class="form-group" id="question141-group">
											<label class="sr-only" for="question141">Job Title</label>
											<select class="form-control input-sm lfreq" required="required" id="job_title_level" name="job_title_level" placeholder="Job Function">
												<option value="">Select Job Title</option>
												<option value="IT Management(Manager IS/IT)">IT Management(Manager IS/IT)</option>
												<option value="Senior Non IT Management(CEO,CFO,VP,Director)">Senior Non IT Management(CEO,CFO,VP,Director)</option>
												<option value="Senior IT Management(CIO/CTO/VP/Director)">Senior IT Management(CIO/CTO/VP/Director)</option>
									
												<option value="Sales/Marketing">Sales/Marketing</option>
												<option value="Telecommunications Manager">Telecommunications Manager</option>
												<option value="HR Director">HR Director</option>
												<option value="VP Director">VP Director</option>
												<option value="Head HR">Head HR</option>
												<option value="CHRO">CHRO</option>
												<option value="Director Employment Engagement">Director Employment Engagement</option>
												<option value="Director Talent Accqusition/VP">Director Talent Accqusition/VP</option>
												<option value="Director Recruitment">Director Recruitment</option>
												<option value="Staffing">Staffing</option>
                                                <option value="HR Business Partner">HR Business Partner</option>
                                                <option value="Employee Relations Advisor">Employee Relations Advisor</option>
                                                <option value="HR Manager">HR Manager</option>
                                                <option value="Employment Manager">Employment Manager</option>
                                                <option value="Contract Recruiter">Contract Recruiter</option>
												
											</select>
										</div>
										<div class="form-group" id="question113-group">
											<label class="sr-only" for="question113">Industry</label>
											<select class="form-control input-sm lfreq" required="required" id="industry" name="industry" placeholder="Industry">
												<option value="">Select Industry</option>
												<option value="Advertising/Marketing">Advertising/Marketing</option>
												<option value="Automotive">Automotive</option>
												<option value="Agriculture/Forestry">Agriculture/Forestry</option>
												<option value="Business Services">Business Services</option>
												<option value="Consulting">Consulting</option>
												<option value="Distribution">Distribution</option>
												<option value="Entertainment">Entertainment</option>
												<option value="Education">Education</option>
												<option value="Engineering/Construction">Engineering/Construction</option>
												<option value="Financial">Financial</option>
												<option value="Food and Beverage">Food and Beverage</option>
												<option value="Government-Federal">Government-Federal</option>
												<option value="Government-State and Local">Government-State and Local</option>
												<option value="Healthcare/Medical">Healthcare/Medical</option>
												<option value="Hospitality">Hospitality</option>
												<option value="Insurance">Insurance</option>
												<option value="Legal">Legal</option>
												<option value="Manufacturing">Manufacturing</option>
												<option value="Non-profit">Non-profit</option>
												<option value="Real-Estate">Real-Estate</option>
												<option value="Retail">Retail</option>
												<option value="Student/Retired/Non-Business">Student/Retired/Non-Business</option>
												<option value="Transportation/Shipping">Transportation/Shipping</option>
												<option value="Technology">Technology</option>
												<option value="Telecommunications">Telecommunications</option>
												<option value="Utilities">Utilities</option>
												<option value="Warehousing">Warehousing</option>
												<option value="Other">Other</option>
											</select>
										</div>
										<div class="form-group" id="question7-group">
											<label class="sr-only" for="question7">Zip Code</label>
											<input type="text" class="form-control input-sm lfreq tblfreq zip" id="zip" name="zip" maxlength="5"  placeholder="Zip Code" value="" required/>
										</div>
                                       
                                       
										<div class="form-group" id="question1-group">
											<label class="sr-only" for="question1">City</label>
											<input type="text" class="form-control input-sm lfreq tblfreq" id="city" name="city" maxlength="50"  placeholder="City" required />
										</div>
                                     
                                        
										<div class="form-group" id="question110-group">
											<label class="sr-only" for="question110">state</label>
											<select class="form-control input-sm lfreq" required="required" id="state" name="state" placeholder="state">
												<option value="">State</option>
												<option value="AL">Alabama</option>
												<option value="AK">Alaska</option>
												<option value="AZ">Arizona</option>
												<option value="AR">Arkansas</option>
												<option value="CA">California</option>
												<option value="CO">Colorado</option>
												<option value="CT">Connecticut</option>
												<option value="DE">Delaware</option>
												<option value="DC">District of Columbia</option>
												<option value="FL">Florida</option>
												<option value="GA">Georgia</option>
												<option value="HI">Hawaii</option>
												<option value="ID">Idaho</option>
												<option value="IL">Illinois</option>
												<option value="IN">Indiana</option>
												<option value="IA">Iowa</option>
												<option value="KS">Kansas</option>
												<option value="KY">Kentucky</option>
												<option value="LA">Louisiana</option>
												<option value="ME">Maine</option>
												<option value="MD">Maryland</option>
												<option value="MA">Massachusetts</option>
												<option value="MI">Michigan</option>
												<option value="MN">Minnesota</option>
												<option value="MS">Mississippi</option>
												<option value="MO">Missouri</option>
												<option value="MT">Montana</option>
												<option value="NE">Nebraska</option>
												<option value="NV">Nevada</option>
												<option value="NH">New Hampshire</option>
												<option value="NJ">New Jersey</option>
												<option value="NM">New Mexico</option>
												<option value="NY">New York</option>
												<option value="NC">North Carolina</option>
												<option value="ND">North Dakota</option>
												<option value="OH">Ohio</option>
												<option value="OK">Oklahoma</option>
												<option value="OR">Oregon</option>
												<option value="PA">Pennsylvania</option>
												<option value="PR">Puerto Rico</option>
												<option value="RI">Rhode Island</option>
												<option value="SC">South Carolina</option>
												<option value="SD">South Dakota</option>
												<option value="TN">Tennessee</option>
												<option value="TX">Texas</option>
												<option value="UT">Utah</option>
												<option value="VT">Vermont</option>
												<option value="VA">Virginia</option>
												<option value="WA">Washington</option>
												<option value="WV">West Virginia</option>
												<option value="WI">Wisconsin</option>
												<option value="WY">Wyoming</option>
											</select>
										</div>
                                        
                                       <div class="form-group" id="question110-group">
											<label for="question110" style="font-weight:normal">Will you be evaluating new Health Benefit providers for your employees in the next 6 months? </label>
											<select class="form-control input-sm lfreq" required="required" id="custom" name="custom" placeholder="state">
												
                                   <option value="">Select</option>
                                   <option value="Yes">Yes</option>
                                   <option value="No">No</option>
                                        </select>
										</div>
										<p class="buttons text-center">
                                        
                                        <input type="submit">
                                        
											<!--<a href="download-page.html">
												<img src="images/form-button-submit.jpg" width="135" height="36">
												</a>  -->      
							        </p>
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
                                </form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<footer class="footer">
				<div class="container">
					<p class="text-muted">© Copyright 2017, League, All rights reserved.</p>
				</div>
			</footer>

    <!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
        <script type="text/javascript">
            $("#worktango").validate();
        </script>
</body>
</html>
