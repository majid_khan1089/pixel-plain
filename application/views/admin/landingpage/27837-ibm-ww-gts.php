<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>The Forrester Total Economic Impact™ of IBM Multivendor Support Services</title>
    
    <!-- Styles -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<link href="http://www.mqltosql.com/email-template/HTML/27837-IBM-WW-GTS/css/styles.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
 
   <style type="text/css">
            input[type=submit] {
          background:url(http://www.mqltosql.com/email-template/HTML/27837-IBM-WW-GTS/images/form-button-submit.jpg);
          color: transparent;
          width: 135px;
          height: 38px;
        }
        .error{
            color: red;
            font-weight: normal;
        }
        </style>
  
  

</head>
<body>

    
<!-- Fixed navbar -->
<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand">
				<img src="http://www.mqltosql.com/email-template/HTML/27837-IBM-WW-GTS/images/image1.png" alt="Forrester" />
			</a>
		</div>
		<!-- the actual nav items -->
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav"></ul>
		</div>
		<!--/.nav-collapse -->
	</div>
</nav>
<!-- main content -->
<div class="container">
	<br />
	<div class="row">
		<!-- main column -->
		<div class="col-md-5">
        <h3> The Forrester Total Economic Impact&#8482; of IBM Multivendor Support Services </h3>
					
        <div class="row">
        	<div class="col-sm-12">
			<div class="form-page-content">
				<input id="categoryid" name="categoryid" value="491" type="hidden">
                	
					<img class="img-responsive pull-right" src="http://www.mqltosql.com/email-template/HTML/27837-IBM-WW-GTS/images/The-Forrester-TEI-Total-Economic-Impact-of-IBM-Multivendor-Services.jpg">
					<p style="font-weight:bold;">The IBM Multivendor Support Services Estimator Tool will help you estimate your potential 3-year cost savings when you contract with IBM as your technology support provider for your heterogeneous IT environment. </p>
                    <p>This interactive economic impact calculator is based upon a Forrester Consulting study with IBM: The Total Economic Impact of IBM Multivendor Support Services. </p>
                    <p> For the study Forrester surveyed 25 organizations that use IBM hardware support services to identify and quantify key benefits of investing in IBM Multivendor Support Services (MVS), including : </p>
                    <ul>
                    
                    <li> IT tech support and maintenance spending savings </li>
                    <li> Hardware support tasks savings </li>
                    <li> Vendor relationship management savings </li>
                    <li> Capital expense deferred </li>
                    
                    </ul>
                    <p>Note: IBM defines Multivendor as the heterogeneous hardware (made by any vendor) you have in your data center environment. For the purposes of this tool, we focus just on equipment in your data center; however, IBM offers technology support services that extend beyond your data center to connected sensors, and end-user and mobile devices. </p>
                    <br>
					</div>
          </div>
                    
          </div>
				</div>
				<!-- right column -->
				<div class="col-md-7">
					<div class="panel panel-primary reg-form">
						<div class="dnfrmc full" id="dnfrmmask">
                        <form class="autoform" id="symantech" method="Post" name="symantech">
							<div class="reg-form-panel">
								<div id="leadform-error"></div>
								<p class="intro">Please fill in the information below to access this resource:</p>
								<div class="row">
									<div class="col-sm-4">
										<div class="form-group" id="question1-group" style="padding-bottom:27px;">
											<label class="sr-only" for="question1">First Name</label>
											<input type="text" class="form-control input-sm lfreq tblfreq" id="f_name" name="f_name" maxlength="50"  placeholder="First Name" required />
										</div>
										<div class="form-group" id="question2-group" style="padding-bottom:27px;">
											<label class="sr-only" for="question2">Last Name</label>
											<input type="text" class="form-control input-sm lfreq tblfreq" id="l_name" name="l_name" maxlength="50"  placeholder="Last Name" required/>
										</div>
										<div class="form-group" id="question8-group" style="padding-bottom:27px;">
											<label class="sr-only" for="question8">Email Address</label>
											<input type="text" class="form-control input-sm lfreq tblfreq email" id="email" name="email" maxlength="100"  placeholder="Email Address" required/>
										</div>
										<div class="form-group" id="question9-group" style="padding-bottom:27px;">
											<label class="sr-only" for="question9">Telephone</label>
											<input type="text" class="form-control input-sm lfreq tblfreq tel" id="phone" name="phone" maxlength="50"  placeholder="Telephone" required/>
										</div>
										<div class="form-group" id="question3-group" style="padding-bottom:27px;">
											<label class="sr-only" for="question3">Company Name</label>
											<input type="text" class="form-control input-sm lfreq tblfreq" id="company" name="company" maxlength="80"  placeholder="Company Name" required/>
										</div>
										<div class="form-group" id="question110-group" style="padding-bottom:27px;">
											<label class="sr-only" for="question110">Company Size</label>
											<select class="form-control input-sm lfreq" id="company_size" required="required" name="company_size" placeholder="Company Size">
												<option value="">Select Company Size</option>
												<option value="1 to 3">1 to 3</option>
												<option value="4 to 10">4 to 10</option>
												<option value="11 to 25">11 to 25</option>
												<option value="26 to 50">26 to 50</option>
												<option value="51 to 100">51 to 100</option>
												<option value="101 to 200">101 to 200</option>
												<option value="201 to 500">201 to 500</option>
												<option value="501 to 1000">501 to 1000</option>
												<option value="1001 or more">1001 or more</option>
											</select>
										</div>
										<div class="form-group" id="question141-group" style="padding-bottom:27px;">
											<label class="sr-only" for="question141">Job Function</label>
											<select class="form-control input-sm lfreq " id="job_title_level" required="required" name="job_title_level" placeholder="Job Function">
												<option value="">Select Job Function</option>
												<option value="IT Management(Manager IS/IT)">IT Management(Manager IS/IT)</option>
												<option value="Senior Non IT Management(CEO,CFO,VP,Director)">Senior Non IT Management(CEO,CFO,VP,Director)</option>
												<option value="Senior IT Management(CIO/CTO/VP/Director)">Senior IT Management(CIO/CTO/VP/Director)</option>
												<option value="Network Management/Administration">Network Management/Administration</option>
												<option value="IT Staff">IT Staff</option>
												<option value="CSO">CSO</option>
												<option value="Product Management">Product Management</option>
												<option value="Sales/Marketing">Sales/Marketing</option>
												<option value="Telecommunications Manager">Telecommunications Manager</option>
												<option value="Systems Management/Administration">Systems Management/Administration</option>
												<option value="Project Manager">Project Manager</option>
												<option value="Programmer/Developer">Programmer/Developer</option>
												<option value="Privacy Officer">Privacy Officer</option>
												<option value="Consultant/Systems Integrator">Consultant/Systems Integrator</option>
												<option value="Database Administrator">Database Administrator</option>
												<option value="Educator">Educator</option>
												<option value="Non IT Staff">Non IT Staff</option>
												<option value="Architect">Architect</option>
												<option value="Application Manager">Application Manager</option>
												<option value="Analyst">Analyst</option>
											</select>
										</div>
										<div class="form-group" id="question113-group" style="padding-bottom:27px;">
											<label class="sr-only" for="question113">Industry</label>
											<select class="form-control input-sm lfreq " id="industry" required="required" name="industry" placeholder="Industry">
												<option value="">Select Industry</option>
												<option value="Advertising/Marketing">Advertising/Marketing</option>
												<option value="Automotive">Automotive</option>
												<option value="Agriculture/Forestry">Agriculture/Forestry</option>
												<option value="Business Services">Business Services</option>
												<option value="Consulting">Consulting</option>
												<option value="Distribution">Distribution</option>
												<option value="Entertainment">Entertainment</option>
												<option value="Education">Education</option>
												<option value="Engineering/Construction">Engineering/Construction</option>
												<option value="Financial">Financial</option>
												<option value="Food and Beverage">Food and Beverage</option>
												<option value="Government-Federal">Government-Federal</option>
												<option value="Government-State and Local">Government-State and Local</option>
												<option value="Healthcare/Medical">Healthcare/Medical</option>
												<option value="Hospitality">Hospitality</option>
												<option value="Insurance">Insurance</option>
												<option value="Legal">Legal</option>
												<option value="Manufacturing">Manufacturing</option>
												<option value="Non-profit">Non-profit</option>
												<option value="Real-Estate">Real-Estate</option>
												<option value="Retail">Retail</option>
												<option value="Student/Retired/Non-Business">Student/Retired/Non-Business</option>
												<option value="Transportation/Shipping">Transportation/Shipping</option>
												<option value="Technology">Technology</option>
												<option value="Telecommunications">Telecommunications</option>
												<option value="Utilities">Utilities</option>
												<option value="Warehousing">Warehousing</option>
												<option value="Other">Other</option>
											</select>
										</div>
										<div class="form-group" id="question7-group">
											<label class="sr-only" for="question7">Zip Code</label>
											<input type="text" class="form-control input-sm lfreq tblfreq zip" required="required" id="zip" name="zip" maxlength="5"  placeholder="Zip Code" value=""/>
										</div>
									</div>
									<div class="col-sm-8">
										<div class="form-group" id="question110-group" style="padding-bottom:5px;">
											<label for="question110" style="font-weight:normal"> Having good technical support on your data center assets (server, storage and network devices) is critical to keeping your data center running smoothly. Are you satisfied with your current support model? Are you confident in your data center's technical support team / external partner to solve serious problems and proactively prevent outages? </label>
											<select class="form-control input-sm lfreq" required="required" id="custom_question1" name="custom_question1" placeholder="Company Size">
												<option value="">Select </option>
												<option value="No, it could probably be better. What do you offer? Or where can I find out about how IBM TSS might be better?">No, it could probably be better. What do you offer? Or where can I find out about how IBM TSS might be better?</option>
												<option value="I'm not sure, how would I know if ours is good enough?">I'm not sure, how would I know if ours is good enough? </option>
											</select>
										</div>
										<div class="form-group" id="question110-group" style="padding-bottom:5px;">
											<label for="question110" style="font-weight:normal"> Do you know when your next Data Center asset (server, storage and network device) is coming off of OEM warranty or ending its maintenance contract? And, did you know that you can extend the life of that device, and increase your TCO, by getting third party maintenance on those products? </label>
											<select class="form-control input-sm lfreq" required="required" id="custom_question2" name="custom_question2" placeholder="Company Size">
												<option value="">Select </option>
												<option value="Yes and no. What's that? Tell me more. Where can I find more information?"> Yes and no. What's that? Tell me more. Where can I find more information?  </option>
												<option value="No, and no. What's that? Tell me more.Where can I find more information?">  No, and no. What's that? Tell me more.Where can I find more information?  </option>
                                                <option value="No and yes. What's that? Tell me more. Where can I find more information?">  No and yes. What's that? Tell me more. Where can I find more information?  </option>
                                                <option value="I don't know. What's that? Tell me more. Where can I find more information?">  I don't know. What's that? Tell me more. Where can I find more information ?  </option>
											</select>
										</div>
										<div class="form-group" id="question110-group" style="padding-bottom:5px;">
											<label for="question110" style="font-weight:normal"> You probably have IT equipment in your datacenter from more than one manufacturer. You also probably have more than one vendor who is providing the warranty or technical support services for all those devices. Have you given some thought to how you could simplify your technical support by consolidating your support with one, or fewer support vendors (a third party maintainer)? </label>
											<select class="form-control input-sm lfreq" required="required" id="custom_question3" name="custom_question3" placeholder="Company Size">
												<option value="">Select </option>
												<option value="No, haven't thought about that. How would that work? Tell me more. Where can I find more information?">  No, haven't thought about that. How would that work? Tell me more. Where can I find more information? </option>
												<option value="Yes, we've thought about it. And would like to understand our options. How would that work? Tell me more. Where can I find more information?"> Yes, we've thought about it. And would like to understand our options. How would that work? Tell me more. Where can I find more information?</option>
											</select>
										</div>
                                        <div class="form-group" id="question110-group">
    <label for="question110" style="font-weight:normal">IBM companies will keep you informed of their products and services worldwide via Email , Okay. </label>
    <select class="form-control input-sm lfreq" required="required" id="custom_question4" name="custom_question4" placeholder="Company Size">
    <option value="">Select </option>
    <option value="Email">Email</option>
    <option value="Phone">Phone</option>
    <option value="Both">Both</option>
    </select>
</div>
                                        
								  </div>
								</div>
								<p class="buttons text-center">
                                			<input type="submit">                                
									<!--<a href="images/The-Forrester-TEI-Total-Economic-Impact-of-IBM-Multivendor-Services.pdf">
										<img src="images/form-button-submit.jpg" width="135" height="36">
										</a>-->
									</p>
									
						    <div class="clearfix"></div>
								</div>
                                </form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<footer class="footer">
				<div class="container">
					<p class="text-muted">© Copyright 2017, Business Tech Alert, All rights reserved.</p>
				</div>
			</footer>

    <!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
        <script type="text/javascript">
            $("#symantech").validate({
            /*rules: {
                f_name: "required",                
            },
            messages: {
                f_name: "Please enter your firstname"               
            }*/
        });
        </script>

</body>
</html>
