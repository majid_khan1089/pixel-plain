<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"><head>
    
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>Choosing the Right Phone System for Your Business</title>
    
    <!-- Styles -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />
<link href="http://www.martechb2b.com/email-template/HTML/top-10-voip-List/css/styles.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
 
 <style type="text/css">
            input[type=submit] {
          background:url(http://www.martechb2b.com/email-template/HTML/top-10-voip-List/images/form-button-submit.jpg);
          color: transparent;
          width: 135px;
          height: 38px;
        }
        .error{
            color: red;
            font-weight: normal;
        }
        </style>
  

</head>
<body>

    
<!-- Fixed navbar -->
<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container">
		<div class="navbar-header">
			
			<a class="navbar-brand">
				<img src="http://www.martechb2b.com/email-template/HTML/top-10-voip-List/images/Top-10-Logo-Black.png" alt="" />
			</a>
		</div>
		<!-- the actual nav items -->
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav"></ul>
		</div>
		<!--/.nav-collapse -->
	</div>
</nav>
<!-- main content -->
<div class="container">
	<br />
	<div class="row">
		<!-- main column -->
		<div class="col-sm-7">
                <div class="form-page-content">
                  <h1>Choosing the Right Phone System for Your Business</h1>
                
             <div class="asset-image"><img src="http://www.martechb2b.com/email-template/HTML/top-10-voip-List/images/voip.png" /></div>
                <p style="line-height:29px;">	The desire to have VoIP phones has grown over the past few years as new features and products have flooded the marketplace. With an increase in Demand for this essential piece of office equipment growing, it is necessary for you to carefully consider your options while making a selection.</p>
                
                              
                <p style="line-height:29px;">Top 10 VOIP List helps you and your team learn the most important issues that you should consider while deciding which VoIP phone is right for you.</p>
                <p style="line-height:29px;">Make your decision easy and hassle-free.</p>

<br />                


                </div>
            </div>

				<!-- right column -->
				<div class="col-sm-5">
					<div class="panel panel-primary reg-form">
						<div class="dnfrmc full" id="dnfrmmask">
                        <form class="autoform" id="techservice" method="Post" name="techservice">
                         <input type="hidden" name="form-type" value="tech-download">
							<div class="reg-form-panel">
								<div id="leadform-error"></div>
								<p class="intro">Please fill out the form below to request a quote from a leading VOIP provider that best matches your company's needs.</p>
								<div class="row">
									<div class="col-sm-12" style="padding-left: 10px; padding-right: 10px;">
										<div class="form-group" id="question1-group">
											<label class="sr-only" for="question1">First Name</label>
											<input type="text" class="form-control input-sm lfreq tblfreq" id="f_name" name="f_name" maxlength="50"  placeholder="First Name" required />
										</div>
										<div class="form-group" id="question2-group">
											<label class="sr-only" for="question2">Last Name</label>
											<input type="text" class="form-control input-sm lfreq tblfreq" id="l_name" name="l_name" maxlength="50"  placeholder="Last Name" required/>
										</div>
										<div class="form-group" id="question8-group">
											<label class="sr-only" for="question8">Email Address</label>
											<input type="text" class="form-control input-sm lfreq tblfreq email" id="email" name="email" maxlength="100"  placeholder="Email Address" required/>
										</div>
										<div class="form-group" id="question9-group">
											<label class="sr-only" for="question9">Telephone</label>
											<input type="text" class="form-control input-sm lfreq tblfreq tel" id="phone" name="phone" maxlength="50"  placeholder="Telephone" required/>
										</div>
										<div class="form-group" id="question3-group">
											<label class="sr-only" for="question3">Company Name</label>
											<input type="text" class="form-control input-sm lfreq tblfreq" id="company" name="company" maxlength="80"  placeholder="Company Name" required/>
										</div>
										<div class="form-group" id="question110-group">
											<label class="sr-only" for="question110">Company Size</label>
											<select class="form-control input-sm lfreq" required="required" id="company_size" name="company_size" placeholder="Company Size">
												<option value="">Select Company Size</option>
												<option value="1 to 3">1 to 3</option>
												<option value="4 to 10">4 to 10</option>
												<option value="11 to 25">11 to 25</option>
												<option value="26 to 50">26 to 50</option>
												<option value="51 to 100">51 to 100</option>
												<option value="101 to 200">101 to 200</option>
												<option value="201 to 500">201 to 500</option>
												<option value="501 to 1000">501 to 1000</option>
												<option value="1001 or more">1001 or more</option>
											</select>
										</div>
										<div class="form-group" id="question9-group">
											<label class="sr-only" for="question9">Job Title</label>
											<input type="text" class="form-control input-sm lfreq tblfreq tel" id="job_title_level" name="job_title_level" maxlength="50"  placeholder="Job Title" required/>
										</div>
                                        
										<div class="form-group" id="question113-group">
											<label class="sr-only" for="question113">Industry</label>
											<select class="form-control input-sm lfreq" required="required" id="industry" name="industry" placeholder="Industry">
												<option value="">Select Industry</option>
												<option value="Advertising/Marketing">Advertising/Marketing</option>
												<option value="Automotive">Automotive</option>
												<option value="Agriculture/Forestry">Agriculture/Forestry</option>
												<option value="Business Services">Business Services</option>
												<option value="Consulting">Consulting</option>
												<option value="Distribution">Distribution</option>
												<option value="Entertainment">Entertainment</option>
												<option value="Education">Education</option>
												<option value="Engineering/Construction">Engineering/Construction</option>
												<option value="Financial">Financial</option>
												<option value="Food and Beverage">Food and Beverage</option>
												<option value="Government-Federal">Government-Federal</option>
												<option value="Government-State and Local">Government-State and Local</option>
												<option value="Healthcare/Medical">Healthcare/Medical</option>
												<option value="Hospitality">Hospitality</option>
												<option value="Insurance">Insurance</option>
												<option value="Legal">Legal</option>
												<option value="Manufacturing">Manufacturing</option>
												<option value="Non-profit">Non-profit</option>
												<option value="Real-Estate">Real-Estate</option>
												<option value="Retail">Retail</option>
												<option value="Student/Retired/Non-Business">Student/Retired/Non-Business</option>
												<option value="Transportation/Shipping">Transportation/Shipping</option>
												<option value="Technology">Technology</option>
												<option value="Telecommunications">Telecommunications</option>
												<option value="Utilities">Utilities</option>
												<option value="Warehousing">Warehousing</option>
												<option value="Other">Other</option>
											</select>
										</div>
										<div class="form-group" id="question7-group">
											<label class="sr-only" for="question7">Zip Code</label>
											<input type="text" class="form-control input-sm lfreq tblfreq zip" required="required" id="zip" name="zip" maxlength="5"  placeholder="Zip Code" value=""/>
										</div>
                                        
                                        <div class="form-group" id="question110-group">
											<label style="font-weight:normal;" for="question110">What part do you play in the decision making process?</label>
											<select class="form-control input-sm lfreq" required="required" id="custom1" name="custom1" placeholder="Company Size">
												<option value="">Select </option>
												<option value="Decision Maker">Decision Maker</option>
												<option value="Influencer/Recommender">Influencer/Recommender</option>
                                                <option value="Other">Other</option>
											</select>
										</div>
                                        
                                        <div class="form-group" id="question110-group">
											<label style="font-weight:normal;" for="question110">When would you be in the market to purchase a Business Phone System?</label>
											<select class="form-control input-sm lfreq" required="required" id="custom2" name="custom2" placeholder="Company Size">
												<option value="">Select </option>
												<option value="0-3 Months">0-3 Months</option>
												<option value="3-6 Months">3-6 Months</option>
                                                <option value="6>">6></option>
											</select>
										</div>
                                        
                                        
                                        <div class="form-group" id="question110-group">
											<label style="font-weight:normal;" for="question110">Would you be interested in receiving a call back and a quote from one of our VOIP partners within the next few days?</label>
											<select class="form-control input-sm lfreq" required="required" id="custom3" name="custom3" placeholder="Company Size">
												<option value="">Select </option>
												<option value="Yes">Yes</option>
												<option value="No">No</option>
											</select>
										</div>
										<p class="buttons text-center">
                                       		
                                             <input type="submit">
                                       		
											<!--<a href="download-page.html">
												<img src="images/form-button-submit.jpg" width="135" height="36">
												</a>-->
						          </p>
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
                                </form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<footer class="footer">
				<div class="container">
					<p class="text-muted">© Copyright 2017, Top 10 VOIP List, All rights reserved.</p>
				</div>
			</footer>

    <!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
        <script type="text/javascript">
            $("#techservice").validate();
        </script>

</body>
</html>
