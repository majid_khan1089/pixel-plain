<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"><head>
    
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>11 Ways Facilities Departments Manage Spend Better</title>
    
    <!-- Styles -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />
<link href="http://martechb2b.com/email-template/HTML/service-channel-new/css/styles.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
 
   <style type="text/css">
            input[type=submit] {
          background:url(http://martechb2b.com/email-template/HTML/service-channel-new/images/read-more.png);
          color: transparent;
          width: 250px;
          height: 43px;
		  border:0px;
        }
        .error{
            color: red;
            font-weight: normal;
        }
        </style>

</head>
<body>

    
<!-- Fixed navbar -->
<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container">
		<div class="navbar-header">
			
			<a class="navbar-brand">
				<img src="http://martechb2b.com/email-template/HTML/service-channel-new/images/service-chennal-logo-small.png" alt="" />
			</a>
		</div>
		<!-- the actual nav items -->
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav"></ul>
		</div>
		<!--/.nav-collapse -->
	</div>
</nav>
<!-- main content -->
<div class="container">
	<br />
	<div class="row">
		<!-- main column -->
		<div class="col-sm-7">
                <div class="form-page-content">
                  <h1>11 Ways Facilities Departments Manage Spend Better</h1>
                
             <div class="asset-image"><img src="http://martechb2b.com/email-template/HTML/service-channel-new/images/11_Ways_Ebook_3Dimensional.png" /></div>
               
                <p>Hundreds of companies are innovating their FM programs with systems like ServiceChannel to save real dollars! Being smarter about data, analytics and business intelligence are the new job requirements. </p>
                
                <p>How do the leading multi-site brands use technology to save millions of dollars from their facilities management operations - without any capital investment? This detailed and comprehensive Ebook will tell you everything you need to know about <strong>11 Ways Facilities Departments Manage Spend Better with ServiceChannel </strong>including how: </p>
                
                
                
<ul>
							<li><strong>Real time reports</strong> and alerts focus on bottom line impact</li>
							<li><strong>Reduced broker management</strong> fees cut R&M expenses</li>
							<li><strong>Scorecarding</strong> boosts external contractor performance</li>
							<li><strong>Taking control of NTE</strong> lets you take control of spend</li>
                            <li><strong>Innovations</strong> drive significant costs savings and revenue impact</li>
						</ul>
                             

<br />                


                </div>
            </div>

				<!-- right column -->
				<div class="col-sm-5">
					<div class="panel panel-primary reg-form">
						<div class="dnfrmc full" id="dnfrmmask">
                        <form class="autoform" id="service_channel_new" method="Post" name="service_channel_new">
                         <input type="hidden" name="form-type" value="service-channel-new-download">
							<div class="reg-form-panel">
								<div id="leadform-error"></div>
								<p class="intro" style="font-size:22px;">GET YOUR FREE EBOOK NOW</p>
								<div class="row">
									<div class="" style="padding-left: 10px; padding-right: 10px;">
                                    	<div class="col-sm-12"> 
										<div class="form-group" id="question1-group">
											<label class="sr-only" for="question1">First Name</label>
											<input type="text" class="form-control input-sm lfreq tblfreq" id="f_name" name="f_name" maxlength="50"  placeholder="First Name" required />
										</div>
                                        </div>
                                        <div class="col-sm-12">
										<div class="form-group " id="question2-group">
											<label class="sr-only" for="question2">Last Name</label>
											<input type="text" class="form-control input-sm lfreq tblfreq" id="l_name" name="l_name" maxlength="50"  placeholder="Last Name" required/>
										</div>
                                        </div>
                                        <div class="col-sm-12">
										<div class="form-group" id="question8-group">
											<label class="sr-only" for="question8">Email Address</label>
											<input type="text" class="form-control input-sm lfreq tblfreq email" id="email" name="email" maxlength="100"  placeholder="Email Address" required/>
										</div>
                                        </div>
                                       
                                        <div class="col-sm-12">
										<div class="form-group" id="question3-group">
											<label class="sr-only" for="question3">Company Name</label>
											<input type="text" class="form-control input-sm lfreq tblfreq" id="company" name="company" maxlength="80"  placeholder="Company Name" required/>
										</div>
                                        </div>
                                        
                                        <div class="col-sm-12">
                                        <div class="form-group" id="question113-group">
											<label class="sr-only" for="question113">Industry</label>
											<select class="form-control input-sm lfreq" required="required" id="industry" name="industry" placeholder="Industry">
												<option value="">Select Industry</option>
												   <option value="Construction">Construction</option>
   <option value="Consulting">Consulting</option>
   <option value="Convenience Stores">Convenience Stores</option>
   <option value="Distribution / Wholesale">Distribution / Wholesale</option>
   <option value="Education">Education</option>
   <option value="Energy / Utilities">Energy / Utilities</option>
   <option value="Engineering">Engineering</option>
   <option value="Entertainment">Entertainment</option>
   <option value="Environmental">Environmental</option>
   <option value="Financial Services">Financial Services</option>
   <option value="Fitness / Health Clubs / Spas">Fitness / Health Clubs / Spas</option>
   <option value="Government / Public">Government / Public</option>
   <option value="Grocery">Grocery</option>
   <option value="Healthcare">Healthcare</option>
   <option value="Hospitality">Hospitality</option>
   <option value="Logistics">Logistics</option>
   <option value="Manufacturing">Manufacturing</option>
   <option value="Not For Profit">Not For Profit</option>
   <option value="Parking">Parking</option>
   <option value="Property Management / Housing">Property Management / Housing</option>
   <option value="Recreation">Recreation</option>
   <option value="Restaurants">Restaurants</option>
   <option value="Retail">Retail</option>
   <option value="Service Provider / Contractor">Service Provider / Contractor</option>
   <option value="Technology">Technology</option>
   <option value="Telecommunications">Telecommunications</option>
   <option value="Storage">Storage</option>
   <option value="Transportation">Transportation</option>
   <option value="Other">Other</option>
											</select>
										</div>
                                     	</div>
                                        
										
                                        <div class="col-sm-12">   
										<div class="form-group" id="question110-group">
											<label for="question110" style="font-weight:normal">Which best describes your company?</label>
											<select class="form-control input-sm lfreq" required="required" id="custom" name="custom" placeholder="state">
												<option value="Other">Select</option>
      <option value="We have multiple locations (stores, restaurants, buildings, facilities, properties, etc.)">We have multiple locations (stores, restaurants, buildings, facilities, properties, etc.)</option>
   <option value="We have a single location">We have a single location</option>
   <option value="Contractors who repair and maintain facilities">Contractors who repair and maintain facilities</option>
   <option value="Consultants">Consultants</option>
   <option value="Industry Analysts">Industry Analysts</option>
   <option value="Press / Media">Press / Media</option>
   <option value="These options don't describe me at all!">These options don't describe me at all!</option>

											</select>
										</div>
                                        </div>
                                        
										<p class="buttons text-center">
											<input type="submit">
							        </p>
                                    
                                    
										</div> 
									</div>
									<div class="clearfix"></div>
								</div>
                                </form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<footer class="footer">
				<div class="container">
					<p class="text-muted">© Copyright 2017, ServiceChannel, All rights reserved.</p>
				</div>
			</footer>

    <!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
        <script type="text/javascript">
            $("#service_channel_new").validate();
        </script>
</body>
</html>
