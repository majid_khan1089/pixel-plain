<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    
   <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>Tips to Improve the Customer Experience with Digital Intelligence</title>
    
    <!-- Styles -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<link href="http://www.martechb2b.com/email-template/HTML/30029-ibm/css/styles.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
 
  <style type="text/css">
            input[type=submit] {
          background:url(http://www.martechb2b.com/email-template/HTML/30029-ibm/images/watch-now-top.png);
          color: transparent;
          width: 275px;
          height: 52px;
        }
        .error{
            color: red;
            font-weight: normal;
        }
        </style> 

</head>
<body>

    
<!-- Fixed navbar -->
<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container">
		<div class="navbar-header">
			
			<a class="navbar-brand">
				<img src="http://www.martechb2b.com/email-template/HTML/30029-ibm/images/Logo-png-transparent.png" alt="" />
			</a>
		</div>
		<!-- the actual nav items -->
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav"></ul>
		</div>
		<!--/.nav-collapse -->
	</div>
</nav>
<!-- main content -->
<div class="container">
	<br />
	<div class="row">
		<!-- main column -->
		<div class="col-md-5">
			<div class="form-page-content">

					<h1>Tips to Improve the Customer Experience with Digital Intelligence</h1>
                   
                    <p><img class="img-responsive" src="http://www.martechb2b.com/email-template/HTML/30029-ibm/images/video-image.jpg"></p>
                    
                    <p>The age of the customer is upon us, brought on by the winds of digital change. Customers now have more power in dealing brands, probably the most power they've had in history. Digital channels have made this easier for the consumer to engage with us, but also with our competitors and each other.</p>
                    
                     <p>Now we are competing on the digital front-ends to compete for customers. Traditional analytics are ill equipped to help with this, because it is backward looking. We can't really correlate analytics across all the channels and makes it hard to use them in the moment the customer shows up.</p>
                     
                     <p>Watch this webinar to hear the experts on the current state of digital analytics and learn why not upgrading is a threat to your business. James McCormack of Forrester will detail what steps you need to take now.</p>
					</div>
				</div>
				<!-- right column -->
				<div class="col-sm-7">
					<div class="panel panel-primary reg-form">
						<div class="dnfrmc full" id="dnfrmmask">
                        <form class="autoform" id="ibm_webinar" method="Post" name="ibm_webinar">
                         <input type="hidden" name="form-type" value="ibm-webiner-download">
							<div class="reg-form-panel">
								<div id="leadform-error"></div>
								<p class="intro">Please fill in the information below to access this resource:</p>
								<div class="row">
									<div class="col-sm-4">
										<div class="form-group" id="question1-group" style="padding-bottom:8px">
											<label class="sr-only" for="question1">First Name</label>
											<input type="text" class="form-control input-sm lfreq tblfreq"  id="f_name" name="f_name" maxlength="50"  placeholder="First Name" required />
										</div>
										<div class="form-group" id="question2-group" style="padding-bottom:8px">
											<label class="sr-only" for="question2">Last Name</label>
											<input type="text" class="form-control input-sm lfreq tblfreq" id="l_name" name="l_name" maxlength="50"  placeholder="Last Name" required/>
										</div>
										<div class="form-group" id="question8-group" style="padding-bottom:8px">
											<label class="sr-only" for="question8">Email Address</label>
											<input type="text" class="form-control input-sm lfreq tblfreq email" id="email" name="email" maxlength="100"  placeholder="Email Address" required/>
										</div>
										<div class="form-group" id="question9-group" style="padding-bottom:8px">
											<label class="sr-only" for="question9">Telephone</label>
											<input type="text" class="form-control input-sm lfreq tblfreq tel" id="phone" name="phone" maxlength="50"  placeholder="Telephone" required/>
										</div>
										<div class="form-group" id="question3-group" style="padding-bottom:8px">
											<label class="sr-only" for="question3">Company Name</label>
											<input type="text" class="form-control input-sm lfreq tblfreq" id="company" name="company" maxlength="80"  placeholder="Company Name" required/>
										</div>
										<div class="form-group" id="question110-group" style="padding-bottom:8px">
											<label class="sr-only" for="question110">Company Size</label>
											<select class="form-control input-sm lfreq" required="required" id="company_size" name="company_size" placeholder="Company Size">
												<option value="">Select Company Size</option>
												<option value="1 to 3">1 to 3</option>
												<option value="4 to 10">4 to 10</option>
												<option value="11 to 25">11 to 25</option>
												<option value="26 to 50">26 to 50</option>
												<option value="51 to 100">51 to 100</option>
												<option value="101 to 200">101 to 200</option>
												<option value="201 to 500">201 to 500</option>
												<option value="501 to 1000">501 to 1000</option>
												<option value="1001 or more">1001 or more</option>
											</select>
										</div>
										<div class="form-group" id="question141-group" style="padding-bottom:8px">
											<label class="sr-only" for="question141">Job Function</label>
											<select class="form-control input-sm lfreq" required="required" id="job_title_level" name="job_title_level" placeholder="Job Function">
												<option value="">Select Job Function</option>
												<option value="IT Management(Manager IS/IT)">IT Management(Manager IS/IT)</option>
												<option value="Senior Non IT Management(CEO,CFO,VP,Director)">Senior Non IT Management(CEO,CFO,VP,Director)</option>
												<option value="Senior IT Management(CIO/CTO/VP/Director)">Senior IT Management(CIO/CTO/VP/Director)</option>
												<option value="Network Management/Administration">Network Management/Administration</option>
												<option value="IT Staff">IT Staff</option>
												<option value="CSO">CSO</option>
												<option value="Product Management">Product Management</option>
												<option value="Sales/Marketing">Sales/Marketing</option>
												<option value="Telecommunications Manager">Telecommunications Manager</option>
												<option value="Systems Management/Administration">Systems Management/Administration</option>
												<option value="Project Manager">Project Manager</option>
												<option value="Programmer/Developer">Programmer/Developer</option>
												<option value="Privacy Officer">Privacy Officer</option>
												<option value="Consultant/Systems Integrator">Consultant/Systems Integrator</option>
												<option value="Database Administrator">Database Administrator</option>
												<option value="Educator">Educator</option>
												<option value="Non IT Staff">Non IT Staff</option>
												<option value="Architect">Architect</option>
												<option value="Application Manager">Application Manager</option>
												<option value="Analyst">Analyst</option>
											</select>
										</div>
										<div class="form-group" id="question113-group" style="padding-bottom:9px">
											<label class="sr-only" for="question113">Industry</label>
											<select class="form-control input-sm lfreq" required="required" id="industry" name="industry" placeholder="Industry">
												<option value="">Select Industry</option>
												<option value="Advertising/Marketing">Advertising/Marketing</option>
												<option value="Automotive">Automotive</option>
												<option value="Agriculture/Forestry">Agriculture/Forestry</option>
												<option value="Business Services">Business Services</option>
												<option value="Consulting">Consulting</option>
												<option value="Distribution">Distribution</option>
												<option value="Entertainment">Entertainment</option>
												<option value="Education">Education</option>
												<option value="Engineering/Construction">Engineering/Construction</option>
												<option value="Financial">Financial</option>
												<option value="Food and Beverage">Food and Beverage</option>
												<option value="Government-Federal">Government-Federal</option>
												<option value="Government-State and Local">Government-State and Local</option>
												<option value="Healthcare/Medical">Healthcare/Medical</option>
												<option value="Hospitality">Hospitality</option>
												<option value="Insurance">Insurance</option>
												<option value="Legal">Legal</option>
												<option value="Manufacturing">Manufacturing</option>
												<option value="Non-profit">Non-profit</option>
												<option value="Real-Estate">Real-Estate</option>
												<option value="Retail">Retail</option>
												<option value="Student/Retired/Non-Business">Student/Retired/Non-Business</option>
												<option value="Transportation/Shipping">Transportation/Shipping</option>
												<option value="Technology">Technology</option>
												<option value="Telecommunications">Telecommunications</option>
												<option value="Utilities">Utilities</option>
												<option value="Warehousing">Warehousing</option>
												<option value="Other">Other</option>
											</select>
										</div>
										<div class="form-group" id="question7-group" style="padding-bottom:9px">
											<label class="sr-only" for="question7">Zip Code</label>
											<input type="text" class="form-control input-sm lfreq tblfreq zip" id="zip" name="zip" maxlength="5"  placeholder="Zip Code" value=""/>
										</div>
									</div>
									<div class="col-sm-8">
										<div class="form-group" id="question110-group" style="padding-bottom:30px">
											<label for="question110" style="font-weight:normal">Are you currently measuring your customers experience with your enterprise? </label>
											<select class="form-control input-sm lfreq" required="required" id="custom1" name="custom1" placeholder="Company Size">
												<option value="">Select </option>
												<option value="No, we are not currently measuring customer experience">No, we are not currently measuring customer experience</option>
                                                <option value="We are currently researching measurement tools">We are currently researching measurement tools</option>
                                              
											</select>
										</div>
										<div class="form-group" id="question110-group" style="padding-bottom:30px">
											<label for="question110" style="font-weight:normal;">Which of the following customer analytics issues are you struggling with the most?  </label>
											<select class="form-control input-sm lfreq" required="required" id="custom2" name="custom2" placeholder="Company Size">
												<option value="">Select </option>
												<option value="Uniting teams around the customer">Uniting teams around the customer</option>
                                                <option value="Seeing the journey your customers travel">Seeing the journey your customers travel</option>
                                                <option value="Understanding customer behavior">Understanding customer behavior</option>
                                                
												
											</select>
										</div>
										<div class="form-group" id="question110-group" style="padding-bottom:30px">
											<label for="question110" style="font-weight:normal">Which customer analytics measurement would you benefit most from? </label>
											<select class="form-control input-sm lfreq" required="required" id="custom3" name="custom3" placeholder="Company Size">
												<option value="">Select </option>
												<option value="Understanding customer demographics">Understanding customer demographics </option>
                                                <option value="Understanding customer purchase history">Understanding customer purchase history </option>
                                                <option value="Understanding who your customers really are">Understanding who your customers really are</option>
                                                <option value="Understanding your customers experience with your brand">Understanding your customers experience with your brand</option>
                                                
											</select>
										</div>
                                        
                                        <div class="form-group" id="question110-group" style="padding-bottom:20px">
											<label for="question110" style="font-weight:normal">Please keep me informed of products, services and offerings from IBM companies worldwide </label>
											<select class="form-control input-sm lfreq" required="required" id="custom4" name="custom4" placeholder="Company Size">
												<option value="">Select </option>
												<option value="Yes">Yes </option>
                                                <option value="No">No</option>
                                                
											</select>
										</div>
                                  </div>
								</div>
								<p class="buttons text-center">
                                <input type="submit">
									<!--<a href="landing-page.html">
										<img src="images/form-button-submit.jpg" width="135" height="36">
										</a>-->
									</p>
									
						    <div class="clearfix"></div>
								</div>
                                </form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<footer class="footer">
				<div class="container">
					<p class="text-muted">© Copyright 2017, IBM, All rights reserved.</p>
				</div>
			</footer>

    <!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
        <script type="text/javascript">
            $("#ibm_webinar").validate();
        </script>
</body>
</html>
