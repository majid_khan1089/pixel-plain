<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"><head>
    
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>A Lot Hinges on Having the Right Needle</title>
    
    <!-- Styles -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />
<link href="http://www.martechb2b.com/email-template/HTML/Terumo/css/styles.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
 
<style type="text/css">
            input[type=submit] {
          background:url(http://www.martechb2b.com/email-template/HTML/Terumo/images/form-button-submit.jpg);
          color: transparent;
          width: 135px;
          height: 36px;
		  border:0px;
        }
        .error{
            color: red;
            font-weight: normal;
        }
        </style>  


</head>
<body>

    
<!-- Fixed navbar -->
<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container">
		<div class="navbar-header">
			
			<a class="navbar-brand">
				<img src="http://www.martechb2b.com/email-template/HTML/Terumo/images/TER-1-landing-page.png" alt="" />
			</a>
		</div>
		<!-- the actual nav items -->
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav"></ul>
		</div>
		<!--/.nav-collapse -->
	</div>
</nav>
<!-- main content -->
<div class="container">
	<br />
	<div class="row">
		<!-- main column -->
		<div class="col-sm-9">
                <div class="form-page-content">
                  <h1 style="color:#009367"><strong>We make some great points.</strong></h1>
                  <h2 style="color:#009367"><strong>Sharper. Safer. Smarter.</strong> Scroll down to see how Terumo's <strong>SurGuard&reg;3</strong> has everything you're looking for in a safety needle. </h2>
             <div class="row">
             <div class="col-sm-6 col-sm-offset-3">   
             <div><img class="img-responsive" src="http://www.martechb2b.com/email-template/HTML/Terumo/images/TER-2.png" /></div>
               </div>
                <div class="col-sm-8 col-sm-offset-2">   
             <div><div class="video-con">
					<div class="embed-responsive embed-responsive-16by9">
					  	<video id="hero-video" controls poster="http://www.martechb2b.com/email-template/HTML/Terumo/images/TER-8.png">
							<source src="http://www.martechb2b.com/email-template/HTML/Terumo/images/SurGuard3.webm" type="video/webm">
							<source src="http://www.martechb2b.com/email-template/HTML/Terumo/images/SurGuard3.mp4" type="video/mp4">
							<source src="http://www.martechb2b.com/email-template/HTML/Terumo/images/SurGuard3.ogv" type="video/ogv">
						</video>
					</div>
				</div></div>
                </div>
              </div>  
		<br/><br/>
             


                </div>
            </div>

				<!-- right column -->
				<div class="col-sm-3">
					<div class="panel panel-primary reg-form">
						<div class="dnfrmc full" id="dnfrmmask">
                        <form class="autoform" id="terumo" method="Post" name="terumo">
                         <input type="hidden" name="form-type" value="terumo-download">
							<div class="reg-form-panel">
								<div id="leadform-error"></div>
								<p class="intro">Please fill in the information below to access this resource:</p>
								<div class="row">
									<div class="col-sm-12" style="padding-left: 10px; padding-right: 10px;">
										<div class="form-group" id="question1-group">
											<label class="sr-only" for="question1">First Name</label>
											<input type="text" class="form-control input-sm lfreq tblfreq" id="f_name" name="f_name" maxlength="50"  placeholder="First Name" required />
										</div>
										<div class="form-group" id="question2-group">
											<label class="sr-only" for="question2">Last Name</label>
											<input type="text" class="form-control input-sm lfreq tblfreq" id="l_name" name="l_name" maxlength="50"  placeholder="Last Name" required/>
										</div>
										<div class="form-group" id="question8-group">
											<label class="sr-only" for="question8">Email Address</label>
											<input type="text" class="form-control input-sm lfreq tblfreq email" id="email" name="email" maxlength="100"  placeholder="Email Address" required/>
										</div>
										<div class="form-group" id="question9-group">
											<label class="sr-only" for="question9">Telephone</label>
											<input type="text" class="form-control input-sm lfreq tblfreq tel" id="phone" name="phone" maxlength="50"  placeholder="Telephone" required/>
										</div>
										<div class="form-group" id="question3-group">
											<label class="sr-only" for="question3">Company Name</label>
											<input type="text" class="form-control input-sm lfreq tblfreq" id="company" name="company" maxlength="80"  placeholder="Company Name" required/>
										</div>
										<div class="form-group" id="question110-group">
											<label class="sr-only" for="question110">Company Size</label>
											<select class="form-control input-sm lfreq" required="required" id="company_size" name="company_size" placeholder="Company Size">
												<option value="">Select Company Size</option>
												<option value="1 to 3">1 to 3</option>
												<option value="4 to 10">4 to 10</option>
												<option value="11 to 25">11 to 25</option>
												<option value="26 to 50">26 to 50</option>
												<option value="51 to 100">51 to 100</option>
												<option value="101 to 200">101 to 200</option>
												<option value="201 to 500">201 to 500</option>
												<option value="501 to 1000">501 to 1000</option>
												<option value="1001 or more">1001 or more</option>
											</select>
										</div>
                                        
                                        <div class="form-group" id="question3-group">
											<label class="sr-only" for="question3">Job Title</label>
											<input type="text" class="form-control input-sm lfreq tblfreq" id="job_title" name="job_title" maxlength="80"  placeholder="Job Title" required/>
										</div>
                                        
										
										<div class="form-group" id="question113-group">
											<label class="sr-only" for="question113">Industry</label>
											<select class="form-control input-sm lfreq" required="required" id="industry" name="industry" placeholder="Industry">
												<option value="">Select Industry</option>
												<option value="Advertising/Marketing">Advertising/Marketing</option>
												<option value="Automotive">Automotive</option>
												<option value="Agriculture/Forestry">Agriculture/Forestry</option>
												<option value="Business Services">Business Services</option>
												<option value="Consulting">Consulting</option>
												<option value="Distribution">Distribution</option>
												<option value="Entertainment">Entertainment</option>
												<option value="Education">Education</option>
												<option value="Engineering/Construction">Engineering/Construction</option>
												<option value="Financial">Financial</option>
												<option value="Food and Beverage">Food and Beverage</option>
												<option value="Government-Federal">Government-Federal</option>
												<option value="Government-State and Local">Government-State and Local</option>
												<option value="Healthcare/Medical">Healthcare/Medical</option>
												<option value="Hospitality">Hospitality</option>
												<option value="Insurance">Insurance</option>
												<option value="Legal">Legal</option>
												<option value="Manufacturing">Manufacturing</option>
												<option value="Non-profit">Non-profit</option>
												<option value="Real-Estate">Real-Estate</option>
												<option value="Retail">Retail</option>
												<option value="Student/Retired/Non-Business">Student/Retired/Non-Business</option>
												<option value="Transportation/Shipping">Transportation/Shipping</option>
												<option value="Technology">Technology</option>
												<option value="Telecommunications">Telecommunications</option>
												<option value="Utilities">Utilities</option>
												<option value="Warehousing">Warehousing</option>
												<option value="Other">Other</option>
											</select>
										</div>
										<div class="form-group" id="question7-group">
											<label class="sr-only" for="question7">City</label>
											<input type="text" class="form-control input-sm lfreq tblfreq zip" id="city" name="city" maxlength="5"  placeholder="city" value="" required/>
										</div>
                                        <div class="form-group" id="question113-group">
											<label class="sr-only" for="question113">State</label>
											<select class="form-control input-sm lfreq" required="required" id="State" name="State" placeholder="Industry">
												<option value="">Select State</option>
												<option value="Alabama">Alabama</option>
										<option value="Alaska">Alaska</option>
										<option value="Arizona">Arizona</option>
										<option value="Arkansas">Arkansas</option>
										<option value="California">California</option>
										<option value="Colorado">Colorado</option>
										<option value="Connecticut">Connecticut</option>
										<option value="Delaware">Delaware</option>
										<option value="District of Columbia">District of Columbia</option>
										<option value="Florida">Florida</option>
										<option value="Georgia">Georgia</option>
										<option value="Hawaii">Hawaii</option>
										<option value="Idaho">Idaho</option>
										<option value="Illinois">Illinois</option>
										<option value="Indiana">Indiana</option>
										<option value="Iowa">Iowa</option>
										<option value="Kansas">Kansas</option>
										<option value="Kentucky">Kentucky</option>
										<option value="Louisiana">Louisiana</option>
										<option value="Maine">Maine</option>
										<option value="Maryland">Maryland</option>
										<option value="Massachusetts">Massachusetts</option>
										<option value="Michigan">Michigan</option>
										<option value="Minnesota">Minnesota</option>
										<option value="Mississippi">Mississippi</option>
										<option value="Missouri">Missouri</option>
										<option value="Montana">Montana</option>
										<option value="Nebraska">Nebraska</option>
										<option value="Nevada">Nevada</option>
										<option value="New Hampshire">New Hampshire</option>
										<option value="New Jersey">New Jersey</option>
										<option value="New Mexico">New Mexico</option>
										<option value="New York">New York</option>
										<option value="North Carolina">North Carolina</option>
										<option value="North Dakota">North Dakota</option>
										<option value="Ohio">Ohio</option>
										<option value="Oklahoma">Oklahoma</option>
										<option value="Oregon">Oregon</option>
										<option value="Pennsylvania">Pennsylvania</option>
										<option value="Rhode Island">Rhode Island</option>
										<option value="South Carolina">South Carolina</option>
										<option value="South Dakota">South Dakota</option>
										<option value="Tennessee">Tennessee</option>
										<option value="Texas">Texas</option>
										<option value="Utah">Utah</option>
										<option value="Vermont">Vermont</option>
										<option value="Virginia">Virginia</option>
										<option value="Washington">Washington</option>
										<option value="West Virginia">West Virginia</option>
										<option value="Wisconsin">Wisconsin</option>
										<option value="Wyoming">Wyoming</option>
											</select>
										</div>
                                        <div class="form-group" id="question110-group">
											<label style="font-weight:normal" for="question110">Would you like to receive a free sample of Hypodermic needles</label>
											<select class="form-control input-sm lfreq" required="required" id="company_size" name="company_size" placeholder="Company Size">
												<option value="">Select</option>
												<option value="Yes">Yes</option>
												<option value="No">No</option>
											</select>
										</div>
                                        <p> </p>
										<p class="buttons text-center">
                                        <input type="submit">
											<!--<a href="download-page.html">
												<img src="images/form-button-submit.jpg" width="135" height="36">
												</a>-->
							        </p>
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
                                </form>
							</div>
						</div>
					</div>
				</div>
                <div class="row">
                
                 <div class="row"> 
                <div class="col-sm-12">
                <div style=" background-color: #009367;height: 30px;">       
                 </div>
                <div align="center"> <img class="img-responsive" src="http://www.martechb2b.com/email-template/HTML/Terumo/images/TER-4.png"></div>
                
                </div>                
                </div>
                <br/><br/>
                <div class="col-sm-6">
              <img class="img-responsive" src="http://www.martechb2b.com/email-template/HTML/Terumo/images/index.png"><br/><br/>
              <img class="img-responsive" src="http://www.martechb2b.com/email-template/HTML/Terumo/images/TER-5.png"><br/><br/>
              
              <div class="grey-box">
					With industry-leading sharpness and a broad range of sizes, SurGuard3 is making everyone comfortable.
				</div>
              <img class="img-responsive" src="http://www.martechb2b.com/email-template/HTML/Terumo/images/TER-6.png" >
              
              </div>
            
            <div class="col-sm-6">
            <h1 style="color:#009367">It's <strong>Sharper</strong>. </h1>
            <p> SurGuard3 is 20% sharper than the market leader, and the sharpest on average among major brands. Patients get a more comfortable injection, and clinicians get the peace of mind of a simple injection process. </p>
            
            <h1 style="color:#009367">It's <strong>Safer</strong>.</h1>
            <p> SurGuard3's safety mechanism includes a lock for both the needle and the hub. It's designed to be difficult to override, so clinicians spend less time worrying about needle stick injuries and more time focusing on patients. </p>
            
             <h1 style="color:#009367">It's <strong>Smarter</strong>.</h1>
            <p> Clinicians can activate the safety mechanism using their finger, their thumb or a hard surface. Whatever the preference or whatever the situation calls for, there's a method to meet every need. </p>
            
            <h1 style="color:#009367">And another great point? <br/>
<strong>The price point.</strong></h1>
            <p> Clinicians can activate the safety mechanism using their finger, their thumb or a hard surface. Whatever the preference or whatever the situation calls for, there's a method to meet every need. </p>
            
           
            
            </div>
                </div>
                
			</div>
			<footer class="footer">
				<div class="container">
					<p class="text-muted">TERUMO, and SurGuard are trademarks owned by Terumo Corporation, Copyright 2017.</p>
				</div>
			</footer>

    <!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
        <script type="text/javascript">
            $("#terumo").validate();
        </script>

</body>
</html>
