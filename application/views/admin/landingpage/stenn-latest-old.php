<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"><head>
    
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>Unlocking Successful Supply Chains in Emerging Markets</title>
    
    <!-- Styles -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />
<link href="http://www.martechb2b.com/email-template/HTML/SSM/css/styles.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
  <style type="text/css">
            input[type=submit] {
          background:url(http://www.martechb2b.com/email-template/HTML/SSM/images/read-more.png);
          color: transparent;
          width: 250px;
          height: 43px;
		  border:0px;
        }
        .error{
            color: red;
            font-weight: normal;
        }
        </style>
  

</head>
<body>

    
<!-- Fixed navbar -->
<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container">
		<div class="navbar-header">
			
			<a class="navbar-brand">
				<img src="http://www.martechb2b.com/email-template/HTML/SSM/images/logo-small.png" alt="" />
			</a>
		</div>
		<!-- the actual nav items -->
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav"></ul>
		</div>
		<!--/.nav-collapse -->
	</div>
</nav>
<!-- main content -->
<div class="container">
	<br />
	<div class="row">
		<!-- main column -->
		<div class="col-sm-8">
                <div class="form-page-content">
                  <h1>Unlocking Successful Supply Chains in Emerging Markets</h1>
                
             <div class="asset-image"><img src="http://www.martechb2b.com/email-template/HTML/SSM/images/STENN_Unlocking_Successful_Supply_Chains_UK.png" /></div>
                <p>	Enterprise retailers, wholesalers and manufacturers have an unprecedented opportunity. By unlocking new supplier relationships within emerging markets they can lower their total Cost of Goods Sold, while improving their cash flow. </p>
                
                <h3>How to source higher quality suppliers that are cheaper, more flexible, and give better terms </h3>
                
                <p>Cost and quality will always be at the forefront of concerns for retailers, wholesalers, and manufacturers. If these businesses can gain access to emerging markets to open up the supply chain with the range of suppliers that they can choose from, then sourcing and procurement executives can drastically reduce their Cost of Goods Sold. This is while still maintaining, or even increasing, the high level of goods that they receive.  </p>
                <p>Independent international trade finance providers can supply risk-free, no-cost financing options to open up new supplier relationships in emerging markets. This is done by using an international non-bank trade finance provider, such as STENN, who can pay the supplier 
and offer delayed payment terms to the buyer, so that both sides get the financial outcome that they want. </p>

<p>Independent international receivable finance companies such as these are getting a great deal of support in emerging markets such as Vietnam
 and are helping to add to its status as one of the world's fastest growing economies.  </p>

<p>	This guide will show you how you can build a relationship without having to go through cumbersome banking systems or other costly third-party financial solutions such as trading houses. Read on to learn how you can build successful and profitable connections around the globe at no cost to your company.</p>

<br />                


                </div>
            </div>

				<!-- right column -->
				<div class="col-sm-4">
					<div class="panel panel-primary reg-form">
						<div class="dnfrmc full" id="dnfrmmask">
                        <form class="autoform" id="stenn" method="Post" name="stenn">
                         <input type="hidden" name="form-type" value="stenn-download">
							<div class="reg-form-panel">
								<div id="leadform-error"></div>
								<p class="intro">Please fill in the information below to access this resource:</p>
								<div class="row">
									<div class="" style="padding-left: 10px; padding-right: 10px;">
										<div class="col-sm-6"> 
										<div class="form-group" id="question1-group">
											<label class="sr-only" for="question1">First Name</label>
											<input type="text" class="form-control input-sm lfreq tblfreq" id="f_name" name="f_name" maxlength="50"  placeholder="First Name" required />
										</div>
                                        </div>
                                        <div class="col-sm-6">
										<div class="form-group " id="question2-group">
											<label class="sr-only" for="question2">Last Name</label>
											<input type="text" class="form-control input-sm lfreq tblfreq" id="l_name" name="l_name" maxlength="50"  placeholder="Last Name" required/>
										</div>
                                        </div>
                                        <div class="col-sm-12">
										<div class="form-group" id="question8-group">
											<label class="sr-only" for="question8">Email Address</label>
											<input type="text" class="form-control input-sm lfreq tblfreq email" id="email" name="email" maxlength="100"  placeholder="Email Address" required/>
										</div>
                                        </div>
                                        <div class="col-sm-12">
										<div class="form-group" id="question9-group">
											<label class="sr-only" for="question9">Telephone</label>
											<input type="text" class="form-control input-sm lfreq tblfreq tel" id="phone" name="phone" maxlength="50"  placeholder="Telephone" required/>
										</div>
                                        </div>
                                        <div class="col-sm-12">
										<div class="form-group" id="question3-group">
											<label class="sr-only" for="question3">Company Name</label>
											<input type="text" class="form-control input-sm lfreq tblfreq" id="company" name="company" maxlength="80"  placeholder="Company Name" required/>
										</div>
                                        </div>
                                        <div class="col-sm-12">
										<div class="form-group" id="question3-group">
											<label class="sr-only" for="question3">Company Address</label>
											<input type="text" class="form-control input-sm lfreq tblfreq" id="company_address" name="company_address" maxlength="80"  placeholder="Company Address" required/>
										</div>
                                        </div>
                                     <div class="col-sm-6">   
										<div class="form-group" id="question110-group">
											<label class="sr-only" for="question110">Company Size</label>
											<select class="form-control input-sm lfreq " id="company_size" required="required" name="company_size" placeholder="Company Size">
												<option value="">Employee Size</option>
												<option value="0-50">0-50</option>
												<option value="51-100">51-100</option>
												<option value="101-250">101-250</option>
												<option value="251-500">251-500</option>
												<option value="501-1000">501-1000</option>
												<option value="1001-5000">1001-5000</option>
												<option value="5001-10000">5001-10000</option>
												<option value="10000+">10000+</option>
											</select>
										</div>
                                        </div>
                                        
                                        <div class="col-sm-6">   
										<div class="form-group" id="question110-group">
											<label class="sr-only" for="question110">Company Size</label>
											<select class="form-control input-sm lfreq" required="required" id="department" name="department" placeholder="Department">												<option value="">Department</option>
												<option value="Security">Security</option>
												<option value="IT">IT</option>
												<option value="Development">Development</option>
												<option value="Compliance">Compliance</option>
												<option value="Legal">Legal</option>
												<option value="Finance">Finance</option>
												<option value="Sales">Sales</option>
												<option value="Marketing">Marketing</option>
												<option value="General">General</option>
											</select>
										</div>
                                        </div>
                                        <div class="col-sm-12">  
										<div class="form-group" id="question141-group">
											<label class="sr-only" for="question141">Job Function</label>
											<select class="form-control input-sm lfreq" required="required" id="job_title_level" name="job_title_level" placeholder="Job Function">
												<option value="">Select Job Title</option>
												<option value=" Manager Finance"> Manager Finance</option>
											<option value="Director Finance">Director Finance</option>
											<option value="VP Finance">VP Finance</option>
											<option value="Head Finance">Head Finance</option>
											<option value="CFO">CFO</option>
											<option value="Head of Treasury">Head of Treasury</option>
											<option value="Supply Chain Manager">Supply Chain Manager</option>
											<option value="Supply Chain Director">Supply Chain Director</option>
											<option value="VP Supply Chain">VP Supply Chain</option>
											<option value="Sourcing Manager">Sourcing Manager</option>
											<option value="Director Sourcing">Director Sourcing</option>
											<option value="VP Sourcing">VP Sourcing</option>
											<option value="Global Sourcing Director">Global Sourcing Director</option>
											<option value="Senior Buyer">Senior Buyer</option>
											<option value="Head of Purchasing">Head of Purchasing</option>
											<option value="Purchasing Director">Purchasing Director</option>
											<option value="Senior Supply Chain Manager">Senior Supply Chain Manager</option>
											<option value="Head of Sourcing">Head of Sourcing</option>
											<option value="Head of Global Sourcing">Head of Global Sourcing</option>
											
											</select>
										</div>
                                        </div>
										
                                        <div class="col-sm-6">   
										<div class="form-group" id="question110-group">
											<label class="sr-only" for="question110">state</label>
											<select class="form-control input-sm lfreq" required="required" id="state" name="state" placeholder="state">
												<option value="">State</option>
												<option value="AL">Alabama</option>
												<option value="AK">Alaska</option>
												<option value="AZ">Arizona</option>
												<option value="AR">Arkansas</option>
												<option value="CA">California</option>
												<option value="CO">Colorado</option>
												<option value="CT">Connecticut</option>
												<option value="DE">Delaware</option>
												<option value="DC">District of Columbia</option>
												<option value="FL">Florida</option>
												<option value="GA">Georgia</option>
												<option value="HI">Hawaii</option>
												<option value="ID">Idaho</option>
												<option value="IL">Illinois</option>
												<option value="IN">Indiana</option>
												<option value="IA">Iowa</option>
												<option value="KS">Kansas</option>
												<option value="KY">Kentucky</option>
												<option value="LA">Louisiana</option>
												<option value="ME">Maine</option>
												<option value="MD">Maryland</option>
												<option value="MA">Massachusetts</option>
												<option value="MI">Michigan</option>
												<option value="MN">Minnesota</option>
												<option value="MS">Mississippi</option>
												<option value="MO">Missouri</option>
												<option value="MT">Montana</option>
												<option value="NE">Nebraska</option>
												<option value="NV">Nevada</option>
												<option value="NH">New Hampshire</option>
												<option value="NJ">New Jersey</option>
												<option value="NM">New Mexico</option>
												<option value="NY">New York</option>
												<option value="NC">North Carolina</option>
												<option value="ND">North Dakota</option>
												<option value="OH">Ohio</option>
												<option value="OK">Oklahoma</option>
												<option value="OR">Oregon</option>
												<option value="PA">Pennsylvania</option>
												<option value="PR">Puerto Rico</option>
												<option value="RI">Rhode Island</option>
												<option value="SC">South Carolina</option>
												<option value="SD">South Dakota</option>
												<option value="TN">Tennessee</option>
												<option value="TX">Texas</option>
												<option value="UT">Utah</option>
												<option value="VT">Vermont</option>
												<option value="VA">Virginia</option>
												<option value="WA">Washington</option>
												<option value="WV">West Virginia</option>
												<option value="WI">Wisconsin</option>
												<option value="WY">Wyoming</option>
											</select>
										</div>
                                        </div>
                                        
                                        <div class="col-sm-6">   
										<div class="form-group" id="question110-group">
											<label class="sr-only" for="question110">Country</label>
											<select class="form-control input-sm lfreq" required="required" id="country" name="country" placeholder="country">												<option value="">Country</option>
												<option value="AF">Afghanistan</option>
												<option value="AL">Albania</option>
												<option value="DZ">Algeria</option>
												<option value="AD">Andorra</option>
												<option value="AO">Angola</option>
												<option value="AI">Anguilla</option>
												<option value="AQ">Antarctica</option>
												<option value="AG">Antigua & Barbuda</option>
												<option value="AR">Argentina</option>
												<option value="AM">Armenia</option>
												<option value="AW">Aruba</option>
												<option value="AU">Australia</option>
												<option value="AT">Austria</option>
												<option value="AZ">Azerbaijan</option>
												<option value="BS">Bahamas</option>
												<option value="BH">Bahrain</option>
												<option value="BD">Bangladesh</option>
												<option value="BB">Barbados</option>
												<option value="BY">Belarus</option>
												<option value="BE">Belgium</option>
												<option value="BZ">Belize</option>
												<option value="BJ">Benin</option>
												<option value="BM">Bermuda</option>
												<option value="BT">Bhutan</option>
												<option value="BO">Bolivia</option>
												<option value="BA">Bosnia-Herzegovina</option>
												<option value="BW">Botswana</option>
												<option value="BV">Bouvet Island</option>
												<option value="BR">Brazil</option>
												<option value="VG">British Virgin Islands</option>
												<option value="BN">Brunei</option>
												<option value="BG">Bulgaria</option>
												<option value="BF">Burkina Faso</option>
												<option value="BI">Burundi</option>
												<option value="KH">Cambodia</option>
												<option value="CM">Cameroon</option>
												<option value="CA">Canada</option>
												<option value="CV">Cape Verde</option>
												<option value="KY">Cayman Islands</option>
												<option value="CF">Central African Republic</option>
												<option value="TD">Chad</option>
												<option value="CL">Chile</option>
												<option value="CN">China</option>
												<option value="CO">Colombia</option>
												<option value="KM">Comoros</option>
												<option value="CG">Congo</option>
												<option value="CK">Cook Islands</option>
												<option value="CR">Costa Rica</option>
												<option value="CI">Cote D'ivoire</option>
												<option value="HR">Croatia</option>
												<option value="CU">Cuba</option>
												<option value="CY">Cyprus</option>
												<option value="CZ">Czech Republic</option>
												<option value="DK">Denmark</option>
												<option value="DJ">Djibouti</option>
												<option value="DM">Dominica</option>
												<option value="DO">Dominican Republic</option>
												<option value="TP">East Timor</option>
												<option value="EC">Ecuador</option>
												<option value="EG">Egypt</option>
												<option value="SV">El Salvador</option>
												<option value="GQ">Equatorial Guinea</option>
												<option value="ER">Eritrea</option>
												<option value="EE">Estonia</option>
												<option value="ET">Ethiopia</option>
												<option value="FK">Falkland Islands</option>
												<option value="FO">Faroe Islands</option>
												<option value="FJ">Fiji</option>
												<option value="FI">Finland</option>
												<option value="FR">France</option>
												<option value="GF">French Guiana</option>
												<option value="PF">French Polynesia</option>
												<option value="GA">Gabon</option>
												<option value="GM">Gambia</option>
												<option value="GE">Georgia</option>
												<option value="DE">Germany</option>
												<option value="GH">Ghana</option>
												<option value="GI">Gibraltar</option>
												<option value="GR">Greece</option>
												<option value="GL">Greenland</option>
												<option value="GD">Grenada</option>
												<option value="GP">Guadeloupe</option>
												<option value="GT">Guatemala</option>
												<option value="GN">Guinea</option>
												<option value="GW">Guinea-Bissau</option>
												<option value="GY">Guyana</option>
												<option value="HT">Haiti</option>
												<option value="HN">Honduras</option>
												<option value="HK">Hong Kong</option>
												<option value="HU">Hungary</option>
												<option value="IS">Iceland</option>
												<option value="IN">India</option>
												<option value="ID">Indonesia</option>
												<option value="IQ">Iraq</option>
												<option value="IE">Ireland</option>
												<option value="IR">Islamic Republic of Iran</option>
												<option value="IL">Israel</option>
												<option value="IT">Italy</option>
												<option value="JM">Jamaica</option>
												<option value="JP">Japan</option>
												<option value="JO">Jordan</option>
												<option value="KZ">Kazakhstan</option>
												<option value="KE">Kenya</option>
												<option value="KI">Kiribati</option>
												<option value="KP">Korea, DPRK</option>
												<option value="KR">Korea, ROK</option>
												<option value="KW">Kuwait</option>
												<option value="KG">Kyrgyzstan</option>
												<option value="LA">Laos</option>
												<option value="LV">Latvia</option>
												<option value="LB">Lebanon</option>
												<option value="LS">Lesotho</option>
												<option value="LR">Liberia</option>
												<option value="LY">Libya</option>
												<option value="LI">Liechtenstein</option>
												<option value="LT">Lithuania</option>
												<option value="LU">Luxembourg</option>
												<option value="MO">Macau</option>
												<option value="MG">Madagascar</option>
												<option value="MW">Malawi</option>
												<option value="MY">Malaysia</option>
												<option value="MV">Maldives</option>
												<option value="ML">Mali</option>
												<option value="MT">Malta</option>
												<option value="MQ">Martinique</option>
												<option value="MR">Mauritania</option>
												<option value="MU">Mauritius</option>
												<option value="YT">Mayotte</option>
												<option value="MX">Mexico</option>
												<option value="MD">Moldova</option>
												<option value="MC">Monaco</option>
												<option value="MN">Mongolia</option>
												<option value="MS">Monserrat</option>
												<option value="MA">Morocco</option>
												<option value="MZ">Mozambique</option>
												<option value="MM">Myanmar (Burma)</option>
												<option value="NA">Nambia</option>
												<option value="NR">Nauru</option>
												<option value="NP">Nepal</option>
												<option value="NL">Netherlands</option>
												<option value="AN">Netherlands Antilles</option>
												<option value="NC">New Caledonia</option>
												<option value="NZ">New Zealand</option>
												<option value="NI">Nicaragua</option>
												<option value="NE">Niger</option>
												<option value="NG">Nigeria</option>
												<option value="NU">Niue</option>
												<option value="NF">Norfolk Island</option>
												<option value="NO">Norway</option>
												<option value="OM">Oman</option>
												<option value="PK">Pakistan</option>
												<option value="PA">Panama</option>
												<option value="PG">Papua New Guinea</option>
												<option value="PY">Paraguay</option>
												<option value="PE">Peru</option>
												<option value="PH">Philippines</option>
												<option value="PN">Pitcairn</option>
												<option value="PL">Poland</option>
												<option value="PT">Portugal</option>
												<option value="QA">Qatar</option>
												<option value="RE">Reunion</option>
												<option value="RO">Romania</option>
												<option value="RU">Russian Federation</option>
												<option value="RW">Rwanda</option>
												<option value="LC">Saint Lucia</option>
												<option value="WS">Samoa</option>
												<option value="SM">San Marino</option>
												<option value="ST">Sao Tome & Principe</option>
												<option value="SA">Saudi Arabia</option>
												<option value="SN">Senegal</option>
												<option value="SC">Seychelles</option>
												<option value="SL">Sierra Leone</option>
												<option value="SG">Singapore</option>
												<option value="SK">Slovakia</option>
												<option value="SI">Slovenia</option>
												<option value="SB">Solomon Islands</option>
												<option value="SO">Somalia</option>
												<option value="ZA">South Africa</option>
												<option value="ES">Spain</option>
												<option value="LK">Sri Lanka</option>
												<option value="SH">St. Helena</option>
												<option value="PM">St. Pierre & Miquelon</option>
												<option value="SD">Sudan</option>
												<option value="SR">Suriname</option>
												<option value="SZ">Swaziland</option>
												<option value="SE">Sweden</option>
												<option value="CH">Switzerland</option>
												<option value="SY">Syria</option>
												<option value="TW">Taiwan</option>
												<option value="TJ">Tajikistan</option>
												<option value="TZ">Tanzania</option>
												<option value="TH">Thailand</option>
												<option value="TG">Togo</option>
												<option value="TK">Tokelau</option>
												<option value="TO">Tonga</option>
												<option value="TT">Trinidad & Tobago</option>
												<option value="TN">Tunisia</option>
												<option value="TR">Turkey</option>
												<option value="TM">Turkmenistan</option>
												<option value="TV">Tuvalu</option>
												<option value="UM">U.S. Pacific Islands</option>
												<option value="UG">Uganda</option>
												<option value="UA">Ukraine</option>
												<option value="AE">United Arab Emirates</option>
												<option value="GB">United Kingdom</option>
												<option value="US">United States</option>
												<option value="UY">Uruguay</option>
												<option value="UZ">Uzbekistan</option>
												<option value="VU">Vanuatu</option>
												<option value="VA">Vatican City (Holy See)</option>
												<option value="VE">Venezuela</option>
												<option value="VN">Vietnam</option>
												<option value="EH">Western Sahara</option>
												<option value="YE">Yemen</option>
												<option value="YU">Yugoslavia</option>
												<option value="ZR">Zaire</option>
												<option value="ZM">Zambia</option>
												<option value="ZW">Zimbabwe</option>
											</select>
										</div>
                                        </div>
                                        <div class="col-sm-6"> 
										<div class="form-group" id="question1-group">
											<label class="sr-only" for="question1">City</label>
											<input type="text" class="form-control input-sm lfreq tblfreq" id="city" name="city" maxlength="50"  placeholder="City" required />
										</div>
                                        </div>
                                        <div class="col-sm-6">
										<div class="form-group " id="question2-group">
											<label class="sr-only" for="question2">Postal Code</label>
											<input type="text" class="form-control input-sm lfreq tblfreq" id="zipcode" name="zipcode" maxlength="50"  placeholder="Postal Code" required/>
										</div>
                                        </div>
                                        <div class="col-sm-12">
										<div class="form-group" id="question7-group">
											<label for="question7" style="font-weight:normal;">Where does your organization source parts or products from?</label>
											<select class="form-control input-sm lfreq" required="required" id="Custom_1" name="Custom_1" placeholder="Department">															 												<option value=""> Select</option>
											<option value="Asia">Asia</option>
											<option value="Africa">Africa</option>
											<option value="Latin America">Latin America</option>
											<option value="Other">Other</option>
											</select>
										</div>
                                        </div>
                                        
                                        <div class="col-sm-12">
										<div class="form-group" id="question7-group">
											<label for="question7" style="font-weight:normal;">What is your role in the decision making process?</label>
											<select class="form-control input-sm lfreq" required="required" id="Custom_2" name="Custom_2" placeholder="Department">															 												<option value=""> Select</option>
											<option value="Decision Maker">Decision Maker</option>
											<option value="Part of a team that makes decisions">Part of a team that makes decisions</option>
											<option value="Other">Other</option>
											</select>
										</div>
                                        </div>
                                        
                                        
                                        
                                        <div class="col-sm-12">
										<div class="form-group" id="question7-group">
											<label for="question7" style="font-weight:normal;">Would you be interested in evaluating supply chain finance providers in the next 6 months?</label>
											<div class="inline-group">
										<label class="radio-inline">
											<input type="radio" name="quest3" value="Yes" />Yes
										</label>
										<label class="radio-inline">
											<input type="radio" name="quest3" value="No" />No
										</label>
									</div>
										</div>
                                        </div>
                                        
                                        <div class="col-sm-12">
										<div class="form-group" id="question7-group">
											<label for="question7" style="font-weight:normal;">Would you be open to receive a call back in the next 7-10 days?</label>
											<select class="form-control input-sm lfreq" required="required" id="Custom_3" name="Custom_3" placeholder="Department">					<option value=""> Select</option>
											<option value="Yes">Yes</option>
											<option value="No">No</option>
											
											</select>
										</div>
                                        </div>
                                        
										<p class="buttons text-center">
                                        
                                        <input type="submit">
											<!--<a href="download-page.html">
												<img src="images/form-button-submit.jpg" width="135" height="36">
												</a>-->
							        </p>
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
                                </form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<footer class="footer">
				<div class="container">
					<p class="text-muted">© Copyright 2017, Stenn, All rights reserved.</p>
				</div>
			</footer>

    <!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
        <script type="text/javascript">
            $("#stenn").validate();
        </script>
</body>
</html>
