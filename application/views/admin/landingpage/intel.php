<html>
<head>
	<title>Virtualizing the Evolved Packet Core</title>
    

	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script src="https://code.jquery.com/jquery-3.2.1.min.js" ></script>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" >

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

	<link rel="stylesheet" href="http://martechb2b.com/email-template/HTML/intel/css/jquery-ui.css">
	<link rel="stylesheet" href="http://martechb2b.com/email-template/HTML/intel/css/style.css">
    
    <style type="text/css">
            input[type=submit] {
          background:url(http://martechb2b.com/email-template/HTML/intel/images/download-now.png);
          color: transparent;
          width: 308px;
          height: 43px;
        }
        .error{
            color: red;
            font-weight: normal;
        }
        </style>
    
</head>
<body>
	<div class="container-fluid logo">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-xs-12 left">
					<img src="http://martechb2b.com/email-template/HTML/intel/images/intel_PNG22.png">
			  </div>
				<div class="col-lg-6 col-xs-12 right">

				</div>
			</div>
			
		</div>
	</div>

	<div class="container-fluid title">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<h1 style="color:#000">Virtualizing the Evolved Packet Core</h1>
				</div>
			</div>
		</div>
	</div>

	<div class="container-fluid content">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-xs-12">
					
					<p>Virtualizing the evolved packed core is among the top three ways to reduce capital expenses through network functions virtualization according to a recent survey by Infonetics Research</p>
                    <img src="http://martechb2b.com/email-template/HTML/intel/images/intel-img.jpg" style="float: right; margin-left: 10px; margin-bottom: 10px; border: 1px solid black;" />
		      <p style="font-weight:bold;">Scale up and support rapidly rising 4G LTE traffic:</p>
              
	<ul>              
    <li> Implementing a Virtualized EPC </li>
    <li> Achieving Cost-Effective Scaling with vEPC </li>
    <li> Building vEPC Solutions with Intel Technologies </li>
    </ul>
    
    <p>Download Now - Learn how to move to a vEPC solution with a standard high-volume server. </p>
					<div style="clear: right;"></div>
				</div>
				<div class="col-lg-4 col-xs-12">
					<!--  -->
					<div class="form">
						<form class="autoform" id="intel" method="Post" name="intel">
                         <input type="hidden" name="form-type" value="sap-seven-key-download">
							<div class="reg-form-panel">
								
								<h2> Register to learn more.</h2>
								<div class="row">
									<div class="col-sm-12" style="padding-left: 10px; padding-right: 10px;">
										<div class="form-group" id="question1-group">
											<label class="sr-only" for="question1">First Name</label>
											<input type="text" class="form-control input-sm lfreq tblfreq" id="f_name" name="f_name" maxlength="50"  placeholder="First Name" required />
										</div>
										<div class="form-group" id="question2-group">
											<label class="sr-only" for="question2">Last Name</label>
											<input type="text" class="form-control input-sm lfreq tblfreq" id="l_name" name="l_name" maxlength="50"  placeholder="Last Name" required/>
										</div>
                                        <div class="form-group" id="question8-group">
											<label class="sr-only" for="question8">Business Email</label>
											<input type="text" class="form-control input-sm lfreq tblfreq email" id="email" name="email" maxlength="100"  placeholder="Email Address" required/>
										</div>
                                                                           
                                        <div class="form-group" id="question3-group">
											<label class="sr-only" for="question3">Company Name</label>
											<input type="text" class="form-control input-sm lfreq tblfreq" id="company" name="company" maxlength="80"  placeholder="Company Name" required/>
										</div>
                                        
                                        
                                              <div class="form-group" id="question7-group">
											<label class="sr-only" for="question7">Address</label>
											<input type="text" class="form-control input-sm lfreq tblfreq zip" id="address" name="address" maxlength="100"  placeholder="Address" value="" required/>
										</div>
                                        
										
										<div class="form-group" id="question9-group">
											<label class="sr-only" for="question9">City</label>
											<input type="text" class="form-control input-sm lfreq tblfreq tel" id="city" name="city" maxlength="50"  placeholder="City" required/>
										</div>
                                                                               
                                        <div class="form-group" id="question7-group">
											<label class="sr-only" for="question7">State</label>
											<input type="text" class="form-control input-sm lfreq tblfreq zip" id="state" name="state" maxlength="100"  placeholder="state" value="" required/>
										</div>
                                        
                                        <div class="form-group" id="question7-group">
											<label class="sr-only" for="question7">Country</label>
											<input type="text" class="form-control input-sm lfreq tblfreq zip" id="country" name="country" maxlength="100"  placeholder="Country" value="" required/>
										</div>
                                        
                                         <div class="form-group" id="question7-group">
											<label class="sr-only" for="question7">Zip</label>
											<input type="text" class="form-control input-sm lfreq tblfreq zip" id="zip" name="zip" maxlength="100"  placeholder="Zip" value="" required/>
										</div>
                                        
                                      <div class="form-group" id="question110-group">
											<label class="sr-only" for="question110">Company Size</label>
											<select class="form-control input-sm lfreq" required="required" id="company_size" name="company_size" placeholder="Company Size">
												<option value="">Company Size</option>
											<option value="0-50">0-50</option>
											<option value="51-100">51-100</option>
											<option value="101-250">101-250</option>
											<option value="251-500">251-500</option>
											<option value="501-1000">501-1000</option>
											<option value="1001-5000">1001-5000</option>
											<option value="5001-10000">5001-10000</option>
											<option value="10000+">10000+</option>
											</select>
										</div>
                                        
                                        <div class="form-group" id="question7-group">
											<label class="sr-only" for="question7">Job Title</label>
											<input type="text" class="form-control input-sm lfreq tblfreq zip" id="job_title" name="job_title" maxlength="100"  placeholder="Job Title" value="" required/>
										</div>
                                        
                                        <div class="form-group" id="question110-group">
											<label class="sr-only" for="question110">Job Function</label>
											<select class="form-control input-sm lfreq" required="required" id="job_function" name="job_function" placeholder="Company Size">
												<option value="">Job Function</option>
											<option value="Network Architect">Network Architect</option>
                                            <option value="Engineers">Engineers</option>
                                            <option value="Chief Technology Officer">Chief Technology Officer</option>
											</select>
										</div>
                                        
                                                                               
										<p class="buttons text-center">
                                        
                                        <input type="submit">
                                        
											<!--<a href="download-page.html">
												<img src="images/form-button-submit.jpg" width="135" height="36">
												</a>  -->      
							        </p>
                                    
                                    <p style="font-size:14px"> By signing up, you are confirming you are an adult 18 years or older and you agree to Intel & partners contacting you with marketing-related emails or by telephone with information about Intel & partner products, events, and updates. You may unsubscribe at any time. Intel's web sites and communications are subject to our <a href="https://www-ssl.intel.com/content/www/us/en/privacy/intel-privacy-notice.html" target="_blank">Privacy Notice</a> and <a href="https://www.intel.com/content/www/us/en/legal/terms-of-use.html" target="_blank">Terms of Use</a>. </p>
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
                                </form>
					</div>
				</div>
			</div>
		</div>
	</div>

 <!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
        <script type="text/javascript">
            $("#intel").validate();
        </script>

</body>
</html>