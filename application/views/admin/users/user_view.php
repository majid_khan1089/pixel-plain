<?php
    $oldKey = array('_');
    $newKey = array(' ');
?>
<table class="table table-striped">
    <tbody>
        <?php if(!empty($userDetails)) { ?>
            <?php foreach ($userDetails as $key => $value) { ?>
                <?php if($key != 'user_id' && $key != 'created_at') { ?>
                    <tr>
                        <td><strong><?php echo ucfirst(str_replace($oldKey,$newKey,$key));  ?></strong></td>
                        <td valign="center"><?php echo ($value) ? $value : '-';?></td>
                    </tr>
                <?php } ?>
            <?php } ?>
        <?php } else { ?>
        <tr>
            <td colspan="1" align="center"><strong>No Records Found</strong></td>
        </tr>
        <?php } ?>
    </tbody>
</table>