<?php $this->load->view('admin/common/header'); ?>
<div class="content-wrapper">
    <div class="container">
        <section class="content-header">
            <h1></h1>
            <br/>
            <ol class="breadcrumb">
                <li><a href="javascript:void(0);"><i class="fa fa-dashboard"></i> Home</a></li>
            </ol>
        </section>
        <section class="content">
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Users Listing</h3>
                    <div class="pull-right">
                        <!-- <a href="<?php //echo base_url('add-user'); ?>" class="btn btn-primary">Add User</a> -->
                        <a href="<?php echo base_url('upload-user'); ?>" class="btn btn-default">Upload User</a>
                        <a href="javascript:void(0);" class="btn btn-danger bulkDelete">Bulk Delete</a>
                        <a href="javascript:void(0);" class="btn btn-primary bulkMailToUser">Send Campaign Mail</a>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <?php $this->load->view('admin/common/flash_message'); ?>
                        </div>
                        <br/>         
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <table class="table table-striped table-bordered table-hover" id="customUserTable">
                                <thead>
                                    <tr>
                                        <th data-orderable="false"><input type="checkbox" id="select_all" class="checkAll"></th>
                                        <th><strong>Campaign Type</strong></th>
                                        <th>First Name</th>
                                         <th>Email</th>
                                        <!--<th>Mobile</th>
                                        <th>Status</th> -->
                                        <th><?php echo $this->lang->line('txt_form_action'); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php /* if(!empty($listings)) { ?>
                                    <?php $i=0; foreach($listings as $listing){?>
                                    <tr>
                                        <td><?php //echo ++$i;?>
                                            <input type="checkbox" class="checkbox" name="user_id_[<?php echo $listing->user_id; ?>]" value="<?php echo $listing->user_id; ?>">
                                        </td>
                                        <td><a href="javascript:void(0);" data-id="<?php echo $listing->user_id; ?>" class="showUserDetails"><strong><?php echo ($listing->first_name || $listing->last_name) ? ucfirst($listing->first_name) . ' ' . $listing->last_name: '-';?></strong></a></td>
                                        <td><?php echo ($listing->email) ? $listing->email : '-';?></td>
                                        <td><?php echo ($listing->mobile) ? ucwords($listing->mobile) : '-';?></td>
                                        <td>
                                            <?php if($listing->status) { ?>
                                            <span class="label label-success">Active</span>
                                            <?php } else { ?>
                                            <span class="label label-danger">Inactive</span>
                                            <?php } ?>
                                        </td>
                                        <td>
                                            <a href="<?php echo base_url('/edit/'.$listing->user_id. '/user'); ?>" class="btn btn-primary btn-xs">
                                                <i class="fa fa-pencil"></i>
                                            </a>
                                            <a onclick="confirm('Are you sure want to delete user?');" href="<?php echo base_url('/delete/'.$listing->user_id. '/user'); ?>" class="btn btn-danger btn-xs">
                                                <i class="fa fa-remove"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    <?php }?>
                                    <?php } */ ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<div class="modal fade" id="userModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title text-center text-info" id="myModalLabel">User Details</h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="sendMassMail" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title text-center text-info" id="myModalLabel">Campaign Details</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <form method="POST" id="sendBulkMail" class="sendBulkMail">
                            <div class="form-group">
                                <label for="selected_campaign">Campaign Type:</label>
                                <select name="selected_campaign" id="selected_campaign" class="form-control">
                                    <!-- <option value="">Select Campaign</option> -->
                                    <?php if(!empty($listings)) {
                                        foreach ($listings as $campaignKey => $campaignValue) { ?>
                                        <option value="<?php echo $campaignValue->campaign_slug; ?>"><?php echo $campaignValue->campaign_name; ?></option>
                                    <?php } } else {  ?>
                                        <option value="">No New Campaign Added Yet</option>
                                    <?php } ?>
                                </select><br/>
                                <p style="display: none"><strong>Campaign Type :</strong> <span id="campaignType"></span></p>
                                <p style="display: none"><strong>Email Subject :</strong> <span id="emailSubject"></span></p>
                            </div>                            
                        </form>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <input type="submit"  class="btn btn-success sendMailToUser" value="SUBMIT"/>
                <button type="button" class="btn btn-default resetCheckbox" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript"> 
var table; 
jQuery(document).ready(function($) { 
    //datatables
    table = $('#customUserTable').DataTable({ 
        "processing": true,
        "serverSide": true,
        "order": [],
        "ajax": {
            "url": "<?php echo site_url('user/getUserDatatable')?>",
            "type": "POST"
        }, 
        "columnDefs": [
            { 
                "targets": [ 0 ],
                "orderable": false,
            },
        ], 
    }); 
});
</script>
<?php $this->load->view('admin/common/footer'); ?>