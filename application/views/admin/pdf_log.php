<?php $this->load->view('admin/common/header'); ?>
<div class="content-wrapper">
   <div class="container">
      <section class="content-header">
         <h1></h1><br/>
         <ol class="breadcrumb">
            <li><a href="javascript:void(0);"><i class="fa fa-dashboard"></i> Home</a></li>
         </ol>
      </section>

      <section class="content">           
         <div class="box box-default">         

         <div class="box-body">
            <div class="row">
               <div class="col-md-12">
                  <?php $this->load->view('admin/common/flash_message'); ?>
               </div><br/>         
            </div>

            <div class="row">
               <div class="col-lg-12">
                  <table class="table table-striped table-bordered table-hover" id="customTable">
                     <thead>
                        <tr>
                           <th><?php echo $this->lang->line('txt_form_sno'); ?></th>
                           <th>Email Address</th>
                           <th>Date</th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php if(!empty($listings)) { ?>
                        <?php $i=0; foreach($listings as $listing){?>
                        <tr class="odd gradeX">
                           <td><?php echo ++$i;?></td>
                           <td><?php echo ($listing->email) ? $listing->email : '-';?></td>
                           <td><?php echo ($listing->date) ? $listing->date : '-';?></td>           
                        </tr>
                        <?php }?>
                        <?php } ?>                      
                     </tbody>
                  </table>
               </div>                  
            </div>
         </div>
      </div>
      </section>
   </div>
</div>
<?php $this->load->view('admin/common/footer'); ?>