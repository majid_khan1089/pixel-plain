<?php $this->load->view('admin/common/header'); ?>
<div class="content-wrapper">
   <div class="container">
      <section class="content-header">
         <h1></h1><br/>
         <ol class="breadcrumb">
            <li><a href="javascript:void(0);"><i class="fa fa-dashboard"></i> Home</a></li>
         </ol>
      </section>

      <section class="content">           
         <div class="box box-default">
            <div class="box-header with-border">
               <h3 class="box-title">Campaign Listing</h3>
               <a href="add_campaign" class="btn btn-primary pull-right">Add Campaign</a>
         </div>

         <div class="box-body">
            <div class="row">
               <div class="col-md-12">
                  <?php $this->load->view('admin/common/flash_message'); ?>
               </div><br/>         
            </div>

            <div class="row">
               <div class="col-lg-12">
                  <table class="table table-striped table-bordered table-hover" id="customTable">
                     <thead>
                        <tr>
                           <th><?php echo $this->lang->line('txt_form_sno'); ?></th>
                           <th><?php echo $this->lang->line('txt_form_campaign_name'); ?></th>
                           <th>Type</th>
                           <th>Mail Subject</th>
                           <th>Status</th>
                           <th><?php echo $this->lang->line('txt_form_action'); ?></th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php if(!empty($listings)) { ?>
                        <?php $i=0; foreach($listings as $listing){?>
                        <tr>
                           <td><?php echo ++$i;?></td>
                           <td><a href="javascript:void(0);" data-id="<?php echo $listing->campaign_id; ?>" class="showCampaign"><strong><?php echo ($listing->campaign_name) ? $listing->campaign_name : '-';?></strong></a></td>
                           <td><?php echo ($listing->campaign_type) ? strtoupper($listing->campaign_type) : '-';?></td>
                           <td><?php echo ($listing->mail_subject) ? ucwords($listing->mail_subject) : '-';?></td>
                           <td>
                              <?php if($listing->status) { ?>
                                 <span class="label label-success">Active</span>
                                 <?php } else { ?>
                                    <span class="label label-danger">Inactive</span>
                              <?php } ?></td>
                            <td>
                                <a href="<?php echo base_url('/edit-campaign/'.$listing->campaign_id); ?>" class="btn btn-primary btn-xs">
                                    <i class="fa fa-pencil"></i>
                                </a>
                                <a href="javascript:void(0);" data-id="<?php echo $listing->campaign_id; ?>" data-status="<?php echo $listing->status; ?>" class="btn <?php echo ($listing->status) ? 'btn-success' : 'btn-danger'; ?> btn-xs campaign-status">
                                    <?php if($listing->status) { ?>
                                        <i class="fa fa-unlock"></i>
                                    <?php } else { ?>
                                        <i class="fa fa-lock"></i>                                 
                                    <?php } ?>
                                </a>
                            </td>
                        </tr>
                        <?php }?>
                        <?php } ?>
                     </tbody>
                  </table>
               </div>      
            </div>
         </div>
      </div>
      </section>
   </div>
</div>

<div class="modal fade" id="campaignModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title text-center text-info" id="myModalLabel">Campaign Detail</h4>
      </div>
      <div class="modal-body"></div>
      <div class="modal-footer">
         <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<?php $this->load->view('admin/common/footer'); ?>