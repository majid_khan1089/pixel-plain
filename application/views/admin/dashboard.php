<?php $this->load->view('admin/common/header'); ?>
<div class="content-wrapper">
	<div class="container">
   	<section class="content-header">
     	<h1>DashBoard</h1>
      	<ol class="breadcrumb">
         	<li><a href="javascript:void(0);"><i class="fa fa-dashboard"></i> DashBoard Home</a></li>
         	<!-- <li><a href="#">Layout</a></li>
         	<li class="active">Top Navigation</li> -->
      	</ol>
   	</section>

   	<section class="content">
      	<div class="box box-default">
         	<div class="box-header with-border">
            	<h3 class="box-title">DashBoard Home</h3>
            </div>
            <div class="box-body">
               <div class="row">
                  <div class="col-sm-3 col-xs-6">
                     <div class="tile-stats tile-red">
                        <div class="icon"><i class="entypo-users"></i></div> 
                        <div data-delay="0" data-duration="1500" data-postfix="" data-end="83" data-start="0" class="num"><?php echo $registerUser; ?></div>
                        <h3>Registered users</h3>
                        <p>so far in our  website.</p> 
                     </div>
                  </div> 

                  <div class="col-sm-3 col-xs-6">
                     <div class="tile-stats tile-green">
                        <div class="icon"><i class="entypo-chart-bar"></i></div>
                        <div data-delay="600" data-duration="1500" data-postfix="" data-end="135" data-start="0" class="num"><?php echo $totalCampaign; ?></div>
                        <h3>Total Campaign</h3>
                        <p>this is the average value.</p>
                     </div>
                  </div>
                  <div class="clear visible-xs"></div>

                   <div class="col-sm-3 col-xs-6">
                     <div class="tile-stats tile-blue">
                        <div class="icon"><i class="entypo-rss"></i></div>
                        <div data-delay="1800" data-duration="1500" data-postfix="" data-end="52" data-start="0" class="num"><?php echo $totalEmail; ?></div>
                        <h3>Total Email's</h3>
                        <p>on our site right now.</p>
                     </div>
                  </div>

                  <div class="col-sm-3 col-xs-6">
                     <div class="tile-stats tile-aqua">
                        <div class="icon"><i class="entypo-mail"></i></div>
                        <div data-delay="1200" data-duration="1500" data-postfix="" data-end="23" data-start="0" class="num"><?php echo $totalEmailSend; ?></div>
                        <h3>Settings</h3>
                        <p>Admin Panel Setting.</p>
                     </div>
                  </div> 

                 
               </div>
            </div>
         </div>
   	</section>
	</div>
</div>
<?php $this->load->view('admin/common/footer'); ?>