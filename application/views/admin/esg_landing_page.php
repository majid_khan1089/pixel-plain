<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <script src="//cdn.optimizely.com/js/2871430788.js"></script>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Why You Should Add Cloud Storage to Your Data Protection Strategy</title>
        <!-- Styles -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
        <link href="http://bizmartech.com/HTML/ESG/css/styles.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <!-- Fixed navbar -->
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/"><img src="http://bizmartech.com/HTML/ESG/images/esg-logo-mobile-1.png" alt="IBM" /></a>
                </div>
                <!-- the actual nav items -->
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                    </ul>
                </div>
                <!--/.nav-collapse -->
            </div>
        </nav>
        <!-- main content -->
        <div class="container">
            <br />
            <div class="row">
                <!-- main column -->
                <div class="col-md-5">
                    <div class="form-page-content">
                        <input id="categoryid" name="categoryid" value="491" type="hidden">
                        <h1>Why You Should Add Cloud Storage to Your Data Protection Strategy</h1>
                        <h3>And How Google Can Help Make It Happen</h3>
                        <p>
                            If you’re fortifying your data protection environment, leveraging the cloud makes sense (and cents). But data
                            protection cloud implementations vary greatly. It’s wise to become familiar with the pros and cons of the various cloud-
                            inclusive protection approaches organizations are using today—including the intriguing Google Cloud Storage Platform.
                        </p>
                        <p>Data protection and the cloud intersect in many ways—not only as IaaS, but also as : </p>
                        <ol>
                            <li> Backup-as-a-service</li>
                            <li> Disaster recovery-as-a-servic</li>
                            <li> Storage-as-a-service (for data protection) </li>
                        </ol>
                        <p>Both D2C and D2D2C have merit. The main deciding factor between them usually centers on the organization’s recovery
                            requirements. Specifically, the service level agreements (SLAs) that IT departments maintain with internal business units
                            and external customers sometimes demand a faster, more agile recovery capability than what a pure-cloud (i.e., D2C)
                            arrangement can typically accommodate. The need to recover data locally tends to drive the “D2C versus D2D2C” decision. 
                        </p>
                        <br>
                    </div>
                </div>
                <!-- right column -->
                <div class="col-md-7">
                    <div class="panel panel-primary reg-form">
                        <div class="dnfrmc full" id="dnfrmmask">
                            <div class="reg-form-panel">
                            <form method="Post" id="esg-form">
                                                           
                                <div id="leadform-error"></div>
                                <p class="intro">Please fill in the information below to access this resource:</p>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group" id="question1-group">
                                            <label class="sr-only" for="question1">First Name</label>
                                            <input type="text" class="form-control input-sm lfreq tblfreq" id="f_name" name="f_name" maxlength="50"  placeholder="First Name" required />
                                        </div>
                                        <div class="form-group" id="question2-group">
                                            <label class="sr-only" for="question2">Last Name</label>
                                            <input type="text" class="form-control input-sm lfreq tblfreq" id="l_name" name="l_name" maxlength="50"  placeholder="Last Name" required/>
                                        </div>
                                        <div class="form-group" id="question8-group">
                                            <label class="sr-only" for="question8">Email Address</label>
                                            <input type="text" class="form-control input-sm lfreq tblfreq email" id="email" name="email" maxlength="100"  placeholder="Email Address" required/>
                                        </div>
                                        <div class="form-group" id="question9-group">
                                            <label class="sr-only" for="question9">Telephone</label>
                                            <input type="text" class="form-control input-sm lfreq tblfreq tel" id="phone" name="phone" maxlength="50"  placeholder="Telephone" required/>
                                        </div>
                                        <div class="form-group" id="question3-group">
                                            <label class="sr-only" for="question3">Company Name</label>
                                            <input type="text" class="form-control input-sm lfreq tblfreq" id="company" name="company" maxlength="80"  placeholder="Company Name" required/>
                                        </div>
                                        <div class="form-group" id="question110-group">
                                            <label class="sr-only" for="question110">Company Size</label>
                                            <select class="form-control input-sm lfreq " id="company_size" name="company_size" placeholder="Company Size">
                                                <option value="">Select Company Size</option>
                                                <option value="1 to 3">1 to 3</option>
                                                <option value="4 to 10">4 to 10</option>
                                                <option value="11 to 25">11 to 25</option>
                                                <option value="26 to 50">26 to 50</option>
                                                <option value="51 to 100">51 to 100</option>
                                                <option value="101 to 200">101 to 200</option>
                                                <option value="201 to 500">201 to 500</option>
                                                <option value="501 to 1000">501 to 1000</option>
                                                <option value="1001 or more">1001 or more</option>
                                            </select>
                                        </div>
                                        <div class="form-group" id="question141-group">
                                            <label class="sr-only" for="question141">Job Function</label>
                                            <select class="form-control input-sm lfreq " id="job_title_level" name="job_title_level" placeholder="Job Function">
                                                <option value="">Select Job Function</option>
                                                <option value="IT Management(Manager IS/IT)">IT Management(Manager IS/IT)</option>
                                                <option value="Senior Non IT Management(CEO,CFO,VP,Director)">Senior Non IT Management(CEO,CFO,VP,Director)</option>
                                                <option value="Senior IT Management(CIO/CTO/VP/Director)">Senior IT Management(CIO/CTO/VP/Director)</option>
                                                <option value="Network Management/Administration">Network Management/Administration</option>
                                                <option value="IT Staff">IT Staff</option>
                                                <option value="CSO">CSO</option>
                                                <option value="Product Management">Product Management</option>
                                                <option value="Sales/Marketing">Sales/Marketing</option>
                                                <option value="Telecommunications Manager">Telecommunications Manager</option>
                                                <option value="Systems Management/Administration">Systems Management/Administration</option>
                                                <option value="Project Manager">Project Manager</option>
                                                <option value="Programmer/Developer">Programmer/Developer</option>
                                                <option value="Privacy Officer">Privacy Officer</option>
                                                <option value="Consultant/Systems Integrator">Consultant/Systems Integrator</option>
                                                <option value="Database Administrator">Database Administrator</option>
                                                <option value="Educator">Educator</option>
                                                <option value="Non IT Staff">Non IT Staff</option>
                                                <option value="Architect">Architect</option>
                                                <option value="Application Manager">Application Manager</option>
                                                <option value="Analyst">Analyst</option>
                                            </select>
                                        </div>
                                        <div class="form-group" id="question113-group">
                                            <label class="sr-only" for="question113">Industry</label>
                                            <select class="form-control input-sm lfreq " id="industry" name="industry" placeholder="Industry">
                                                <option value="">Select Industry</option>
                                                <option value="Advertising/Marketing">Advertising/Marketing</option>
                                                <option value="Automotive">Automotive</option>
                                                <option value="Agriculture/Forestry">Agriculture/Forestry</option>
                                                <option value="Business Services">Business Services</option>
                                                <option value="Consulting">Consulting</option>
                                                <option value="Distribution">Distribution</option>
                                                <option value="Entertainment">Entertainment</option>
                                                <option value="Education">Education</option>
                                                <option value="Engineering/Construction">Engineering/Construction</option>
                                                <option value="Financial">Financial</option>
                                                <option value="Food and Beverage">Food and Beverage</option>
                                                <option value="Government-Federal">Government-Federal</option>
                                                <option value="Government-State and Local">Government-State and Local</option>
                                                <option value="Healthcare/Medical">Healthcare/Medical</option>
                                                <option value="Hospitality">Hospitality</option>
                                                <option value="Insurance">Insurance</option>
                                                <option value="Legal">Legal</option>
                                                <option value="Manufacturing">Manufacturing</option>
                                                <option value="Non-profit">Non-profit</option>
                                                <option value="Real-Estate">Real-Estate</option>
                                                <option value="Retail">Retail</option>
                                                <option value="Student/Retired/Non-Business">Student/Retired/Non-Business</option>
                                                <option value="Transportation/Shipping">Transportation/Shipping</option>
                                                <option value="Technology">Technology</option>
                                                <option value="Telecommunications">Telecommunications</option>
                                                <option value="Utilities">Utilities</option>
                                                <option value="Warehousing">Warehousing</option>
                                                <option value="Other">Other</option>
                                            </select>
                                        </div>
                                        <div class="form-group" id="question7-group">
                                            <label class="sr-only" for="question7">Zip Code</label>
                                            <input type="text" class="form-control input-sm lfreq tblfreq zip" id="zip" name="zip" maxlength="5"  placeholder="Zip Code" value=""/>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group" id="question110-group" style="padding-bottom:35px;">
                                            <label for="question110" style="font-weight:normal">Would you like to receive periodic emails on news, product updates, and specials from Google for Work, or our partners?</label>
                                            <select class="form-control input-sm lfreq " id="subscribe_offer" name="subscribe_offer" placeholder="Company Size">
                                                <option value="">Select </option>
                                                <option value="Yes">Yes</option>
                                                <option value="No">No</option>
                                            </select>
                                        </div>
                                        <div class="form-group" id="question110-group" style="padding-bottom:35px;">
                                            <label for="question110" style="font-weight:normal">I would like to talk to ​Google's Sales team</label>
                                            <select class="form-control input-sm lfreq " id="question110" name="question110" placeholder="Company Size">
                                                <option value="">Select </option>
                                                 <option value="Yes">Yes</option>
                                                <option value="No">No</option>
                                            </select>
                                        </div>
                                        <div class="form-group" id="question110-group" style="padding-bottom:35px;">
                                            <label for="question110" style="font-weight:normal">What is the volume of data your company maintains?</label>
                                            <select class="form-control input-sm lfreq " id="question110" name="question110" placeholder="Company Size">
                                                <option value="">Select </option>
                                                <option value="Less than 500TB">Less than 500TB</option>
                                                <option value="Between 500TB and 1PB">Between 500TB and 1PB</option>
                                                <option value="Between 1PB and 5PB">Between 1PB and 5PB</option>
                                                <option value="Between 5PB and 10PB">Between 5PB and 10PB</option>
                                                <option value="10PB">10PB</option>
                                            </select>
                                        </div>
                                        <div class="form-group" id="question110-group" style="padding-bottom:35px;">
                                            <label for="question110" style="font-weight:normal">Currently we are placed with - </label>
                                            <select class="form-control input-sm lfreq " id="question110" name="question110" placeholder="Company Size">
                                                <option value="">Select </option>
                                                <option> Veritas - Infoscale</option>
                                                <option> Veritas - Hyperscale</option>
                                                <option> Commvault- Purpose-built backup appliances (PBBAs) Hardware + software</option>
                                                <option> NetApp - All hardware/software</option>
                                                <option> Dell Powervault</option>
                                                <option> Symantec NetBackup</option>
                                                <option> HP Ultrium</option>
                                                <option> Tandberg LTO</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <p class="buttons text-center">
                                    <a href="javascript:void(0);" onclick="document.getElementById('esg-form').submit();">
                                        <img src="http://bizmartech.com/HTML/ESG/images/form-button-submit.jpg" width="135" height="36">
                                    </a>
                                </p>
                                <div class="clearfix"> </div>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer class="footer">
            <div class="container">
                <p class="text-muted">© Copyright 2016, ESG, All rights reserved.</p>
            </div>
        </footer>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    </body>
</html>
