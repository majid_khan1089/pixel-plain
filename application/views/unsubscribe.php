<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="skype : majid.khan1089">

    <title>MartechB2B</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo HTTP_CSS_PATH; ?>/bootstrap.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo HTTP_CSS_PATH; ?>/modern-business.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo base_url(); ?>assets/front-end/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <br/><br/><br/><br/>
    <div class="container">
        <div class="row">
           <div class="col-md-12">
              <?php $this->load->view('admin/common/flash_message'); ?>
           </div><br/>
        </div>

        <div class="row">
            <div class="span12">
                <div class="thumbnail center well well-small text-center">
                    <h1>Email Listing</h1>
                    <?php if(isset($_GET['e']) && $_GET['e']) { ?>
                    <p class="text-green">You have been successfully unsubscribed from email communications.If you did this in error, you may re-subscribe by clicking the button below.</p>
                    <?php } ?>
                    
                    <form method="post">
                        <div class="input-prepend"><span class="add-on"><i class="icon-envelope"></i></span>
                            <input type="text" id="" name="email" placeholder="your@email.com" class="" required="">
                            <input type="hidden" name="user_email" value="<?php echo (isset($_GET["e"]) && $_GET["e"]) ? base64_decode($_GET["e"]) : ''; ?>">
                        </div>
                        <br />
                        <input type="submit" value="Subscribe Now!" class="btn btn-large btn-info" />
                  </form>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo HTTP_JS_PATH;?>jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo HTTP_JS_PATH;?>bootstrap.min.js"></script>
</body>

</html>