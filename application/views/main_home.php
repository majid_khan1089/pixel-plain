<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Martechb2b</title>
<style>
.containerBox {
    position: relative;
    display: inline-block;
}
.text-box {
    position: absolute;    
    height: 16%;
    text-align: center;    
    width: 100%;
}
.text-box:before {
   content: '';
   display: inline-block;
   height: 100%;
   vertical-align: middle;
}
h3 {
   display: inline-block;
   font-size: 20px; /*or whatever you want*/
   color: #FF4500;   
}
img {
  display: block;
  max-width: 100%;
  height: auto;
  background-size:cover;
  background-size: 100%;
}

#background {
    width: 100%; 
    height: 100%; 
    position: fixed; 
    left: 0px; 
    top: 0px; 
    z-index: -1; /* Ensure div tag stays behind content; -999 might work, too. */
}
.stretch {
    width:100%;
    height:100%;
}
</style>
</head>

<body style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #adafb3;">
<div class="containerBox">
<div id="background">
    <img src="<?php echo  base_url('assets/front-end/images/comingsoon.jpg'); ?>" class="stretch" alt="" />
</div>
    <!-- <div class="text-box">
    <h3 class="blink_me">COMING SOON...!!</h3> 
    </div> -->
    <!-- <img class="img-responsive" src="<?php // echo  base_url('assets/front-end/images/coming_soon.jpg'); ?>"/> -->
</div>
    </body>
</html>