<!-- jQuery -->
    <script src="<?php echo HTTP_JS_PATH;?>jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo HTTP_JS_PATH;?>bootstrap.min.js"></script>

    <!-- Script to Activate the Carousel -->
    <script>
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    })
    </script>
    <script type="text/javascript">
    function validate(){
    	var email = $('#email').val();
    	$.ajax({
  					type: "POST",
  					url: "validate",
  					data:'email='+ email,
  					cache: false,
  					success: function(data){
     					if(data == 1){
     						$("#msg").html("<span class='text-success'>user Validated : Your download will start shortly</span>");
                		}
     					else{
     						$("#msg").html("<span class='text-danger'>Email Id not found. Please make sure you enter valid email address</span>");
     					}
     					
  					}
				});
    }
    </script>

</body>

</html>