
<div style="background-color:#FFFFFF">
<div class="container">
  <div class="row">
    <div class="col-md-8">
      <h3 style="color:#027dc3; font-weight:bold"><strong>5 Creative Ideas to Propel Your Webinars</strong> </h3>
      <div class="row">
      <div class="col-md-4">
        <img class="img-responsive" src="<?php echo HTTP_CAMPAIGN_IMAGES_PATH;?>5-creative246x316.jpg"></div>
      <div class="col-md-8">
     <p> Learn the key elements that go into producing, promoting and delivering successful webinars that deliver great results.</p>

<p> This white paper will share five out-there ideas for delivering truly memorable webinars, including:</p>
  <ul>
                    <li>How gamification can make complex data digestible and fun</li>
                <li>Simple ways to add engaging video to your webinars</li>
                <li>informal and interactive webinar formats</li>
          </ul>
      </div>
      </div>
      <div style="padding-top:20px;">
        <form method="POST" action="<?php echo base_url('register'); ?>">
            <input type="hidden" name="campId" value="<?php echo $campId?>">
            <input type="hidden" name="contentId" value="<?php echo $contentId?>">
            <input type="hidden" name="source" value="<?php echo $source?>">
            <input type="hidden" name="email" value="<?php echo $email?>">      
            <button type="submit" class="btn btn-warning">Read More</button>
        </form>
      </div>
       <h3 style="color:#027dc3; font-weight:bold"><strong>SPICE UP
YOUR WEBINARS </strong> </h3>
      <p>There's no limit to how creative you can get with your
webinars. As marketers, we want to deliver on the
expectation that we're going to give a great presentation.
We're asking people to sit still and stare at a fixed spot for up
to an hour. Sure, that audience is going to get some valuable
information, but why not make it fun?
By mixing up your webinar formats, crowdsourcing material,
leveraging gamification and using video in creative ways,
you can create new content, spice up old content and get
more value from your experts. Best of all, you'll have more
fun with your webinars and your audiences will, too.</p>
    </div>
    <div class="col-md-4">
      <div> 
      <img class="img-responsive"  src="<?php echo HTTP_CAMPAIGN_IMAGES_PATH;?>245.png"> 
     </div>
      <div>
       <h4 style="color:#027dc3; font-weight:bold"><strong>CHANGE THE FORMAT </strong> </h4>
        <p>We see it again and again: marketers relying on the same
you-talk-they-listen webinars every single time. There's
nothing wrong with audio and slides, but why not mix
it up once in a while? Less formal formats are a fun way
to engage your audience in a conversation, and they're
especially great for leveraging third-party speakers and
taking the pressure off subject matter experts who are less
comfortable behind a microphone.
 </p>
   <h4 style="color:#027dc3; font-weight:bold"><strong>THE CROWDSOURCED WEBINAR </strong> </h4>
        <p>"We need more content!" It's one of the constant
refrains of marketers everywhere. Why not turn to your
customers? They know what they want and expect;
all you have to do is ask. And if you do ask, it's easy to
crowdsource content themes, some webinar content or
even all the content for an entire webinar.

 </p>
 </div>
      
    </div>
  </div>
  
  <hr>
  
  <!-- Footer -->
  <footer>
    <div class="row">
      <div class="col-md-12">
        <div class="text-center">
        <img src="<?php echo HTTP_CAMPAIGN_IMAGES_PATH;?>on24-logo.png">
        <p> ON24, INC.
          201 THIRD STREET SAN FRANCISCO, CA 94103
          877.202.9599 ON24.COM </p>
          <p> © 2015 ON24, Inc. For more information on the benefits of the ON24 virtual environment, contact us at 877-202-9599 or visit www.on24.com. </p>
        </div>
      </div>
    </div>
  </footer>
</div>
</div>