<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!-- If you delete this tag, the sky will fall on your head -->
<meta name="viewport" content="width=device-width" />

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>What Every Facilities Manager Should Do Now Ebook</title>
	
<style type="text/css">
    @media only screen and (max-width: 480px){
        .emailImage{
            height:auto !important;
            max-width:600px !important;
            width: 100% !important;
        }
    }
	
	/* ------------------------------------- 
		GLOBAL 
------------------------------------- */
* { 
	margin:0;
	padding:0;
}
* { font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif; }

img { 
	max-width: 100%; 
}
.collapse {
	margin:0;
	padding:0;
}
body {
	-webkit-font-smoothing:antialiased; 
	-webkit-text-size-adjust:none; 
	width: 100%!important; 
	height: 100%;
	color:#000;
}


/* ------------------------------------- 
		ELEMENTS 
------------------------------------- */
a { color: #2BA6CB;}

.btn {
	text-decoration:none;
	color: #FFF;
	background-color: #666;
	padding:10px 16px;
	font-weight:bold;
	margin-right:10px;
	text-align:center;
	cursor:pointer;
	display: inline-block;
}

p.callout {
	padding:15px;
	background-color:#ECF8FF;
	margin-bottom: 15px;
}
.callout a {
	font-weight:bold;
	color: #2BA6CB;
}

table.social {
/* 	padding:15px; */
	background-color: #ebebeb;
	
}
.social .soc-btn {
	padding: 3px 7px;
	font-size:12px;
	margin-bottom:10px;
	text-decoration:none;
	color: #FFF;font-weight:bold;
	display:block;
	text-align:center;
}
a.fb { background-color: #3B5998!important; }
a.tw { background-color: #1daced!important; }
a.gp { background-color: #DB4A39!important; }
a.ms { background-color: #000!important; }

.sidebar .soc-btn { 
	display:block;
	width:100%;
}

/* ------------------------------------- 
		HEADER 
------------------------------------- */
table.head-wrap { width: 100%;}

.header.container table td.logo { padding: 15px; }
.header.container table td.label { padding: 15px; padding-left:0px;}


/* ------------------------------------- 
		BODY 
------------------------------------- */
table.body-wrap { width: 100%;}


/* ------------------------------------- 
		FOOTER 
------------------------------------- */
table.footer-wrap { width: 100%;	clear:both!important;
}
.footer-wrap .container td.content  p { border-top: 1px solid rgb(215,215,215); padding-top:15px;}
.footer-wrap .container td.content p {
	font-size:10px;
	font-weight: bold;
	
}


/* ------------------------------------- 
		TYPOGRAPHY 
------------------------------------- */
h1,h2,h3,h4,h5,h6 {
font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; line-height: 1.1; margin-bottom:5px; color:#000;
}
h1 small, h2 small, h3 small, h4 small, h5 small, h6 small { font-size: 60%; color: #6f6f6f; line-height: 0; text-transform: none; }

h1 { font-weight:200; font-size: 44px;}
h2 { font-weight:200; font-size: 30px;}
h3 { font-weight:500; font-size: 27px;}
h4 { font-weight:500; font-size: 23px;}
h5 { font-weight:900; font-size: 17px;}
h6 { font-weight:900; font-size: 14px; text-transform: uppercase; color:#444;}

.collapse { margin:0!important;}

p, ul { 
	margin-bottom: 5px; 
	font-weight: normal; 
	font-size:13px; 
	line-height:1.6;
}
p.lead { font-size:17px; }
p.last { margin-bottom:0px;}

ul li {
	margin-left:5px;
	list-style-position: inside;
}

/* ------------------------------------- 
		SIDEBAR 
------------------------------------- */
ul.sidebar {
	background:#ebebeb;
	display:block;
	list-style-type: none;
}
ul.sidebar li { display: block; margin:0;}
ul.sidebar li a {
	text-decoration:none;
	color: #666;
	padding:10px 16px;
/* 	font-weight:bold; */
	margin-right:10px;
/* 	text-align:center; */
	cursor:pointer;
	border-bottom: 1px solid #777777;
	border-top: 1px solid #FFFFFF;
	display:block;
	margin:0;
}
ul.sidebar li a.last { border-bottom-width:0px;}
ul.sidebar li a h1,ul.sidebar li a h2,ul.sidebar li a h3,ul.sidebar li a h4,ul.sidebar li a h5,ul.sidebar li a h6,ul.sidebar li a p { margin-bottom:0!important;}



/* --------------------------------------------------- 
		RESPONSIVENESS
		Nuke it from orbit. It's the only way to be sure. 
------------------------------------------------------ */

/* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
.container {
	display:block!important;
	max-width:600px!important;
	margin:0 auto!important; /* makes it centered */
	clear:both!important;
}

/* This should also be a block element, so that it will fill 100% of the .container */
.content {
	padding:5px;
	max-width:600px;
	margin:0 auto;
	display:block; 
}

/* Let's make sure tables in the content area are 100% wide */
.content table { width: 100%; }


/* Odds and ends */
.column {
	width: 300px;
	float:left;
}
.column tr td { padding: 5px; }
.column-wrap { 
	padding:0!important; 
	margin:0 auto; 
	max-width:600px!important;
}
.column table { width:100%;}
.social .column {
	width: 280px;
	min-width: 279px;
	float:left;
}

/* Be sure to place a .clear element after each set of columns, just to be safe */
.clear { display: block; clear: both; }

.ul_flush, .ol_flush {
  list-style: none;
  padding-left: 0;
  display: table;
}
.ul_flush li:after, .ol_flush li:after {
  content: "";
  display: block;
  margin-bottom: 0em;
}

.ul_flush li {
  display: table-row;
}
.ul_flush li:before {
  content: "•";
  display: table-cell;
  padding-right: 0.4em;
}

/* ------------------------------------------- 
		PHONE
		For clients that support media queries.
		Nothing fancy. 
-------------------------------------------- */
@media only screen and (max-width: 600px) {
	
	a[class="btn"] { display:block!important; margin-bottom:10px!important; background-image:none!important; margin-right:0!important;}

	div[class="column"] { width: auto!important; float:none!important;}
	
	table.social div[class="column"] {
		width:auto!important;
	}

}
</style>
</head>
 
<body bgcolor="#FFFFFF">

<!-- HEADER -->



<!-- BODY -->
<table class="body-wrap" style="display:block!important;width:610px!important;margin:0 auto!important; clear:both!important;">
	<tr>
		<td valign="top" bgcolor="#FFFFFF" class="container" style="border:#6abff0 3px solid; border-radius: 15px 15px 15px 15px;-moz-border-radius: 15px 15px 15px 15px;-webkit-border-radius: 15px 15px 15px 15px;">

			<div class="content">
            
            
			<table>
				<tr>
					<td style="padding-bottom:5px;" align="center">
                    <img src="http://www.martechb2b.com/email-template/HTML/service-channel/images/service-chennal-logo.png" /></td>
				</tr>
                             
                <tr>
					<td>
                    <p style="color:#666; font-weight:bold;">Dear Subscriber,</p>
                        <p style="color:#666; font-weight:bold;">We trust this email finds you well.</p>                              </td>
				</tr>
                <tr>
                  <td style="background-color:#6abff0; padding:5px; "> <div style="font-weight:bold; color:#000000; text-align:center; font-size:30px">What Every Facilities Manager Should Do Now Ebook </div></td>
                </tr>
                <tr>
                  <td style="text-align:center; font-size:16px; font-weight:bold">&nbsp;</td>
                </tr>
                <tr>
                  <td><table width="600" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td align="center" valign="top" style="width:300px;"><a href="<?php echo $link; ?>">
                      <img  style="border:0px;" src="http://www.martechb2b.com/email-template/HTML/service-channel/images/CTA_Ebook_CoverOnStack_WhatEveryFMShoulDo11.png" /> </a></td>
                      <td valign="top" style="width:300px;font-size:14px;">
                                      
                      <p style="font-size:16px;font-family:Arial, Helvetica, sans-serif; color: #000;line-height:1.6;">Facilities managers face specific challenges at different times of the year.</p>
                      <p style="font-size:16px;font-family:Arial, Helvetica, sans-serif;line-height:1.6;"> In this ebook, you'll see a number of scenarios you likely face from season to season - and the specific actions you can take now to improve your company - and your own - performance. .</p></td>
                    </tr>
                  </table>
                    <table width="600" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="20" valign="top" style="padding-top:5px;"><img src="http://www.martechb2b.com/email-template/HTML/service-channel/images/BulletPoint.png" width="15" height="15" /></td>
                        <td style="font-size:16px;font-family:Arial, Helvetica, sans-serif; color: #000;padding-bottom:10px;">Automate Seasonal Facilities Tasks using automated planned / scheduled maintenance and real-time monitoring</td>
                      </tr>
                      <tr>
                         <td width="20" valign="top" ><img src="http://www.martechb2b.com/email-template/HTML/service-channel/images/BulletPoint.png" width="15" height="15" /></td>
                        <td style="font-size:16px;font-family:Arial, Helvetica, sans-serif; color: #000;padding-bottom:10px;">Streamline Landscaping and Snow Removal with contractor-initiated work orders</td>
                      </tr>
                      <tr>
                         <td width="20" valign="top" style="padding-top:5px;"><img src="http://www.martechb2b.com/email-template/HTML/service-channel/images/BulletPoint.png" width="15" height="15" /></td>
                        <td style="font-size:16px;font-family:Arial, Helvetica, sans-serif; color: #000;padding-bottom:10px;">Minimize Facilities' Risk Profiles by conducting seasonal mobile-based site audits</td>
                      </tr>
                      <tr>
                         <td width="25" valign="top" style="padding-top:5px;"><img src="http://www.martechb2b.com/email-template/HTML/service-channel/images/BulletPoint.png" width="15" height="15" /></td>
                        <td style="font-size:16px;font-family:Arial, Helvetica, sans-serif; color: #000;padding-bottom:10px;">Conduct Regular Performance Measurements with Service Providers through data-based scorecarding</td>
                      </tr>
                      <tr>
                         <td width="25" valign="top" style="padding-top:3px;"><img src="http://www.martechb2b.com/email-template/HTML/service-channel/images/BulletPoint.png" width="15" height="15" /></td>
                        <td style="font-size:16px;font-family:Arial, Helvetica, sans-serif; color: #000;padding-bottom:10px;">Perform Smarter Equipment Budgeting with energy efficiency and repair/replace analysis</td>
                      </tr>
                  </table></td>
                </tr>
                <tr>
            	  <td><table width="600" border="0" cellspacing="0" cellpadding="0">
            	    <tr>
            	      <td style="font-size:12px; width:300px; text-align:center"><img src="http://www.martechb2b.com/email-template/HTML/service-channel/images/service-chennal-logo-small.png" /> <br/></td>
            	      <td align="center" style="width:300px;"><a href="<?php echo $link; ?>"><img style="border:0px;" src="http://www.martechb2b.com/email-template/HTML/service-channel/images/CTA-3.png"  /></a></td>
          	      </tr>
          	    </table></td>
          	  </tr>
			</table>
		  </div>
						
		</td>
		
	</tr>
</table><!-- /BODY -->

<!-- FOOTER -->
<!-- /FOOTER -->

</body>
</html>