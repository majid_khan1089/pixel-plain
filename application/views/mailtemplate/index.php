<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!-- If you delete this tag, the sky will fall on your head -->
<meta name="viewport" content="width=device-width" />

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Dell Boomi Email Template</title>
	
<style type="text/css">
    @media only screen and (max-width: 480px){
        .emailImage{
            height:auto !important;
            max-width:600px !important;
            width: 100% !important;
        }
    }
	
	/* ------------------------------------- 
		GLOBAL 
------------------------------------- */
* { 
	margin:0;
	padding:0;
}
* { font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif; }

img { 
	max-width: 100%; 
}
.collapse {
	margin:0;
	padding:0;
}
body {
	-webkit-font-smoothing:antialiased; 
	-webkit-text-size-adjust:none; 
	width: 100%!important; 
	height: 100%;
	color:#000;
}


/* ------------------------------------- 
		ELEMENTS 
------------------------------------- */
a { color: #2BA6CB;}

.btn {
	text-decoration:none;
	color: #FFF;
	background-color: #666;
	padding:10px 16px;
	font-weight:bold;
	margin-right:10px;
	text-align:center;
	cursor:pointer;
	display: inline-block;
}

p.callout {
	padding:15px;
	background-color:#ECF8FF;
	margin-bottom: 15px;
}
.callout a {
	font-weight:bold;
	color: #2BA6CB;
}

table.social {
/* 	padding:15px; */
	background-color: #ebebeb;
	
}
.social .soc-btn {
	padding: 3px 7px;
	font-size:12px;
	margin-bottom:10px;
	text-decoration:none;
	color: #FFF;font-weight:bold;
	display:block;
	text-align:center;
}
a.fb { background-color: #3B5998!important; }
a.tw { background-color: #1daced!important; }
a.gp { background-color: #DB4A39!important; }
a.ms { background-color: #000!important; }

.sidebar .soc-btn { 
	display:block;
	width:100%;
}

/* ------------------------------------- 
		HEADER 
------------------------------------- */
table.head-wrap { width: 100%;}

.header.container table td.logo { padding: 15px; }
.header.container table td.label { padding: 15px; padding-left:0px;}


/* ------------------------------------- 
		BODY 
------------------------------------- */
table.body-wrap { width: 100%;}


/* ------------------------------------- 
		FOOTER 
------------------------------------- */
table.footer-wrap { width: 100%;	clear:both!important;
}
.footer-wrap .container td.content  p { border-top: 1px solid rgb(215,215,215); padding-top:15px;}
.footer-wrap .container td.content p {
	font-size:10px;
	font-weight: bold;
	
}


/* ------------------------------------- 
		TYPOGRAPHY 
------------------------------------- */
h1,h2,h3,h4,h5,h6 {
font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; line-height: 1.1; margin-bottom:5px; color:#000;
}
h1 small, h2 small, h3 small, h4 small, h5 small, h6 small { font-size: 60%; color: #6f6f6f; line-height: 0; text-transform: none; }

h1 { font-weight:200; font-size: 44px;}
h2 { font-weight:200; font-size: 37px;}
h3 { font-weight:500; font-size: 27px;}
h4 { font-weight:500; font-size: 23px;}
h5 { font-weight:900; font-size: 17px;}
h6 { font-weight:900; font-size: 14px; text-transform: uppercase; color:#444;}

.collapse { margin:0!important;}

p, ul { 
	margin-bottom: 5px; 
	font-weight: normal; 
	font-size:13px; 
	line-height:1.6;
}
p.lead { font-size:17px; }
p.last { margin-bottom:0px;}



/* ------------------------------------- 
		SIDEBAR 
------------------------------------- */
ul.sidebar {
	background:#ebebeb;
	display:block;
	list-style-type: none;
}
ul.sidebar li { display: block; margin:0;}
ul.sidebar li a {
	text-decoration:none;
	color: #666;
	padding:10px 16px;
/* 	font-weight:bold; */
	margin-right:10px;
/* 	text-align:center; */
	cursor:pointer;
	border-bottom: 1px solid #777777;
	border-top: 1px solid #FFFFFF;
	display:block;
	margin:0;
}
ul.sidebar li a.last { border-bottom-width:0px;}
ul.sidebar li a h1,ul.sidebar li a h2,ul.sidebar li a h3,ul.sidebar li a h4,ul.sidebar li a h5,ul.sidebar li a h6,ul.sidebar li a p { margin-bottom:0!important;}



/* --------------------------------------------------- 
		RESPONSIVENESS
		Nuke it from orbit. It's the only way to be sure. 
------------------------------------------------------ */

/* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
.container {
	display:block!important;
	max-width:600px!important;
	margin:0 auto!important; /* makes it centered */
	clear:both!important;
}

/* This should also be a block element, so that it will fill 100% of the .container */
.content {
	padding:5px;
	max-width:600px;
	margin:0 auto;
	display:block; 
}

/* Let's make sure tables in the content area are 100% wide */
.content table { width: 100%; }


/* Odds and ends */
.column {
	width: 300px;
	float:left;
}
.column tr td { padding: 5px; }
.column-wrap { 
	padding:0!important; 
	margin:0 auto; 
	max-width:600px!important;
}
.column table { width:100%;}
.social .column {
	width: 280px;
	min-width: 279px;
	float:left;
}

/* Be sure to place a .clear element after each set of columns, just to be safe */
.clear { display: block; clear: both; }


/* ------------------------------------------- 
		PHONE
		For clients that support media queries.
		Nothing fancy. 
-------------------------------------------- */
@media only screen and (max-width: 600px) {
	
	a[class="btn"] { display:block!important; margin-bottom:10px!important; background-image:none!important; margin-right:0!important;}

	div[class="column"] { width: auto!important; float:none!important;}
	
	table.social div[class="column"] {
		width:auto!important;
	}

}

.ul_flush, .ol_flush {
  list-style: none;
  padding-left: 0;
  display: table;
}
.ul_flush li:after, .ol_flush li:after {
  content: "";
  display: block;
  margin-bottom: 0em;
}

.ul_flush li {
  display: table-row;
}
.ul_flush li:before {
  content: "•";
  display: table-cell;
  padding-right: 0.4em;
}

</style>
</head>
 
<body bgcolor="#FFFFFF">

<!-- HEADER -->



<!-- BODY -->
<table class="body-wrap" style=" width: 100%;">
	<tr>
		<td></td>
		<td class="container" bgcolor="#FFFFFF" style="border: #0085C2 3px solid; border-radius: 15px 15px 15px 15px;-moz-border-radius: 15px 15px 15px 15px;-webkit-border-radius: 15px 15px 15px 15px; display:block!important;max-width:600px!important;margin:0 auto!important; clear:both!important;">

			<div class="content" style="padding:5px;max-width:600px;margin:0 auto;display:block; ">
            
            
			<table>
				<tr>
					<td colspan="2" style="padding-bottom:20px;" align="center">
                    <center><img width="250"  align="center"  src="images/284115LOGO.png" /></center></td>
				</tr>
            
            	<tr>
					<td colspan="2" >
						<p style="color:#666; font-weight:bold;">Dear Subscriber,</p>
                        <p style="color:#666; font-weight:bold;">We trust this email finds you well.</p>
                      

					
					</td>
				</tr>
                <tr>
					<td colspan="2" height="250" background="<?php echo base_url();?>assets/front-end/images/dell-mobi/em-banner-au-mdm-webinar.png" align="center" style="background-position:center;background-repeat:no-repeat;">
						<h3 style="color:#ffffff; font-weight:bold">Webinar: Keeping your data honest with cloud Master Data Management (MDM) </h3><br/>
						<h4 style="color:#ffffff; font-weight:bold">Tuesday, 16 August
11:00 AM AEST </h4><br>
<p><a href="http://marketing.boomi.com/FY17Q3-APJ-Cloud-MDM-Webinar-Registraion.html"><img src="<?php echo base_url();?>assets/front-end/images/dell-mobi/register-button-png-21.png" width="200" /></a> </p>
				    </td>
				</tr>
                
                <tr>
                  <td width="400"> <p style="color:#002050; font-weight:bold"> </p>
<p>Join our data management expert, David Irecki, at this webinar to learn how cloud master data management (MDM) can help your organisation ensure data accuracy and consistency, as well as improve performance and reduce costs easily and effectively.
Learn how you can:
</p>

<ul class="ul_flush">
<li><strong>Adopt cloud MDM deployment in a hybrid IT environment</strong></li>
<li><strong>Model, match, synchronise, cleanse and enrich master data across domains.</strong></li>
<li><strong>Scale your MDM to accommodate growth in data varieties, business applications and user demands</strong></li>
<li><strong>Implement comprehensive data-governance controls</strong></li>
</ul>
</td>

<td width="200" valign="top">
	<table style="border:1px solid #707070;margin-top: 10px;" width="90%"><tbody><tr><td style="font:13px 'Helvetica Neue',Arial,helvetical,sans-serif;color:#444;font-weight:bold;text-align:center;border-bottom:1px solid #706f6f;line-height:30px"><div>Event Details</div>
</td>
</tr>
<tr><td style="padding:5px 5px 5px 10px;font:14px 'Helvetica Neue',Arial,helvetical,sans-serif;color:#444;"><div style="text-align:center;font-weight:bold;"><p style="font-weight: bold;">Tuesday, 16 August<br><br> 11:00 AM AEST<br><br> 9:00AM SGT<br><br></p>


</div>
</td>
</tr>
<tr><td style="padding-bottom:10px" align="center"> 
	<p><a href="http://marketing.boomi.com/FY17Q3-APJ-Cloud-MDM-Webinar-Registraion.html"><img src="<?php echo base_url();?>assets/front-end/images/dell-mobi/register-button-png-21.png" width="150" /></a>
</td>
</tr>
</tbody></table>
	</td>

                </tr>
                
			</table>
	    </div>
			
            	
            
                					
	  </td>
		<td>
        
        </td>
	</tr>
</table><!-- /BODY -->

<!-- FOOTER -->
<!-- /FOOTER -->

</body>
</html>