<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "main";
$route['submitData'] = "main/submitData";
$route['validate'] = "main/validate";
$route['reports'] = "main/reports";
$route['register'] = "main/register";

// Campaign
$route['campaign'] = "campaign";
$route['add_campaign'] ="campaign/add";
$route['edit-client/(:num)'] = "campaign/edit/$1";
$route['edit-campaign/(:num)'] = "campaign/edit/$1";
$route['delete-client/(:num)'] = "campaign/delete/$1";

// Auth
$route['auth/change-password'] = "auth/change_password";
$route['check-campaign-name'] = "campaign/checkCampaignName";
$route['check-campaign-name/(:num)'] = "campaign/checkCampaignName";
$route['campaign-status'] = "campaign/updateCampaignStatus";

$route['download-pdf'] = "main/downloadPDF";
$route['download-success'] = "campaign/downloadContent";

$route['esg-pdf/(:any)'] = "campaign/registerEsgCustomer/$1";

// $route['pdf-download/(:any)'] = "campaign/pdfDownloadRedirect/$1";
$route['pdf-download/(:any)/(:any)'] = "campaign/emailDownload/$1/$2";
$route['register-customer/(:any)'] = "campaign/registerCustomer/$1";
$route['signup-user/(:any)/(:any)'] = "campaign/landingPageRegistration/$1/$2";

$route['show-campaign'] = "campaign/showCampaign";

$route['client-details'] = "campaign/getClientData";

$route['download-content'] = "campaign/downloadContent";

$route['send-bulk-email'] = "campaign/sendBulkCampaignMail";

// User Routes
$route['add-user'] = "user/actionUser";
$route['upload-user'] = "user/uploadUser";
$route['edit/(:num)/user'] = "user/actionUser/$1";
$route['delete/(:num)/user'] = "user/deleteUser/$1";
$route['user-bulk-delete'] = "user/bulkdeleteUser";


// Content
$route['content'] = "content";
$route['add_content'] ="content/add";
$route['edit-content/(:num)'] = "content/edit/$1";
$route['delete-content/(:num)'] = "content/delete/$1";

// Send Email
$route['SendEmail'] = "admin/SendEmail";
$route['denTestEmail'] = "admin/denTestEmail";
$route['getContentByCampaign'] = "admin/getContentByCampaign";

$route['unsubscribe'] = "main/unsubscribe";

$route['unsubscriber/mail-listing'] = "unsubscriber/listing";
$route['unsubscriber/get-data'] ="unsubscriber/getUnsubscriberDatatable";

$route['404_override'] = '';

/* End of file routes.php */
/* Location: ./application/config/routes.php */