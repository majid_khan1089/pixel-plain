<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->library('ion_auth');

		
		if($this->session->userdata('lang')== 'ar'){
			$langFile = 'custom'; $folderName = 'arabic';
		}else{
			$langFile = 'custom'; $folderName = 'english';
		}

		$this->lang->load($langFile, $folderName);		
	}
	
	public function index(){
		if ($this->ion_auth->logged_in())
		{
			redirect('auth', 'refresh');
		}

		$this->load->view('admin/login');
	}


	public function change_lang(){
		$lang =$this->input->post('lang');
		$this->session->set_userdata('lang', $lang);	

		if($this->session->userdata('lang')== 'ar')
		{     
			return true;
		} 
	}

    public function getContentByCampaign(){
        $data = $this->admin_model->getContentByCampaign();
       echo json_encode($data);
    } 

	public function SendEmail(){
       $mailData = array("give"=>1);//$this->admin_model->getMailData($this->input->post('content'));
    
       if(!empty($mailData)){
            //$data['link'] = "http://cloudpapers.net/cp-player/".$mailData[0]->campaign_code."?contentId=".$mailData[0]->content_code."&source=".$mailData[0]->source_id."&email=".$this->input->post('p_name');
            $data['url'] = "";
            
            $config = Array(
                'mailpath' => '/usr/sbin/sendmail',
                'protocol' => 'smtp',
                'smtp_host' => 'smtp.gmail.com',
                'smtp_port' => 465,
                'smtp_user' => 'info@pixel-admin.com',
                'smtp_pass' => '123456',
                'smtp_timeout'=>20,
                'mailtype' => 'html',
                'charset' => 'iso-8859-1',
                'wordwrap' => TRUE,
                'crlf' => "\r\n",
    	        'newline' => "\r\n"
            );

            $this->email->initialize($config);
            $this->email->from('info@pixel-admin.com', 'Bizzmartech.com');
            $this->email->to($this->input->post("p_name"));
            $this->email->subject('Marketo');
            
            $message = $this->load->view('mailtemplate/index', $data, TRUE);
            
            $this->email->message($message);
            
            if($this->email->send())
            {
                $this->session->set_flashdata('danger', "Invalid Request");
                redirect(base_url()."campaign/sendEmail");    
            }
            else{
                $this->session->set_flashdata('success', "Email Sent successfully");
                redirect(base_url()."campaign/sendEmail");
           }
        }
        else{
            echo "No Data Found";     
        }
            echo $this->email->print_debugger();
    }
        
    public function denTestEmail(){
    	$config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => 'info@pixel-admin.com',
            'smtp_pass' => '123456',
            'mailtype'  => 'html', 
            'charset'   => 'iso-8859-1'
        );
		
        $this->load->library('email', $config);
		$this->email->set_newline("\r\n");

		$this->email->from('admin@pixel-admin.com', 'Bizzmartech.com');
    	$this->email->to("info@pixel-admin.com");
    	$this->email->subject('Marketo');
    	$message = $this->load->view('mailtemplate/index');
    	$this->email->message($message);

		// Set to, from, message, etc.
        $result = $this->email->send();
    }
}

/*
//"http://bizmartech.com/?id=".base64_encode($mailData[0]->campaign_code."/".$mailData[0]->content_code."/".$mailData[0]->source_id."/".$this->input->post('p_name'))"1998/4286/TMC001/shamshuddins@gmail.com"; 
*/