<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Unsubscriber extends CI_Controller {
	private $data = array();

	public function __construct(){
		parent::__construct();

		if (!$this->ion_auth->logged_in()) {
			redirect('auth/login', 'refresh');
		}

		$this->load->model('unsubscriber_model');
	}


	public function listing(){
		$this->load->view('admin/unsubscriber/listing',$this->data);
	}

	public function getUnsubscriberDatatable(){
        $data = array();
        $responseResult = $this->unsubscriber_model->get_datatables();

        //print_r($responseResult);die;

        $no = $this->input->post('start');
        $i =1;
        
        foreach ($responseResult as $response) {
            $no++;
            
            $row = array();
            $row[] = $i++ ;            
            $row[] = ($response->email) ? ($response->email) :  'N.A';
            $row[] = ($response->added_on) ? date('d-m-Y H:i:s', strtotime($response->added_on)) : 'N.A';

            $data[] = $row;
        }

        $output = array(
                        "draw" => $this->input->post('draw'),
                        "recordsTotal" => $this->unsubscriber_model->count_all(),
                        "recordsFiltered" => $this->unsubscriber_model->count_filtered(),
                        "data" => $data
                );

        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($output));
    }
}