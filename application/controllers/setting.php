<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends CI_Controller{
	private $data = array();

 	public function __construct() {
        parent::__construct();

        $this->load->model("setting_model");
    }

    public function index(){

    	$this->load->view('setting');
    }

}