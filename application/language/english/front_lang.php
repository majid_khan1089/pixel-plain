<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['txt_home_title']         	  = 'Lawyer';

$lang['txt_menu_']         	  = 'Lawyer';

$lang['txt_menu_about_us']         	= 'ABOUT US';
$lang['txt_menu_our_services']      = 'OUR SERVICES ';
$lang['txt_menu_ask_a_question']	= 'ASK A QUESTION ';
$lang['txt_menu_contact_us']        = 'CONTACT US';