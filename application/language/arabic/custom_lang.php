<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['txt_home']         	  = 'منزل';
$lang['txt_law_book']         = 'كتاب القانون';
$lang['txt_user_questions']   = 'أسئلة المستخدم';
$lang['txt_user_list']        = 'قائمة المستخدم';
$lang['txt_about_us']         = 'معلومات عنا';
$lang['txt_change_password']  = 'تغيير كلمة المرور';
$lang['txt_sign_out']         = 'الخروج';
$lang['txt_admin']         	  = 'المشرف';

$lang['txt_user_listing']     = 'قائمة المستخدم';

// Form Elements
$lang['txt_form_sno']         = 'السيد رقم';
$lang['txt_form_first_name']  = 'الاسم الاول';
$lang['txt_form_last_name']   = 'الاسم الاخير';
$lang['txt_form_email']       = 'البريد الإلكتروني';
$lang['txt_form_mobile_no']   = 'رقم الهاتف المحمول';
$lang['txt_form_user_type']   = 'نوع المستخدم';
$lang['txt_form_active']      = 'الحالة';
$lang['txt_form_action']      = 'إجراء';

$lang['txt_form_book_link']   = 'كتاب لينك';
$lang['txt_form_description_en'] = 'الوصف';
$lang['txt_form_description_ar'] = 'الوصف';
$lang['txt_form_cover_page']  	= 'غطاء الصفحة';
$lang['txt_form_book_name']   	= 'اسم الكتاب';

$lang['txt_form_username']   	= 'اسم الكتاب';
$lang['txt_form_question']   	= 'اسم الكتاب';
$lang['txt_form_add']   	  	= 'إضافة';
$lang['txt_client_questions']   = 'إضافة';
$lang['txt_clients']   	  		= 'إضافة';
$lang['txt_form_subject']   	= 'إضافة';

