<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Unsubscriber_model extends CI_Model
{
    var $table = 'unsubscribe';
    var $primary_key = 'id';
    var $column_order = array(null, 'email','added_on');
    var $column_search = array( 'email','added_on');
    var $order = array('id' => 'desc');
 
    public function __construct(){
        parent::__construct();
    }

    private function _get_datatables_query(){
        $i = 0;

        $this->db->from($this->table);
        
        foreach ($this->column_search as $item){
            if($_POST['search']['value']){
                if($i===0){
                   // $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    //$this->db->like($item, $_POST['search']['value']);
                    $this->db->like('LOWER(' .$item. ')', strtolower($_POST['search']['value']));
                }
                else{
                    //$this->db->or_like($item, $_POST['search']['value']);
                    $this->db->or_like('LOWER(' .$item. ')', strtolower($_POST['search']['value']));
                }
 
               // if(count($this->column_search) - 1 == $i) //last loop
                   // $this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if(isset($_POST['order'])){
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    public function get_datatables(){
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($this->input->post('length'), $this->input->post('start'));
        $query = $this->db->get();
        return $query->result();
    }
 
    public function count_filtered(){
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all(){
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function getLogByID($id){
        return $this->db->get_where($this->table, array($this->primary_key => $id))->row();
    }

    public function deleteCategory($id){
        $status = FALSE;
        $result = $this->getCategoryByID($id);

        if(!empty($result)){
            $this->db->where($this->primary_key, $id);
            $this->db->delete($this->table);
            $status = TRUE;
        }

        return $status;
    }
}