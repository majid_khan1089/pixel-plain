<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}
	
	public function getCampaignData($type = ''){
		$this->db->select('*');
		$this->db->from('campaign');
		$this->db->where('campaign_slug', $type);
		return  $this->db->get()->row();
	}

	public function get_campaign(){
		if($this->uri->segment(2) == 'sendEmail'){
			return $this->db->order_by('date_added', 'desc')->where('status', "1")->get('campaign')->result();	
		}
		
		return $this->db->order_by('date_added', 'desc')->get('campaign')->result();
	}

	public function get_pdf_log(){
		$this->db->select('*');
		$this->db->from('pdf_campagin');
		$this->db->group_by('email');
		return  $this->db->get()->result();
	}

	public function get_registeruser_log($userID = ''){
		$this->db->select('*');
		$this->db->from('clients');
		
		if($userID != ''){
			$this->db->where('user_id', $userID);	
		}

		$this->db->group_by('email,campaign_type');
		
		$this->db->order_by('date_added', 'desc');

		if($userID != ''){
			return  $this->db->get()->row();
		}

		return  $this->db->get()->result();
	}

	public function add_campaign($data = array()){
		$data = array(
			'url' => $this->input->post('camp_url'),
			'campaign_name' => $this->input->post('camp_name'),
			'campaign_slug' => $data['slug'],
			'campaign_type' =>  $this->input->post('campaign_type'),
			'mail_subject' =>  $this->input->post('mail_subject'),
			'url_type'	=> $this->input->post('url_type'),
			'pdf_url' => $this->input->post('pdf_url'),
			'landing_page_slug' =>  'landingpage/'. $data['slug'], //$this->input->post('landing_page_slug'),
			'url' => ($this->input->post('campaign_type') =='email') ?  'pdf-download/' : 'signup-user/',
			);

		$this->db->insert('campaign', $data);
	}

	public function getCampaignByID($id){
		return $this->db->get_where('campaign', array('campaign_id' => $id))->row();
	}

	public function getCampaignBySlug($slug){
		return $this->db->get_where('campaign', array('campaign_slug' => $slug))->row();
	}

	public function updatet_campaign(){
		$data = array(
			'url' => $this->input->post('camp_url'),
			'campaign_name' => $this->input->post('camp_name'),
			'campaign_type' =>  $this->input->post('campaign_type'),
			'mail_subject' =>  $this->input->post('mail_subject'),
			'url_type'	=> $this->input->post('url_type'),
			'pdf_url' => $this->input->post('pdf_url'),
			'landing_page_slug' =>  $this->input->post('landing_page_slug'),
			'url' => ($this->input->post('campaign_type') =='email') ?  'pdf-download/' : 'signup-user/',
		);
		$this->db->where('campaign_id', $this->uri->segment(2));
		$this->db->update('campaign', $data);
	}

	public function delete_campaign($id){
		$this->db->where('campaign_id', $id);
		$this->db->delete('campaign');
	}

	// Content Management
	public function get_content(){
		return $this->db->get('content')->result();
	}

	public function add_content(){
		$data = array(
			'content_name' => $this->input->post('content_name'),
			'campaign_id' => $this->input->post('campaign'),
			'content_code' => $this->input->post('content_code')
			);
		$this->db->insert('content', $data);
	}
         
    public function getContentByCampaign()
    {
       return $this->db->get_where('content', array('campaign_id' => $this->input->post('campaign_id')))->result_array();
    }

	public function getContentByID($id){
		return $this->db->get_where('content', array('content_id' => $id))->result();
	}

	public function updatet_content(){
		$data = array(
			'content_name' => $this->input->post('content_name'),
			'campaign_id' => $this->input->post('campaign'),
			'content_code' => $this->input->post('content_code')
			);
		$this->db->where('content_id', $this->uri->segment(2));
		$this->db->update('content', $data);
	}

	public function delete_content($id){
		$this->db->where('content_id', $id);
		$this->db->delete('content');
	}

	public function getMailData($id){
		$this->db->select('*');
		$this->db->from('campaign');
		$this->db->join('content', 'campaign.campaign_id = content.campaign_id');
		$this->db->where(array('content.content_id' => $id));
		return  $this->db->get()->result();
	}

	public function add_pdf_campaign(){
		$data = array(
			'email' => ($this->uri->segment(2)),
			'link' => "http://bizmartech.com/assets/pdf/10-Mistakes-to-Avoid-When-Buying-a-Phone-System.pdf",
			'date' => date('Y-m-d H:i:s')
		);

		$this->db->insert('pdf_campagin', $data);

		return true;
	}

	// Dump In Database
	public function addEmailTemplate($email, $mailFrom, $mailFromName, $mailSubject, $type, $message, $link){
		$data = array(
			'customer_email'=> $email,
			'mail_from' => $mailFrom,
			'mail_from_name' => $mailFromName,
			'mail_subject' => $mailSubject,
			'email_type'=> $type,
			'email_template' => $message,
			'link' => $link,
			'created_at' => date('Y-m-d H:i:s')
		);

		$this->db->insert('email_logs',$data);
	}

	public function campaignCheck($campaignName){
		return $this->db->get_where('campaign', array('campaign_name' => $campaignName))->num_rows();
	}

	public function getEmailCount(){
		//$this->db->where('account_status', $i);
		return $num_rows = $this->db->count_all_results('email_logs');
	}

	public function updateCampaignStatus($campaignID, $status){
		$data = array("status" => (string) $status);

		$this->db->where('campaign_id', $campaignID);
		$this->db->update('campaign', $data);

		return $this->db->affected_rows();
	}

	public function updateEmailLog($logID){
		$this->db->where('id',$logID);
		$this->db->update('email_logs',array('status'=>'1'));
	}

	public function getEmailLogs(){
		$this->db->select('*');
		$this->db->from('email_logs');
		$this->db->where('status', '0');
		$query = $this->db->get();
              
		return $data = $query->result();
	}
}